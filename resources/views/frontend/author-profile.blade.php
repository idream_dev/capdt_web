@extends('layouts.app')
@section('title', $title)
@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-8">
                            <article class="pagetitle">
                                <h1>Author Profile</h1>
                                <p> {{ $authorprofile->name }}</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-4 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Author Profile</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-2">
                            {{--@include('author.includs.sidebar')--}}
                        </div>

                        <div class="col-lg-10 right-author">

                            <table class="table">
                                <tr>
                                    <td>image:</td>
                                    <td>@if($authorprofile->profile_image!='')<img src="{{url('uploads/users/'.$authorprofile->profile_image)}}">@else -- @endif</td>
                                </tr>
                                <tr>
                                    <td>Name:</td>
                                    <td> {{ $authorprofile->name }}</td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td> {{ $authorprofile->email }} </td>

                                </tr>
                                <tr>
                                    <td>Mobile:</td>
                                    <td> +91xxxxxxxxxx </td>
{{--                                    {{ $authorprofile->mobile }}--}}
                                </tr>

                            </table>

                        </div>
                    </div>


                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection































