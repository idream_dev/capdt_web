@extends('layouts.app')
@section('title', $title)


@section('metaheader')
<!-- For Facebook -->
<meta property="og:title" content="CAPDT MEMES" />
<meta property="og:type" content="article" />
<meta property="og:image" content="{{url('/uploads/memes/'.$memeinfo->m_meme)}}" />
<meta property="og:url" content="{{route('MemesInfo',['id'=>$memeinfo->m_id])}}" />
<meta property="og:description" content="CAPDT MEMES" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="CAPDT MEMES" />
<meta name="twitter:description" content="CAPDT MEMES" />
<meta name="twitter:image" content="{{url('/uploads/memes/'.$memeinfo->m_meme)}}" />




@endsection


@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-6">
                            <article class="pagetitle">
                                <h1>Capdt Memes </h1>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-6 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="{{route('Stories')}}">Memes</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left article col -->
                        <div class="col-lg-9">
                            <div class="articledetail">

                                <!-- row -->

                                <!--/ row -->
                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <figure class="pb-2">
                                            <img src="/uploads/memes/{{$memeinfo->m_meme}}" class="img-fluid w-100">
                                        </figure>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- col-4 -->
                                    <div class="col-lg-4 col-sm-3">
                                        <p class="themecolor dtnote">Uploaded @<span>{{$memeinfo->created_at}}</span></p>
                                    </div>
                                    <!-- col-4 -->
                                    <!-- col-8-->
                                    <div class="col-lg-8 col-sm-9 text-right">
                                        <div class="detailsocial float-right">
                                            <p class="float-left pr-2">Recommend to your friends <div class="sharethis-inline-share-buttons"></div></p>

                                            {{--<ul class="float-left nav">--}}
                                                {{--<li class="nav-item"><a href="http://www.facebook.com/sharer.php?u={{route('MemesInfo',['id'=>$memeinfo->m_id])}}"><img src="/site/images/socialfb.jpg"></a></li>--}}
                                                {{--<li class="nav-item"><a href="http://twitter.com/share?url={{route('MemesInfo',['id'=>$memeinfo->m_id])}}"><img src="/site/images/socialgplus.jpg"></a></li>--}}
                                                {{--<li class="nav-item"><a href="javascript:void(0)"><img src="/site/images/socialtwitter.jpg"></a></li>--}}
                                                {{--<li class="nav-item"><a href="javascript:void(0)"><img src="/site/images/socialutube.jpg"></a></li>--}}
                                            {{--</ul>--}}

                                        </div>
                                    </div>
                                    <!--/ col-8-->
                                </div>
                                <!--/row -->
                                <!-- row -->

                                <!--/ row -->

                            </div>
                        </div>
                        <!--/ left article col -->
                        <!-- right article col -->
                        <div class="col-lg-3 col-sm-12">
                            <h2 class="pt-3">Latest <span>Memes</span></h2>
                            <div class="row">
                            @if(sizeof($latestmemes)>0)
                            @foreach($latestmemes as $latestmeme)

                                    <div class="listitem col-lg-12 col-sm-4">
                                        <figure>
                                            <a href="{{route('MemesInfo',['id'=>$latestmeme->m_id])}}"> <img src="/uploads/memes/{{$latestmeme->m_meme}}" alt="" title="" class="img-fluid"> </a>
                                        </figure>
                                    </div>

                                @endforeach
                            @else
                                <div class="col-lg-12">
                                    <div class="listitem ">
                                        <p class="d-block text-center pb-0">No Stories Yet</p>
                                    </div>
                                </div>
                            @endif
                            </div>
                        </div>
                        <!--/ right article col -->

                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection












































































