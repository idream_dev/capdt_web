@extends('layouts.app')
@section('title', $title)
@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-4">
                            <article class="pagetitle">
                                <h1>Funny Memes </h1>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-8 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="javascript:void(0)">Funny Memes</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <div class="container">
                    <!-- row -->
                    @if(sizeof($memes)>0)
                    <div class="row py-4">

                        @foreach($memes as $memesInfo)
                        <div class="col-lg-3 col-sm-4">
                            <div class="pagecolumn">
                                <figure>
                                    <a href="{{route('MemesInfo',['id'=>$memesInfo->m_id])}}"><img src="uploads/memes/{{$memesInfo->m_meme}}" alt="" title="" class="img-fluid w-100"></a>
                                </figure>

                            </div>
                        </div>

                        @endforeach

                    </div>
                    @else
                        <div class="row py-4">There is no Memes yet</div>
                    @endif
                    <!--/ row -->
                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection































