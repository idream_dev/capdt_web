@extends('layouts.app')
@section('title', $title)
@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-4">
                            <article class="pagetitle">
                                <h1>Latest Videos</h1>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-8 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="javascript:void(0)">Latest Videos</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <div class="container">
                    @if(sizeof($videos)>0)
                    <div class="row py-4">
                        @foreach($videos as $videosInfo)
                        <div class="col-lg-3 col-sm-4">
                            <div class="listitem videolist">
                                <figure>
                                    <a href="{{route('LatestvideoInfo',['id'=>$videosInfo->yt_video_id])}}">
                                        <img src="https://img.youtube.com/vi/{{$videosInfo->yt_video_id}}/mqdefault.jpg" alt="{{$videosInfo->yt_title}}" title="{{$videosInfo->yt_title}}"class="img-fluid">
                                    </a>
                                </figure>
                                <article>
                                    <a href="{{route('LatestvideoInfo',['id'=>$videosInfo->yt_video_id])}}">{{$videosInfo->yt_title}}</a>
                                </article>
                                <p class="comments-col">
                                    <a href="javascript:void(0)"><span class="icon-clock-o icomoon mr-1"></span>{{ \Carbon\Carbon::parse($videosInfo->yt_time_uploaded)->diffForHumans() }}</a>
                                </p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @else
                        <div class="row py-4">There is no Videos yet</div>
                    @endif
                    <!--/ row -->
                    <!-- row -->
                    <div class="row pb-3">
                        <div class="col-lg-12">
                                {!! $videos->appends(['filters' => Request::get('filters')])->render() !!}
                        </div>
                    </div>
                    <!--/ row -->
                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection































