@extends('layouts.app')
@section('title', $title)
@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-6">
                            <article class="pagetitle">
                                <h1>Contact us </h1>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-6 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Contact</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <div class="container">
                    <!-- row -->
                    <div class="row py-4">
                        <!-- col -->
                        <div class="col-lg-8 col-sm-8">
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-6 col-sm-6">
                                    <div class="addcol">
                                        <h6 class="h5">For General Enquries</h6>
                                        <p class="pb-0">Please mail at</p>
                                        <a href="mailto:info@capdt.com">capdtvideos@gmail.com</a>
                                    </div>
                                </div>
                                <!--/ col -->



                                <!-- col -->
                                <div class="col-lg-6 col-sm-6">
                                    <div class="addcol">
                                        <h6 class="h5">For Contact directly</h6>
                                        <p class="pb-0">Please contact at</p>
                                        <a>+91 6303068076</a>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-6 col-sm-6">
                                    <div class="addcol">
                                        <h6 class="h5">Address</h6>
                                        <p class="pb-0">Address: 8-2-268/V/21, Sagar society,<br/>Arora colony, Srinagar Colony Main Rd,<br/>Hyderabad, Telangana 500073</p>
                                    </div>
                                </div>
                                <!--/ col -->

                            </div>
                        </div>
                        <!-- col -->
                        <!-- col social -->
                        <div class="col-lg-4 col-sm-4 ">
                            <table class="tablecontact">
                                <tr>
                                    <td><figure><a href="{!! (!empty($settings) && $settings->s_facebook) ? $settings->s_facebook : 'https://www.facebook.com/CAPDT/' !!}" target="_blank"><span class="icon-facebook icomoon"></span></a></figure></td>
                                    <td><p class="pb-0"><a href="{!! (!empty($settings) && $settings->s_facebook) ? $settings->s_facebook : 'https://www.facebook.com/CAPDT/' !!}" target="_blank">Like & Share on Facebook</a></p></td>
                                </tr>
                                <tr>
                                    <td><figure><a href="{!! (!empty($settings) && $settings->s_instagram) ? $settings->s_instagram : 'https://www.instagram.com/capdt/' !!}" target="_blank"><span class="icon-instagram icomoon"></span></a></figure></td>
                                    <td><p class="pb-0"><a href="{!! (!empty($settings) && $settings->s_instagram) ? $settings->s_instagram : 'https://www.instagram.com/capdt/' !!}" target="_blank">Like & Share on Instagram</a></p></td>
                                </tr>
                                <tr>
                                    <td><figure><a href="{!! (!empty($settings) && $settings->s_twitter) ? $settings->s_twitter : 'https://twitter.com/CapdtOfficial' !!}" target="_blank"><span class="icon-twitter icomoon"></span></a></figure></td>
                                    <td><p class="pb-0"><a href="{!! (!empty($settings) && $settings->s_twitter) ? $settings->s_twitter : 'https://twitter.com/CapdtOfficial' !!}" target="_blank">Like & Share on Twitter</a></p></td>
                                </tr>
                                <tr>
                                    <td><figure><a href="{!! (!empty($settings) && $settings->s_youtube) ? $settings->s_youtube : 'https://www.youtube.com/channel/UCm2Q5o3WbfDbgVnLZFF9wLA' !!}" target="_blank"><span class="icon-youtube-play icomoon"></span></a></figure></td>
                                    <td><p class="pb-0"><a href="{!! (!empty($settings) && $settings->s_youtube) ? $settings->s_youtube : 'https://www.youtube.com/channel/UCm2Q5o3WbfDbgVnLZFF9wLA' !!}" target="_blank">Watch on Youtube</a></p></td>
                                </tr>
                            </table>
                        </div>
                        <!--/ col social -->
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-6">
                            <p>Fill the following form for any queries, Our Executives will get back to you for proper response.</p>
                        </div>
                    </div>
                    <!--/ row -->
                    @if (Session::has('flash_message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif

                    <form method="POST" id="contactform" action="{{ route('contactus') }}" accept-charset="UTF-8"
                          class="contactform">
                    {{ csrf_field() }}
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input type="text" name="name" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text"  name="contact_number" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="text" name="email" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea name="message" class="form-control w-100" style="height:100px;"></textarea>
                                </div>
                            </div>
                        </div>
                        <!--/ row -->
                        <input type="submit" value="Submit" class="btn">

                    </form>

                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection































