@extends('layouts.app')
@section('title', $title)
@section('content')

    <?php
    $channels = getChannels();
    ?>

    <!--main -->
    <main>
        <!-- slider section -->
        <section class="homecarouse">
            <div class="container-fluid ">
                <!-- row -->
                <div class="row">
                    <!-- col slider -->
                    <div class="col-lg-12 mx-0 px-0">
                        <!-- carousel-->
                        @if(sizeof($banners)>0)
                            <div class="homecarousel">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0"
                                            class="active"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner" role="listbox">
                                        <!-- Slide One - Set the background image for this slide in the line below -->
                                        @foreach($banners as $banner)
                                            <div class="carousel-item serieslist @if($loop->iteration==1) active @endif"
                                                 style="background-image: url('/uploads/banners/{{$banner->b_banner}}')">
                                                <a <?php if($banner->b_link != ''){ ?> href="{{$banner->b_link}}"
                                                   target="_blank"
                                                   <?php }else{ ?>  href="javascript:void(0)" <?php } ?>><img
                                                            class="svg" src="{{url('site/images/youtube.svg')}}"></a>
                                                <div class="carousel-caption">
                                                    <div class="captionin">
                                                        @if($banner->b_channel_name!='')
                                                            <h5>{{$banner->b_channel_name}}</h5>@endif
                                                        @if($banner->b_title!='')<h2 class="slidertitle"><a
                                                                    href="#">{{$banner->b_title}}</a></h2>@endif
                                                        @if($banner->b_description!='')
                                                            <p>{{$banner->b_description}}</p>@endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                       data-slide="prev">
                                        <span class="icon-chevron-left icomoon"></span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                       data-slide="next">
                                        <span class="icon-chevron-right icomoon"></span>
                                    </a>

                                </div>
                            </div>
                    @endif
                    <!--/ carousel-->
                    </div>
                    <!--/ col slider -->
                </div>
                <!--/ row -->
            </div>
        </section>


        @if(sizeof($channels)>0)
            <section class="channels py-3">
                <div class="container">
                    <article class="hometitle py-4 text-center">
                        <h2>Our Entertainment Channels</h2>
                    </article>

                    <div class="row justify-content-center">

                    @foreach($channels as $channel)
                        <?php
                        $image = json_decode($channel->ch_thumbnails);
                        ?>
                        <!-- col -->
                            <div class="col-lg-3 col-4 col-sm-4 text-center channelcol">
                                <figure>
                                    {{--<a href='{{route('Channels',['alias'=>$channel->ch_customUrl])}}'--}}
                                    {{--title='{{$channel->ch_title}}'><img width='50px' src='{{$image->high->url}}'--}}
                                    {{--title='{{$channel->ch_title}}'/></a>--}}


                                    <a href='https://www.youtube.com/channel/{{$channel->ch_channel_id}}'
                                       target="_blank"
                                       title='{{$channel->ch_title}}'><img width='50px' src='{{$image->high->url}}'
                                                                           title='{{$channel->ch_title}}'/></a>
                                </figure>
                                <article>
                                    <h5 class="text-uppercase">
                                        {{--<a href="{{route('Channels',['alias'=>$channel->ch_customUrl])}}">--}}
                                        {{--{{$channel->ch_title}}--}}
                                        {{--</a>--}}

                                        <a href="https://www.youtube.com/channel/{{$channel->ch_channel_id}}"
                                           target="_blank">
                                            {{$channel->ch_title}}
                                        </a>
                                    </h5>
                                    {{--<p>--}}
                                    {{--{{--}}
                                    {{--get_yt_subs($channel->ch_channel_id)--}}
                                    {{--}} subscribers--}}
                                    {{--</p>--}}
                                </article>
                            </div>
                        @endforeach
                    </div>
                    <!--/row -->
                </div>
            </section>
        @endif
    <!-- channel section -->
        <!-- popular web series -->

        @if(sizeof($webseries)>0)
            <section class="homewebseries">
                <div class="container-fluid">
                    <article class="hometitle py-4">
                        <h2>Popular Web Series <a href="{{route('WebseriesList')}}">View All</a></h2>
                    </article>
                </div>
                <!-- slider -->
                <section class="sliderhome">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="resCarousel" data-items="2-3-3-3" data-slide="1" data-speed="900"
                                 data-animator="lazy">
                                <div class="resCarousel-inner">
                                    @foreach($webseries as $web)
                                        <div class="item col-lg-2 col-6">
                                            <figure>
                                                <img src="/uploads/webseries/{{$web->w_poster}}" alt="" title=""
                                                     class="img-fluid w-100">
                                            </figure>
                                            <article>
                                                <h4>{{$web->w_title}}</h4>
                                                <a href="{{route('WebseriesView',['alias'=>$web->w_alias])}}">View All
                                                    Videos</a>
                                            </article>
                                        </div>
                                    @endforeach
                                </div>
                                @if(sizeof($webseries)>6)
                                    <a class='leftRs'><span class="icon-chevron-left icomoon"></span></a>
                                    <a class='rightRs'><span class="icon-chevron-right icomoon"></span></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
                <!-- slider -->
            </section>
    @endif
    <!--/ popular web series -->

    @if(sizeof($latestvideos)>0)
        <!-- latest entertainment videos -->
            <section class="videosrow">
                <div class="container-fluid">
                    <article class="hometitle py-4">
                        <h2>Latest Videos <a href="{{route('LatestVideos')}}">View All</a></h2>
                    </article>
                </div>
                <!--slider -->
                <section class="videoslist">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="resCarousel" data-items="2-3-3-3" data-slide="1" data-speed="900"
                                 data-animator="lazy">
                                <div class="resCarousel-inner">
                                    @foreach($latestvideos as $latestvideo)
                                        <div class="item col-lg-2 col-6">
                                            <figure>
                                                <a href="{{route('LatestvideoInfo',['id'=>$latestvideo->yt_video_id])}}">
                                                    <img src="https://img.youtube.com/vi/{{$latestvideo->yt_video_id}}/mqdefault.jpg"
                                                         alt="{{$latestvideo->yt_title}}"
                                                         title="{{$latestvideo->yt_title}}" class="img-fluid w-100">
                                                </a>
                                            </figure>
                                            <article>
                                                <a href="{{route('LatestvideoInfo',['id'=>$latestvideo->yt_video_id])}}">
                                                    <?php //echo substr($latestvideo->yt_title, 0, 50) . '  . . . . .'; ?>
                                                    {{ strtolower($latestvideo->yt_title) }}
                                                </a>
                                            </article>
                                        </div>
                                    @endforeach
                                </div>
                                @if(sizeof($latestvideos)>6)
                                    <a class='leftRs'><span class="icon-chevron-left icomoon"></span></a>
                                    <a class='rightRs'><span class="icon-chevron-right icomoon"></span></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
                <!--/slider-->
            </section>
            <!--/ latest entertainment videos -->
    @endif

    @if(sizeof($popularvideos)>0)
        <!-- Most Popular  videos -->
            <section class="videosrow">
                <div class="container-fluid">
                    <article class="hometitle py-4">
                        <h2>Popular Videos <a href="{{route('PopularVideos')}}">View All</a></h2>
                    </article>
                </div>
                <!--slider -->
                <section class="videoslist">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="resCarousel" data-items="2-3-3-3" data-slide="1" data-speed="900"
                                 data-animator="lazy">
                                <div class="resCarousel-inner">
                                @foreach($popularvideos as $popularvideo)
                                    <!--item -->
                                        <div class="item col-lg-2 col-6">
                                            <figure>
                                                <a href="{{route('PopularvideoInfo',['id'=>$popularvideo->yt_video_id])}}"><img
                                                            src="https://img.youtube.com/vi/{{$popularvideo->yt_video_id}}/mqdefault.jpg"
                                                            alt="{{$popularvideo->yt_title}}"
                                                            title="{{$popularvideo->yt_title}}" class="img-fluid w-100"></a>
                                            </figure>
                                            <article>
                                                <a href="{{route('PopularvideoInfo',['id'=>$popularvideo->yt_video_id])}}">
                                                    <?php //echo substr($latestvideo->yt_title, 0, 50) . '  . . . . .'; ?>
                                                    {{$latestvideo->yt_title}}
                                                </a>
                                            </article>
                                        </div>
                                        <!--/ item-->
                                    @endforeach
                                </div>
                                @if(sizeof($popularvideos)>6)
                                    <a class='leftRs'><span class="icon-chevron-left icomoon"></span></a>
                                    <a class='rightRs'><span class="icon-chevron-right icomoon"></span></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
                <!--/slider-->
            </section>
            <!--/ Most Popular Videos videos -->
    @endif
    @if(sizeof($stories)>0)
        <!-- Capdt Diaries & Articles -->
            <section class="diaries">
                <div class="container-fluid">
                    <article class="hometitle py-4">
                        <h2>Capdt Diaries & Articles <a href="{{route('Stories')}}">View All</a></h2>
                    </article>
                </div>
                <!--slider -->
                <section class="videoslist">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="resCarousel" data-items="2-3-3-3" data-slide="1" data-speed="900"
                                 data-animator="lazy">
                                <div class="resCarousel-inner">
                                    @foreach($stories as $story)
                                        <div class="item col-lg-2 col-6">
                                            <figure>
                                                <a href="{{route('StoryInfo',['alias'=>$story->s_alias])}}"><img
                                                            src="/uploads/stories/{{$story->s_poster}}"
                                                            alt="{{$story->s_title}}" title="{{$story->s_title}}"
                                                            class="img-fluid w-100"></a>
                                            </figure>
                                            <article>
                                                <a href="{{route('StoryInfo',['alias'=>$story->s_alias])}}">{{$story->s_title}}</a>
                                            </article>
                                        </div>
                                    @endforeach
                                </div>
                                @if(sizeof($stories)>6)
                                    <a class='leftRs'><span class="icon-chevron-left icomoon"></span></a>
                                    <a class='rightRs'><span class="icon-chevron-right icomoon"></span></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
                <!--/slider-->
            </section>
            <!--/ Capdt Diaries & Articles -->
    @endif


    @if(sizeof($memes)>0)
        <!-- Funny Memes -->
            <section class="">
                <div class="container-fluid">
                    <article class="hometitle py-4">
                        <h2>Funny Memes <a href="{{route('Memes')}}">View All</a></h2>
                    </article>
                </div>
                <!-- slider -->
                <section class="">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="resCarousel" data-items="2-3-3-3" data-slide="1" data-speed="900"
                                 data-animator="lazy">
                                <div class="resCarousel-inner">
                                @foreach($memes as $meme)
                                    <!--item -->
                                        <div class="item col-lg-2 col-6">
                                            <figure>
                                                <a href="{{route('MemesInfo',['id'=>$meme->m_id])}}"><img
                                                            src="/uploads/memes/{{$meme->m_meme}}" alt="" title=""
                                                            class="img-fluid w-100"></a>
                                            </figure>

                                        </div>


                                        <!--/ item-->
                                    @endforeach
                                </div>
                                @if(sizeof($memes)>6)
                                    <a class='leftRs'><span class="icon-chevron-left icomoon"></span></a>
                                    <a class='rightRs'><span class="icon-chevron-right icomoon"></span></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
                <!-- slider -->
            </section>
    @endif
    <!--/ Funny Memes -->
    </main>

@endsection

@section('footerScript')
    <script>
        $(function () {
            console.log("sdgsdg");
            $("[data-fancybox]").fancybox();
        });
    </script>
@endsection































