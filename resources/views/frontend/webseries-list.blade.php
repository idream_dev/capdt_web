@extends('layouts.app')
@section('title', $title)
@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-4">
                            <article class="pagetitle">
                                <h1>Web Series </h1>
                                <p> Web Serieses of CAPDT team.</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-8 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="{{route('WebseriesList')}}">Web Series</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->

            <section class="subpagebody">
                <div class="container">

                    @if(sizeof($webseries)>0)
                    <div class="row py-4">
                        @foreach($webseries as $webseriesInfo)
                        <div class="col-lg-3 col-sm-4">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="{{route('WebseriesView',['alias'=>$webseriesInfo->w_alias])}}"> <img src="uploads/webseries/{{$webseriesInfo->w_poster}}" alt="{{$webseriesInfo->w_title}}" title="{{$webseriesInfo->w_title}}" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="{{route('WebseriesView',['alias'=>$webseriesInfo->w_alias])}}">{{$webseriesInfo->w_title}}</a>
                                    <p class="features"><span>({{ $webseriesInfo->WebseriesWiseVideos->count() }} Episodes)</span> <span class="float-right"><a href="{{route('WebseriesView',['alias'=>$webseriesInfo->w_alias])}}">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @else
                    <div class="row py-4">There is no Webseries yet</div>
                    @endif

                    <div class="row pb-3">
                        <div class="col-lg-12">
                            {!! $webseries->appends(['filters' => Request::get('filters')])->render() !!}
                        </div>
                    </div>
                    <!--/ row -->
                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection































