@extends('layouts.app')
@section('title', $title)



@section('css')

    <style>

    </style>

@endsection
@section('content')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
            src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2&appId=361437837778249"></script>
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-left ">
                            <ul class="nav brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li>
                                    <a href="{{route('Channels',['alias'=>$channelInfo->ch_customUrl])}}">{{$channelInfo->ch_title}}</a>
                                </li>
                                <li><a>{{$videos->yt_title}}</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left article col -->
                        <div class="col-lg-12">
                            <div class="articledetail">
                                <h4 class="h5 py-3 border-bottom">{{$videos->yt_title}}</h4>
                                <!-- row -->
                                <div class="row">
                                    <!-- col-4 -->
                                    <div class="col-lg-4 col-sm-4">
                                        <p class="themecolor dtnote"><span>{{$videos->yt_time_uploaded}}</span></p>
                                    </div>
                                    <!-- col-4 -->
                                    <!-- col-8-->
                                    <div class="col-lg-8 col-sm-8 text-right">
                                        <div class="detailsocial float-right">
                                            <p class="float-left pr-2">Recommend to your friends</p>
                                            <ul class="float-left nav">
                                                <li class="nav-item"><a href="javascript:void(0)"><img
                                                                src="/site/images/socialfb.jpg"></a></li>
                                                <li class="nav-item"><a href="javascript:void(0)"><img
                                                                src="/site/images/socialtwitter.jpg"></a></li>
                                                <li class="nav-item"><a href="javascript:void(0)"><img
                                                                src="/site/images/socialutube.jpg"></a></li>
                                            </ul>

                                        </div>
                                    </div>
                                    <!--/ col-8-->
                                </div>
                                <!--/ row -->
                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-12 fbcomment-box">
                                        <iframe width="100%" height="450" style="width:100%!important"
                                                src="https://www.youtube.com/embed/{{$videos->yt_video_id}}"
                                                frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="captionin">
                                                    <h2 class="slidertitle py-2"><a
                                                                href="articledetail.php">{{$videos->yt_title}}</a></h2>
                                                    <p>{{$videos->yt_description}}</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--/row -->
                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 commentsmain">                                        <!-- comment box -->

                                        <div class="fb-comments"
                                             data-href="https://developers.facebook.com/docs/plugins/comments#configurator"
                                             data-width="100%" data-numposts="10" data-colorscheme="light"></div>
                                        <!--/ comment box -->
                                    </div>
                                    <div class="col-lg-6 col-sm-6">
                                        <section class="list-episodes">
                                            @if(sizeof($channelvideos)>0)
                                                @foreach($channelvideos as $channelvideosInfo)
                                                    <div class="row pb-2 mb-2 seriesrow detail-list">
                                                        <div class="col-lg-3">
                                                            <figure class="seriesfig">
                                                                <a href="{{route('ChannelsvideoInfo',['alias'=>$channelInfo->ch_customUrl,'id'=>$channelvideosInfo->yt_video_id])}}"><img
                                                                            src="https://img.youtube.com/vi/{{$channelvideosInfo->yt_video_id}}/mqdefault.jpg"
                                                                            alt="{{$channelvideosInfo->yt_title}}"
                                                                            title="{{$channelvideosInfo->yt_title}}"
                                                                            class="img-fluid"></a>
                                                                <span class="time position-absolute">{{ getVideoDuration($channelvideosInfo->yt_video_duration) }}</span>
                                                            </figure>
                                                        </div>
                                                        <div class="col-lg-6 descseries">
                                                            <h6 class=""><a
                                                                        href="{{route('ChannelsvideoInfo',['alias'=>$channelInfo->ch_customUrl,'id'=>$channelvideosInfo->yt_video_id])}}">{{$channelvideosInfo->yt_title}}</a>
                                                            </h6>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <span class="viewtime">{{ \Carbon\Carbon::parse($channelvideosInfo->yt_time_uploaded)->diffForHumans() }} </span>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                {{'No Records Found'}}
                                            @endif
                                        </section>
                                    </div>
                                </div>
                                <!--/ row -->

                            </div>
                        </div>
                        <!--/ left article col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection































