@extends('layouts.app')
@section('title', $title)
@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-6">
                            <article class="pagetitle">
                                <h1>Capdt Diaries & Articles</h1>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-6 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="javascript:void(0)">Capdt Diaries & Articles</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <div class="container">
                    @if(sizeof($stories)>0)
                    <div class="row py-4">
                        @foreach($stories as $storiesInfo)
                        <div class="col-lg-3 col-sm-4">
                            <div class="listitem videolist">
                                <figure>
                                    <a href="{{route('StoryInfo',['alias'=>$storiesInfo->s_alias])}}">
                                        <img src="uploads/stories/{{$storiesInfo->s_poster}}" alt="{{$storiesInfo->s_title}}" title="{{$storiesInfo->s_title}}"class="img-fluid">
                                    </a>
                                </figure>
                                <article>
                                    <a href="{{route('StoryInfo',['alias'=>$storiesInfo->s_alias])}}">{{$storiesInfo->s_title}}</a>
                                </article>
                                <p class="comments-col">
                                    <a href="javascript:void(0)"><span class="icon-clock-o icomoon mr-1"></span>{{ \Carbon\Carbon::parse($storiesInfo->created_at)->diffForHumans() }}</a>
                                </p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @else
                        <div class="row py-4">There is no Stories yet</div>
                    @endif
                    <!--/ row -->
                    <!-- row -->
                    <div class="row pb-3">
                        <div class="col-lg-12">
                                {!! $stories->appends(['filters' => Request::get('filters')])->render() !!}
                        </div>
                    </div>
                    <!--/ row -->
                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection































