@extends('layouts.app')
@section('title', $title)
@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-6">
                            <article class="pagetitle">
                                <h1>Privacy Policy</h1>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-6 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="javascript:void(0)">Privacy Policy</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <div class="container">
                    Privacy Policy Coming Soon
                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection































