@extends('layouts.app')
@section('title', $title)
@section('metaheader')
    <!-- For Facebook -->
    <meta property="og:title" content="{{$storyinfo->s_title}}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{url('/uploads/stories/'.$storyinfo->s_poster)}}"/>
    <meta property="og:url" content="{{route('StoryInfo',['alias'=>$storyinfo->s_alias])}}"/>
    {{--<meta property="og:description" content="{!! $storyinfo->s_description !!}" />--}}

    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="{{$storyinfo->s_title}}"/>
    {{--<meta name="twitter:description" content="{!! $storyinfo->s_description !!}" />--}}
    <meta name="twitter:image" content="{{url('/uploads/stories/'.$storyinfo->s_poster)}}"/>

@endsection

@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-6">
                            <article class="pagetitle">
                                <h1>Capdt Diaries & Articles </h1>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-6 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="{{route('Stories')}}">Article</a></li>
                                <li><a href="javascript:void(0)">{{$storyinfo->s_title}}</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left article col -->
                        <div class="col-lg-9">
                            <div class="articledetail">
                                <h4 class="h5 py-3 border-bottom">{{$storyinfo->s_title}}</h4>
                                <!-- row -->
                                <div class="row">
                                    <!-- col-4 -->
                                    <div class="col-lg-4 col-sm-3">
                                        <p class="themecolor dtnote"><span>{{$storyinfo->created_at}}</span></p>
                                        @if(isset($storyinfo->getAuthor))
                                            <p class="themecolor dtnote">Author : <span><a
                                                        href="{{route('authorProfile',['id'=>$storyinfo->s_user_id])}}">{{$storyinfo->getAuthor->name}}</a></span>
                                            </p>
                                        @endif
                                    </div>
                                    <!-- col-4 -->
                                    <!-- col-8-->
                                    <div class="col-lg-8 col-sm-9 text-right">
                                        <div class="detailsocial float-right">
                                            <p class="float-left pr-2">Recommend to your friends</p>
                                            <div class="sharethis-inline-share-buttons"></div>
                                            {{--<ul class="float-left nav">--}}
                                            {{--<li class="nav-item"><a href="javascript:void(0)"><img src="/site/images/socialfb.jpg"></a></li>--}}
                                            {{--<li class="nav-item"><a href="javascript:void(0)"><img src="/site/images/socialgplus.jpg"></a></li>--}}
                                            {{--<li class="nav-item"><a href="javascript:void(0)"><img src="/site/images/socialtwitter.jpg"></a></li>--}}
                                            {{--<li class="nav-item"><a href="javascript:void(0)"><img src="/site/images/socialutube.jpg"></a></li>--}}
                                            {{--</ul>--}}

                                        </div>
                                    </div>
                                    <!--/ col-8-->
                                </div>
                                <!--/ row -->
                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <figure class="pb-2">
                                            <img src="/uploads/stories/{{$storyinfo->s_poster}}"
                                                 class="img-fluid w-100">
                                        </figure>
                                    </div>
                                </div>
                                <!--/row -->
                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-12 text-justify s_description">
                                        {!! $storyinfo->s_description !!}
                                    </div>
                                </div>
                                <!--/ row -->

                            </div>
                        </div>
                        <!--/ left article col -->
                        <!-- right article col -->
                        <div class="col-lg-3 col-sm-12">
                            <h2 class="pt-3">Related <span>News</span></h2>
                            <div class="row">
                                @if(sizeof($relatedstories)>0)
                                    @foreach($relatedstories as $relatedstoriesInfo)

                                        <div class="listitem col-lg-12 col-sm-4">
                                            <figure>
                                                <a href="{{route('StoryInfo',['alias'=>$relatedstoriesInfo->s_alias])}}">
                                                    <img src="/uploads/stories/{{$relatedstoriesInfo->s_poster}}" alt=""
                                                         title="" class="img-fluid">
                                                </a>
                                            </figure>
                                            <article>
                                                <a href="{{route('StoryInfo',['alias'=>$relatedstoriesInfo->s_alias])}}">{{$relatedstoriesInfo->s_title}}</a>
                                            </article>
                                        </div>

                                    @endforeach
                                @else
                                    <div class="col-lg-12">
                                        <div class="listitem ">
                                            <p class="d-block text-center pb-0">No Stories Yet</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <!--/ right article col -->

                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection












































































