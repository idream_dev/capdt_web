@extends('layouts.app')
@section('title', $title)
@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-8">
                            <article class="pagetitle">
                                <h1>About us</h1>
                                <p> Lorem Ipsum is simply dummy text of the printing and   typesetting industry.</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-4 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="index.php">Home</a></li>
                                <li><a>About us</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-6 tnone">
                            <img src="/site/images/about01.jpg" class="img-fluid" alt="" title="">
                        </div>
                        <div class="col-lg-6 col-sm-12 align-self-center">
                            <h2><span>CAPDT is the pioneers in Telugu Social Media Entertainment. </span></h2>
                            <p>Across platforms we offer services and solutions to Production Houses and Brands by selectively targeting our content and marketing that fulfils their requirement to ensure customer delight. With presence in all platforms and reaching more than 10 million audience per week, we cover a significant base of the Andhra Telangana Regions & also having a strong foothold in Karnataka.</p>
                            <p>CAPDT is also an OTT content creator looking to make it’s way to provide great vernacular digital content to platforms.</p>
                        </div>
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row py-4">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-6">
                            <div class="highlate">
                                <div class="row ">
                                    <div class="col-lg-4 col-sm-4 text-center ">
                                        <img class="svghigh" src="/site/images/svg/video-player.svg">
                                    </div>
                                    <div class="col-lg-8 col-sm-8 align-self-center text-center-mob">
                                        <h3>Channels</h3>
                                        <p class="pb-0">18+</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-4 col-sm-6">
                            <div class="highlate">
                                <div class="row ">
                                    <div class="col-lg-4 col-sm-4 text-center ">
                                        <img class="svghigh" src="/site/images/svg/video-player.svg">
                                    </div>
                                    <div class="col-lg-8 col-sm-8 align-self-center text-center-mob">
                                        <h3>Short films</h3>
                                        <p class="pb-0">200+</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-4 col-sm-6">
                            <div class="highlate">
                                <div class="row ">
                                    <div class="col-lg-4 col-sm-4 text-center ">
                                        <img class="svghigh" src="/site/images/svg/video-player.svg">
                                    </div>
                                    <div class="col-lg-8 col-sm-8 align-self-center text-center-mob">
                                        <h3>Episodes</h3>
                                        <p class="pb-0">100+</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row">
                        <!-- col-->
                        <div class="col-lg-6 col-sm-6">
                            <img src="/site/images/about02.jpg" class="img-fluid">
                            <h2 class="pb-0 pt-2">Our <span>Vision</span></h2>
                            <p>The Company's vision is to be a main Entertainment supplier through the advancement and conveyance of remarkable and quality Videos with assortment and stimulation to make and satisfy the way of life of all audions.</p>
                        </div>
                        <!--/ col -->
                        <!-- col-->
                        <div class="col-lg-6 col-sm-6">
                            <img src="/site/images/about03.jpg" class="img-fluid">
                            <h2 class="pb-0 pt-2">Our <span>Mission</span> </h2>
                            <p>Our objective at Capdt is to enable organizations to handle and vanquish the Content Creation, Entertainment scene. We have discovered that little and average sized organizations need the most help, which is the reason we offer a stage that makes Digital Creations simpler!</p>
                        </div>
                        <!--/ col -->

                    </div>
                    <!--/ row -->
                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection































