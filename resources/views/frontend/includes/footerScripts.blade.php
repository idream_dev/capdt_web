<!-- Scripts -->
<script src="{{ asset('site/js/siteScripts.js')}}" defer></script>
{{--<script src="//js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>--}}
<script type='text/javascript'
        src='//platform-api.sharethis.com/js/sharethis.js#property=5cfa663b4351e9001264faad&product=inline-share-buttons'
        async='async'></script>

<script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
    $(function () {

        $(".texteditor").each(function () {
            CKEDITOR.replace($(this).attr("name"));
        });


    })
</script>
