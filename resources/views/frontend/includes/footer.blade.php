<!--footer-->
<footer class="pt-4">
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-lg-5 col-sm-7">
                <div class="signcol">
                    <h6>Want to be a part of the team? Drop your email and Subscribe</h6>
                    <form method="POST" id="subscriptionForm" action="{{ route('subscriptionSave') }}"
                          accept-charset="UTF-8"
                          class="contactform">
                        {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-8 pr-0 col-8 col-sm-8">
                            <input type="text" name="email" placeholder="Enter Your Email" class="form-control">
                        </div>
                        <div class="col-lg-4 pl-0 col-4 col-sm-4">
                            <input type="submit" class="btn" value="Subscribe">
                            {{--<button type="submit" class="btn">Subscribe</button>--}}
                        </div>
                    </div>
                        <div class="subscriptionMessage"></div>
                    </form>
                </div>
            </div>
            <!--/ col -->
            <!-- col -->
            <div class="col-lg-7 col-sm-5 text-right align-self-center">
                <ul class="nav float-right">
                    <li class="nav-item"><a href="{{route('home')}}" class="nav-link">Home</a></li>
                    <li class="nav-item"><a href="{{route('aboutUs')}}" class="nav-link">About us</a></li>
                    <li class="nav-item"><a href="{{route('terms')}}" class="nav-link">Terms &amp; Conditions </a></li>
                    <li class="nav-item"><a href="{{route('privacyPolicy')}}" class="nav-link">Privacy Policy</a></li>
                    <li class="nav-item"><a href="{{route('contact')}}" class="nav-link">Contact</a></li>
                    <li class="nav-item"><a href="{{route('author_dashboard')}}" class="nav-link">Author Login</a></li>
                </ul>
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!-- copy rights -->
    <section class="copyrights mt-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <p>All copy rights reserved @capdtvideos2019</p>
                </div>
            </div>
        </div>
    </section>
    <!--/ copy rights -->
</footer>
<!--/ footer -->