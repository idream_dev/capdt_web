<!--header -->

<?php
$channels= getChannels();
$settings= getsettings();
?>

<header class="fixed-top">
    <div class="search">
        <div class="searchin">
            <input type="text" placeholder="Search Keywords">
            <a href="javascript:void(0)"><img src="/site/images/svg/close.svg" class="closeicon"></a>
        </div>
    </div>
    <div class="container">
        <nav id="topNav" class="navbar navbar-expand-md px-0">
            <a class="navbar-brand mx-auto" href="{{route('home')}}"><img src="/site/images/logo.svg" title="" alt=""></a>
            <a class="navbar-brand mx-auto" href="{{route('home')}}"><img src="{!! (!empty($settings) && $settings->s_logo) ? '/uploads/settings/'.$settings->s_logo : '/site/images/logo.svg' !!}" title="capdt.media" alt="capdt.media"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-menu-button icomoon"></span>
            </button>
            <div class="navbar-collapse collapse">
                <div class="dropdown">
                    <a href="javascript:void(0)"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="icon-bars icomoon"></span> CHANNELS & STORIES </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @if(sizeof($channels)>0)
                        @foreach($channels as $channel)
                        <a class="dropdown-item" href="{{route('Channels',['alias'=>$channel->ch_customUrl])}}">{{$channel->ch_title}}</a>
                        @endforeach
                        @endif
                        <a class="dropdown-item" href="{{route('Stories')}}">Diaries & Articles</a>
                    </div>
                </div>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item text-right">
                        <a class="nav-link border-right btn-search" href="javascript:void(0)" id="btnsearch" target="_blank"><span class="icon-search1 icomoon"></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{!! (!empty($settings) && $settings->s_facebook) ? $settings->s_facebook : 'https://www.facebook.com/CAPDT/' !!}" target="_blank"><span class="icon-facebook icomoon"></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{!! (!empty($settings) && $settings->s_instagram) ? $settings->s_instagram : 'https://www.instagram.com/capdt/' !!}" target="_blank"><span class="icon-instagram icomoon"></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{!! (!empty($settings) && $settings->s_twitter) ? $settings->s_twitter : 'https://twitter.com/CapdtOfficial' !!}" target="_blank"><span class="icon-twitter icomoon"></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{!! (!empty($settings) && $settings->s_youtube) ? $settings->s_youtube : 'https://www.youtube.com/channel/UCm2Q5o3WbfDbgVnLZFF9wLA' !!}" target="_blank"><span class="icon-youtube-play icomoon"></span></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!--/ header -->





{{--

<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    <li class="nav-item">
                        @if (Route::has('register'))
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>--}}
