<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    @include('admin.common.header')





    {{--<script src="assets/js/modernizr.min.js"></script>--}}
</head>


<body class="account-pages">

<!-- Begin page -->
<div class="accountbg"
     style="background: url('/sadmin/images/bg-1.jpg');background-size: cover;background-position: center;"></div>

<div class="wrapper-page account-page-full">

    <div class="card">
        <div class="card-block">

            <div class="account-box">

                <div class="card-box p-5">
                    <h2 class="text-uppercase text-center pb-4">
                        <a href="index.html" class="text-success">
                            <span><img src="/sadmin/images/logo.png" alt="" height="200"></span>
                        </a>
                    </h2>

                    <form method="POST" class="" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group m-b-20 row">
                            <div class="col-12">
                                <label class="label">{{ __('E-Mail Address') }}</label>
                                {{--<input class="form-control" type="email" id="emailaddress" required=""--}}
                                       {{--placeholder="Enter your email">--}}

                                <input id="email" type="email"
                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       name="email" value="{{ old('email') }}" required autofocus />

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                        <div class="form-group row m-b-20">
                            <div class="col-12">
{{--                                <a href="page-recoverpw.html" class="text-muted float-right">--}}
{{--                                    <small>Forgot your password?</small>--}}
{{--                                </a>--}}
                                <label class="label">{{ __('Password') }}</label>
                                <input id="password" type="password"
                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" required />

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row m-b-20">
                            <div class="col-12">

                                <div class="checkbox checkbox-custom">
                                    <input id="remember" type="checkbox" checked="">
                                    <label for="remember">
                                        Remember me
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group row text-center m-t-10">
                            <div class="col-12">
                                <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Sign
                                    In
                                </button>
                            </div>
                        </div>

                    </form>



                    {{--<div class="row m-t-50">--}}
                        {{--<div class="col-sm-12 text-center">--}}
                            {{--<p class="text-muted">Don't have an account? <a href="page-register.html"--}}
                                                                            {{--class="text-dark m-l-5"><b>Sign Up</b></a>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                </div>
            </div>

        </div>
    </div>

    <div class="m-t-40 text-center">
        <p class="account-copyright">{{date('Y')}} © capdt.media</p>
    </div>

</div>

<script src="/sadmin/js/scripts.js"></script>

<!-- jQuery  -->
{{--<script src="assets/js/jquery.min.js"></script>--}}
{{--<script src="assets/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="assets/js/metisMenu.min.js"></script>--}}
{{--<script src="assets/js/waves.js"></script>--}}
{{--<script src="assets/js/jquery.slimscroll.js"></script>--}}
{{----}}
{{--<!-- App js -->--}}
{{--<script src="assets/js/jquery.core.js"></script>--}}
{{--<script src="assets/js/jquery.app.js"></script>--}}

</body>
</html>


