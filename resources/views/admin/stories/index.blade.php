@extends('admin.layout')

@section('title', $title)

{{--@section('headersscript')--}}
{{--<script src="{{url('js/jquery.validate.min.js')}}"></script>--}}
{{--<script src="{{url('admin/modules/categories/js/webseries.js')}}"></script>--}}
{{--@endsection--}}

@section('headerstyle')
    <style>
        .editor_box div {
            width: 100% !important;
        }
    </style>
@endsection
@section('content')


    <div class="content">

        <div class="container-fluid">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('flash_message'))
                            <br/>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('flash_message' ) }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">


                    <a href="{{route('createStory')}}" class="btn btn-info pull-right">Add Story</a>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">Manage Stories</h4>

                                <?php
                                //                                dump($stories);
                                ?>


                                <table id="storiesList" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Poster</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($stories as $item)
                                        <?php
                                        //                                        dump($item);
                                        ?>

                                        <tr>
                                            <td>{{ $item->s_id }}</td>
                                            <td>{{ $item->s_title }}</td>
                                            <td>
                                                <a href="{{route('admin_authors')}}">
                                                    {{ (isset($item->getAuthor))? $item->getAuthor->name : '' }}
                                                </a>

                                            </td>
                                            <td>@if(isset($item->s_poster))
                                                    @php
                                                        $scode='';
                                                        $image= ($item->s_poster) ? '<img src="/uploads/stories/'.$item->s_poster.'" width="50px" />' : '-';
                                                    @endphp
                                                    @if($image!='-')
                                                        <div class="imagediv">
                                                            <a data-fancybox="" class="popupimages"
                                                               href="{{url('/uploads/stories/'.$item->s_poster)}}">
                                                                {!! $image !!}
                                                            </a>
                                                        </div>
                                                    @endif
                                                @endif</td>
                                            <td>{{ ($item->s_status==1)?"Active":"Inactive" }}</td>
                                            <td>


                                                <a href="{{route('createStory',['id'=>$item->s_id])}}" class=""  title="Edit Stories">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>&nbsp




                                                <a href="{{route('storyDelete')}}" class="btn-xs btn-default"
                                                   onclick="if (confirm('Confirm delete?')){ event.preventDefault(); document.getElementById('delete-form{{ $item->s_id }}').submit();}else{ return false; };">
                                                    <span class="fa fa-trash-o"></span>
                                                </a>

                                                <form id="delete-form{{ $item->s_id }}" method="POST"
                                                      action="{{route('storyDelete')}}"
                                                      accept-charset="UTF-8" style="display:none">
                                                    {{--                                                    {{ method_field('DELETE') }}--}}
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="s_id" value="{{ $item->s_id }}"/>
                                                    <button type="submit" class=""
                                                            title="Delete Webseries"
                                                            onclick="return confirm('Confirm delete')"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <div class="pagination-wrapper"
                                     style="float: right;"> {!! $stories->appends(['search' => Request::get('search')])->render() !!} </div>


                            </div>

                        </div>
                    </div>



                </div>


            </div>
        </div>


    </div>



@endsection

@section('footerScript')
    <!-- Required datatable js -->
    <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables/dataTables.bootstrap4.min.js"></script>

@endsection





