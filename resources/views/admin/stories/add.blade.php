@extends('admin.layout')

@section('title', $title)

{{--@section('headersscript')--}}
{{--<script src="{{url('js/jquery.validate.min.js')}}"></script>--}}
{{--<script src="{{url('admin/modules/categories/js/webseries.js')}}"></script>--}}
{{--@endsection--}}

@section('content')
    <div class="content">
        <!-- container fluid -->
        <div class="container-fluid">


            <!-- page sub content -->
            <div class="row m-b-40">


                <!-- profile view right -->
                <div class="col-lg-12 col-md-2">
                    <!-- white box -->
                    <div class="white-box">

                        <div class="row">
                            <div class="col-md-12">
                                @if (Session::has('flash_message'))
                                    <br/>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>{{ Session::get('flash_message' ) }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>


                        <!-- row -->
                        <div class="row">
                            <div class="col-lg-12">


                                <form method="POST" id="add_form" action="{{route('createStory')}}"
                                      accept-charset="UTF-8" class="stories_add_validation"
                                      enctype="multipart/form-data">

                                    {{ csrf_field() }}
                                    <input type="hidden" name="s_id" value="{{ $s_id }}">

                                    <div class="form-group {{ $errors->has('s_user_id') ? 'has-error' : ''}}">
                                        <label for="s_user_id" class="col-md-4 control-label"> Story Author </label>
                                        <div class="inpu-group">

                                            <?php
                                            $getAuthors = getAuthors()
                                            ?>

                                            <select name="s_user_id" class="form-control">
                                                <option value="">Select author</option>
                                                @if(count($getAuthors) > 0)
                                                    @foreach($getAuthors AS $getAuthor)
                                                        <option
                                                            value="{{$getAuthor->id}}" {{ (isset($story->s_user_id) && $story->s_user_id == $getAuthor->id ) ? 'selected' : ''}}> {{ $getAuthor->name }}</option>
                                                    @endforeach
                                                @endif


                                            </select>
                                            {!! $errors->first('s_user_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('s_title') ? 'has-error' : ''}}">
                                        <label for="s_title" class="col-md-4 control-label">{{ 'Story Title' }}</label>
                                        <div>
                                            <input class="form-control " name="s_title" type="text"
                                                   value="{{ $story ? $story->s_title : '' }}"
                                                   id="s_title" data-aliasbox="s_alias_new">
                                            {!! $errors->first('s_title', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('s_poster') ? 'has-error' : ''}}">
                                        <label for="s_poster" class="col-md-4 control-label">{{ 'Poster' }}</label>
                                        <div>
                                            <input
                                                class="form-control {{ $story && $story->s_poster!= '' ? '': 's_poster_add'}}"
                                                name="s_poster" type="file"
                                                id="s_poster">
                                            {!! $errors->first('s_poster', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    @if(isset($story->s_id))
                                        @php
                                            $scode='';
                                            $image= ($story->s_poster) ? '<img src="/uploads/stories/'.$story->s_poster.'" width="100px"  />' : '-';
                                        @endphp
                                        @if($image!='-')
                                            <div class="imagediv">
                                                <a data-fancybox=""
                                                   class="popupimages"
                                                   href="{{url('/uploads/stories/'.$story->s_poster)}}">
                                                    {!! $image !!}
                                                </a>
                                            </div>
                                        @endif
                                    @endif


                                    <div class="form-group {{ $errors->has('s_description') ? 'has-error' : ''}}">
                                        <label for="s_description"
                                               class="col-md-4 control-label">{{ 'Description' }}</label>
                                        <div>
        <textarea class="form-control texteditor" rows="20" name="s_description" type="textarea"
                  id="s_description">{{ $story ? $story->s_description : '' }}</textarea>
                                            {!! $errors->first('s_description', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('s_meta_keys') ? 'has-error' : ''}}">
                                        <label for="s_meta_keys"
                                               class="col-md-4 control-label">{{ 'Meta Keywords' }}</label>
                                        <div>
        <textarea class="form-control" rows="5" name="s_meta_keys" type="textarea"
                  id="s_meta_keys">{{ $story ? $story->s_meta_keys : '' }}</textarea>
                                            {!! $errors->first('s_meta_keys', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('s_status') ? 'has-error' : ''}}">
                                        <label for="s_status" class="col-md-4 control-label">{{ 'Status' }}</label>
                                        <div>
                                            <select name="s_status" class="form-control" id="s_status">
                                                @foreach (json_decode('{"1": "Active","0": "Inactive"}', true) as $optionKey => $optionValue)
                                                    <option
                                                        value="{{ $optionKey }}" {{ (isset($story->s_status) && $story->s_status == $optionKey ) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                                @endforeach
                                            </select>
                                            {!! $errors->first('s_status', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-offset-4 col-md-4">
                                            <input class="btn btn-primary" type="submit" value="Create">
                                        </div>
                                    </div>
                                </form>


                            </div>
                        </div>

                    </div>
                    <!--/white box -->
                </div>
                <!--/profile view right -->
            </div>
            <!--/page sub content -->
        </div>
        <!-- /container-fluid -->
    </div>

    <!--/ Modal Add Employee to Attendance Register -->
@endsection

@section('footerScript')





    <!-- Required datatable js -->
    <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables/dataTables.bootstrap4.min.js"></script>


    <script type="text/javascript">
        $(function () {


            $('.stories_add_validation').each(function () {
                $(this).validate({
                    ignore: [],
                    errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function (e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                    rules: {
                        s_user_id: {
                            required: true,
                        },
                        s_title: {
                            required: true,
                            minlength: 2,
                            maxlength: 99,
                        },
                        s_alias: {
                            required: true,
                            remote: function (element) {
                                return {
                                    url: '/admin/checkNewStoriesAlias',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: 'POST',
                                    data: {
                                        current_story_alias: function (form, event) {
                                            var formID = $(element).closest('form').attr('id');
                                            var currentAlias = $('#' + formID).find('.edit_current_stories_alias');
                                            return $(currentAlias).val();
                                        },
                                        type: function (form, event) {
                                            return 'edit';
                                        }
                                    }
                                }
                            }
                        },
                    },
                    messages: {
                        s_user_id: {
                            required: 'Select Author ',
                        },
                        s_title: {
                            required: 'Story title is required',
                            minlength: 'Your Category name must consist of at least {0} characters',
                            maxlength: 'Your Category name must consist of at least {0} characters'
                        },
                        s_alias: {
                            required: 'Alias is required',
                            remote: $.validator.format("This alias is already exist")
                        },
                    },
                });

            });

            posterimageRules();


            function posterimageRules() {
                $(".s_poster_add").rules("add", {
                    required: true,
                    accept: 'jpg,png,jpeg',
                    messages: {
                        required: "Upload Poster Image",
                        accept: "Please Upload jpg,png,jpeg file formats only",
                    }
                });
            }

            $("[data-fancybox]").fancybox();


        });

    </script>






@endsection
