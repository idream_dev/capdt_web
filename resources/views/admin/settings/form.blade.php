<div class="form-group {{ $errors->has('s_logo') ? 'has-error' : ''}}">
    <label for="s_logo" class="col-md-4 control-label">{{ 'Logo' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_logo[]" type="file" id="s_logo">
        <div class="label label-danger">Size: 88 * 89 px</div>
        {!! $errors->first('s_logo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(isset($settings->s_id))
    @php
        $scode='';
        $image= ($settings->s_logo) ? '<img src="/uploads/settings/'.$settings->s_logo.'" width="200px" height="200px" />' : '-';
    @endphp
    @if($image!='-')
        <div class="imagediv" style="margin-left:376px;">
            <a data-fancybox="" class="popupimages" href="{{url('/uploads/settings/'.$settings->s_logo)}}">
                {!! $image !!}
            </a>
        </div>
    @endif
@endif
<div class="form-group {{ $errors->has('s_url') ? 'has-error' : ''}}">
    <label for="s_url" class="col-md-4 control-label">{{ 'URL' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_url" type="text" id="s_url"
               value="{{ (isset($settings->s_url)) ? $settings->s_url : old('s_url')}}">
        {!! $errors->first('s_url', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_mobile') ? 'has-error' : ''}}">
    <label for="s_mobile" class="col-md-4 control-label">{{ 'Mobile' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_mobile" type="text" id="s_mobile"
               value="{{ (isset($settings->s_mobile)) ? $settings->s_mobile : old('s_mobile')}}">
        {!! $errors->first('s_mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_phone') ? 'has-error' : ''}}">
    <label for="s_phone" class="col-md-4 control-label">{{ 'Phone' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_phone" type="text" id="s_phone"
               value="{{ (isset($settings->s_phone)) ? $settings->s_phone : old('s_phone')}}">
        {!! $errors->first('s_phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_email') ? 'has-error' : ''}}">
    <label for="s_email" class="col-md-4 control-label">{{ 'Email' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_email" type="text" id="s_email"
               value="{{ (isset($settings->s_email)) ? $settings->s_email : old('s_email')}}">
        {!! $errors->first('s_email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_address') ? 'has-error' : ''}}">
    <label for="s_address" class="col-md-4 control-label">{{ 'Address' }}</label>
    <div class="col-md-6">
<textarea class="form-control" rows="5" name="s_address" type="textarea"
          id="s_address">{{ (isset($settings->s_address)) ? $settings->s_address : old('s_address') }}</textarea>
        {!! $errors->first('s_address', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_facebook') ? 'has-error' : ''}}">
    <label for="s_facebook" class="col-md-4 control-label">{{ 'Facebook' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_facebook" type="text" id="s_facebook"
               value="{{ (isset($settings->s_facebook)) ? $settings->s_facebook : old('s_facebook')}}">
        {!! $errors->first('s_facebook', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_twitter') ? 'has-error' : ''}}">
    <label for="s_twitter" class="col-md-4 control-label">{{ 'Twitter' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_twitter" type="text" id="s_twitter"
               value="{{ (isset($settings->s_twitter)) ? $settings->s_twitter : old('s_twitter')}}">
        {!! $errors->first('s_twitter', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_youtube_link') ? 'has-error' : ''}}">
    <label for="s_youtube_link" class="col-md-4 control-label">{{ 'Youtube link' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_youtube_link" type="text" id="s_youtube_link"
               value="{{(isset($settings->s_youtube_link)) ? $settings->s_youtube_link : old('s_youtube_link')}}">
        {!! $errors->first('s_youtube_link', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('s_instagram') ? 'has-error' : ''}}">
    <label for="s_instagram" class="col-md-4 control-label">{{ 'Instagram' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="s_instagram" type="text" id="s_instagram"
               value="{{ (isset($settings->s_instagram)) ? $settings->s_instagram : old('s_instagram')}}">
        {!! $errors->first('s_instagram', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<input class="btn btn-success mr-2" type="submit" value="{{ (isset($settings->s_id)) ? $submitButtonText : 'Create' }}">







