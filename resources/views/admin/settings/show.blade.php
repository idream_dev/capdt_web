@extends('admin.layout')

@section('title', $title)

@section('content')


    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-6 d-flex align-items-stretch grid-margin">
                <div class="row flex-grow">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">View Settings</h4>


                <a href="{{ url('/admin/settings') }}" title="Back">
                    <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </button>
                </a>
                <a href="{{ url('/admin/settings/' . $settings->s_id . '/edit') }}" title="Edit setting">
                    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                    </button>
                </a>


                    <table class="table table-borderless">
                        <tbody>
                        <tr>
                            <th>ID</th>
                            <td>{{ $settings->s_id }}</td>
                        </tr>
                        <tr>
                            <th>Logo</th>
                            <td>  @php
                                    $image= ($settings->s_logo) ? '<img src="/uploads/settings/'.$settings->s_logo.'" width="200px" height="200px"/>' : '-';
                                @endphp

                                <a data-fancybox="" class="popupimages" href="{{url('/uploads/settings/'.$settings->s_logo)}}">
                                    {!! $image !!}
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th>Mobile</th>
                            <td> {{ $settings->s_mobile }} </td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td> {{ $settings->s_phone }} </td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td> {{ $settings->s_email }} </td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td> {{ $settings->s_address }} </td>
                        </tr>

                        <tr>
                            <th>Facebook</th>
                            <td> {{ $settings->s_facebook }} </td>
                        </tr>
                        <tr>
                            <th>Twitter</th>
                            <td> {{ $settings->s_twitter }} </td>
                        </tr>
                        <tr>
                            <th>Youtube link</th>
                            <td> {{ $settings->s_youtube_link }} </td>
                        </tr>
                        <tr>
                            <th>Instagram</th>
                            <td> {{ $settings->s_instagram }} </td>
                        </tr>

                         </tbody>
                    </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection