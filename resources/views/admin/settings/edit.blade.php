@extends('admin.layout')

@section('title', $title)

@section('headersscript')

@endsection

@section('content')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-9 d-flex align-items-stretch grid-margin">
                <div class="row flex-grow">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-body">
                                <a href="{{ url('/admin/settings') }}" title="Back" style="float: right">
                                    <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </button>
                                </a>

                                <h4 class="card-title">Edit Settings</h4>



                                <form method="POST" id="settings_validation" action="{{ url('/admin/settings/' . $settings->s_id) }}"
                                      accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}

                                    @include ('admin.settings.form', ['submitButtonText' => 'Update'])

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection