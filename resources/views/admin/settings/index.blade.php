@extends('admin.layout')

@section('title', $title)

@section('content')


    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-9 d-flex align-items-stretch grid-margin">
                <div class="row flex-grow">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Manage Settings</h4>



    @if (count($settings)==0)
        <span class="pull-right">
        <a href="{{ url('/admin/settings/create') }}" class="btn btn-success btn-sm"
        title="Add New setting">
        <i class="fa fa-plus" aria-hidden="true"></i> Add New
        </a>
        </span>
    @endif
    <div class="clear"></div>
    @if (Session::has('flash_message'))
        <br/>
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>{{ Session::get('flash_message' ) }}</strong>
        </div>
    @endif

    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Logo</th>
            <th>URL</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($settings as $item)
            <tr>
                <td>{{ $loop->iteration or $item->s_id }}</td>
                <td>

                    @php
                        $image= ($item->s_logo) ? '<img src="/uploads/settings/'.$item->s_logo.'" width="200px" height="200px"/>' : '-';
                    @endphp

                    <a data-fancybox="" class="popupimages" href="{{url('/uploads/settings/'.$item->s_logo)}}">
                        {!! $image !!}
                    </a>
                </td>
                <td>{{ $item->s_url }}</td>
                <td>{{ $item->s_email }}</td>
                <td>{{ $item->s_mobile }}</td>
                <td>
                    <a href="{{ url('/admin/settings/' . $item->s_id) }}" title="View setting">
                        <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                               aria-hidden="true"></i> View
                        </button>
                    </a>
                    <a href="{{ url('/admin/settings/' . $item->s_id . '/edit') }}"
                       title="Edit setting">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                  aria-hidden="true"></i> Edit
                        </button>
                    </a>

                    {{--<form method="POST" action="{{ url('/settings' . '/' . $item->s_id) }}"--}}
                    {{--accept-charset="UTF-8" style="display:inline">--}}
                    {{--{{ method_field('DELETE') }}--}}
                    {{--{{ csrf_field() }}--}}
                    {{--<button type="submit" class="btn btn-danger btn-xs"--}}
                    {{--title="Delete setting"--}}
                    {{--onclick="return confirm(&quot;Confirm delete?&quot;)"><i--}}
                    {{--class="fa fa-trash-o" aria-hidden="true"></i> Delete--}}
                    {{--</button>--}}
                    {{--</form>--}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



