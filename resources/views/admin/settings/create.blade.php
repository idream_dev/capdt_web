@extends('admin.layout')

@section('title', $title)

@section('headersscript')

@endsection

@section('content')

    <div class="content-wrapper">
        <div class="col-lg-12 col-md-2">
            <div class="white-box">
                <div class="row">
                    <div class="col-lg-9">
            <div class="card">
            <div class="card-body">
                                    <h4 class="card-title">Add Settings</h4>
                                    {{--<p class="card-description">--}}
                                        {{--Basic form layout--}}
                                    {{--</p>--}}
                                    <form method="POST" id="settings_validation" action="{{ url('admin/settings') }}" accept-charset="UTF-8" class="" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        @include ('admin.settings.form')

                                    </form>
                                </div>
                        </div>


            </div>
                </div>
            </div>
        </div>
    </div>




@endsection