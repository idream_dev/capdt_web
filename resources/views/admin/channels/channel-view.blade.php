@extends('admin.layout')
@section('title', $title)

@section('headerstyle')
    <link rel="stylesheet" type="text/css" href="/plugins/datatables-new/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/plugins/datatables-new/bootstrap/dataTables.bootstrap4.css">
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {display:none;}

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endsection



@section('content')


    <div class="content">
        <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-9">
                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('flash_message' ) }}</strong>
                    </div>
                @endif
                <div class="box">
                    <div class="card card-box">
                        <div class="card-body ">
                            <strong> Channel Name : {{$channelsInfo->ch_title}} </strong>
                            <hr>
                            <br/>
                            <table id="dataTable_ChannelsList" class="display full-width">

                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box">
                    <div class="card card-box">
                        <div class="card-body">

                            <strong>Channel Info</strong>
                            <hr>
                            <br/>

                            <?php
                            $image = json_decode($channelsInfo->ch_thumbnails);
                            ?>
                            <p><a target='_blank' href='http://youtube.com/channel/{{$channelsInfo->ch_channel_id}}' title='{{$channelsInfo->ch_title}}' ><img width='50px' src='{{$image->high->url}}' title='{{$channelsInfo->ch_title}}' /></a></p>
                            <p><strong>Name</strong> : {{$channelsInfo->ch_title}}</p>
                            <p><strong>ID</strong> : {{$channelsInfo->ch_channel_id}}</p>
                            <p><strong>Published Date</strong> : {{ date('j M Y h:i a', strtotime($channelsInfo->ch_publishedAt)) }} </p>
                            <p><strong>Video Count</strong> : {{ $channelsInfo->channelWiseVideos->count() }}</p>
                            <p><strong>Subscribers Count</strong> : {{ $channelsInfo->ch_subscriberCount }}</p>
                        </div>
                    </div>
                </div>
            </div>




            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
    </div>




@endsection

@section('footerScript')

    <script src="/plugins/datatables-new/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables-new/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables-new/bootstrap/dataTables.bootstrap4.js"></script>

    <script>
        $(function () {

            var channels_list = $('#dataTable_ChannelsList').DataTable({
                "order": [[0, "desc"]],
                responsive: true,
                // "scrollX": true,
                // data: dataSet,
                "processing": true,
                "language":
                    {
                        "processing": "<img style='width:50px; height:50px; bottom: 50px;' src='/backend/images/ripple-loader.gif' />",
                    },
                "serverSide": true,
                "ajax": {
                    "url": "{{ route('DatatablesServersideVideos') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {cid:"{{$channelsInfo->ch_channel_id}}", _token: "{{csrf_token()}}"}
                },
                "rowCallback": function (row, data, index) {

                    $(row).find('.delete').on('click', function () {
                        console.log('asdasas');

                        var x = confirm("Are you sure you want to delete?");
                        if (x)   {

                            var videoId = $(this).data('vid');


                            $.ajax({
                                type: 'POST',
                                url: "{{route('VideoRemove')}}",
                                data: {vid: videoId, _token: "{{csrf_token()}}"},
                                success: function (data, resonce) {


                                }
                            });

                            var row = $(this).closest("tr").get(0);
                            row.remove();

                            // return true;
                        }


                        console.log($(this).data('cid'))
                    });




                },
                columns: [
                    {"data": "yt_vid", title: "id"},
                    {"data": "yt_video_id", title: "Video Id"},
                    {"data": "yt_title", title: "Title"},
                    {"data": "yt_time_uploaded", title: "Upload Time"},
                    {"data": "created_at", title: "Created Time"},
                    // {"data": "Trending", title: "Trending"},
                    {"data": "options", title: "Actions"}
                ]


            });


        })

        $(function () {
            $('#dataTable_ChannelsList #delete').on('click', function () {
                console.log('delete');


            });



        })


    </script>

@endsection