@extends('admin.layout')

@section('title', $title)



@section('content')


    <div class="content-wrapper">

        <div class="col-lg-12 col-md-2">
            <div class="white-box">

                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('flash_message'))
                            <br/>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('flash_message' ) }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">


                    <div class="col-lg-9">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">Manage Channels</h4>

                                <table id="channelsList" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Channel Name</th>
                                        <th>Channel ID</th>
                                        <th>Logo</th>
                                        <th>Channel Published Date</th>
                                        <th>Video Count</th>
                                        <th>Subscribers Count</th>
                                        <th>Status</th>
                                        <th>Update Videos</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($channels)>0)
                                    @foreach($channels as $item)
                                        <?php
                                        $image = json_decode($item->ch_thumbnails);
                                        ?>
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->ch_title }}</td>
                                            <td>{{ $item->ch_channel_id }}</td>
                                            <td><a target='_blank' href='http://youtube.com/channel/{{$item->ch_channel_id}}' title='{{$item->ch_title}}' ><img width='50px' src='{{$image->high->url}}' title='{{$item->ch_title}}' /></a></td>
                                            <td>{{ date('j M Y h:i a', strtotime($item->ch_publishedAt)) }} </td>
                                            <td>{{ $item->channelWiseVideos->count() }}</td>
                                            <td>{{ $item->ch_subscriberCount }}</td>
                                            <td>{{ ($item->ch_status==1)?"Active":"Inactive" }}</td>
                                            <td>
                                                @if($item->ch_channel_id!='')
                                                    <a href="{{route('channelVideosimport',['id' => $item->ch_channel_id])}}" class="" id="{{$item->ch_id}}"><i class="fa fa-youtube-play" aria-hidden="true"></i> Load Videos
                                                    </a>
                                                @endif
                                            </td>
                                            <td>

                                                <a href='{{route('channelShow', $item->ch_channel_id)}}' class='btn-xs btn-default' title='SHOW' ><span class='fa fa-eye'></span></a>&nbsp;
                                                <a href="javascript:void(0)" class="btn-xs btn-default" data-toggle="modal"
                                                   data-target="#editchannel{{$item->ch_id}}"
                                                   title="Edit Channel"><span class='fa fa-pencil-square-o'></span>
                                                </a>&nbsp;
                                                <div class="modal fade custommodal container-fluid"
                                                     id="editchannel{{$item->ch_id}}" tabindex="-1" role="model"
                                                     aria-labelledby="myModalLabel2">


                                                    <div class="modal-dialog modal-md" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal"
                                                                        aria-label="Close"><span
                                                                            aria-hidden="true">&times;</span>
                                                                </button>

                                                                <h4 class="modal-title"
                                                                    id="myModalLabel2">
                                                                    Edit Channel #{{$item->ch_id}}</h4>

                                                            </div>

                                                            <form method="post" class="channels_add_validation"
                                                                  id="editForm_{{$item->ch_id}}"
                                                                  enctype="multipart/form-data" action="{{route('editChannel')}}">
                                                                {{ csrf_field() }}

                                                                <div class="col-lg-12">
                                                                    <br/>
                                                                <input name="ch_id" type="hidden"
                                                                       id="ch_id"
                                                                       value="{{$item->ch_id}}"
                                                                       class="form-control current_channel_id">


                                                                <input type="hidden"
                                                                       class="edit_current_channel_id"
                                                                       value="{{ (isset($item->ch_channel_id)) ? $item->ch_channel_id : old('ch_channel_id')}}"/>
                                                                </div>


                                                                <div class="col-lg-12">
                                                                    <div class="form-group {{ $errors->has('ch_channel_id') ? 'has-error' : ''}}">
                                                                        <label for="ch_channel_id"
                                                                               class="col-md-4 control-label">{{ 'Channel ID' }}</label>
                                                                        <div>
                                                                            <input class="form-control"
                                                                                   name="ch_channel_id" type="text"
                                                                                   id="ch_channel_id"
                                                                                   value="{{ (isset($item->ch_channel_id)) ? $item->ch_channel_id : old('ch_channel_id')}}">
                                                                            {!! $errors->first('ch_channel_id', '<p class="help-block">:message</p>') !!}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-md-offset-4 col-md-4">
                                                                        <input class="btn btn-primary"
                                                                               type="submit" value="Update">
                                                                    </div>
                                                                </div>

                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>


                                                <a href="{{route('channelDelete')}}" class="btn-xs btn-default" onclick="if (confirm('Confirm delete?')){ event.preventDefault(); document.getElementById('delete-form{{ $item->ch_id }}').submit();}else{ return false; };">
                                                    <span class="fa fa-trash-o"></span>
                                                </a>


                                                <form id="delete-form{{ $item->ch_id }}" method="POST" action="{{route('channelDelete')}}"
                                                      accept-charset="UTF-8" style="display:none">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="ch_id" value="{{ $item->ch_id }}"/>
                                                    <input type="hidden" name="ch_channel_id" value="{{ $item->ch_channel_id }}"/>
                                                    <button type="submit" class=""
                                                            title="Delete Channel"
                                                            onclick="return confirm('Confirm delete')"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                    </button>
                                                </form>


                                            </td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr style="text-align: center">
                                            <td colspan="10">No Result Found</td>
                                        </tr>
                                    @endif
                                    </tbody>

                                </table>




                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Add New Channel</h4>
                                <form method="POST" id="add_form" action="{{route('createChannel')}}"
                                      accept-charset="UTF-8" class="channels_add_validation"
                                      enctype="multipart/form-data">

                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('ch_channel_id') ? 'has-error' : ''}}">
                                        <label for="ch_channel_id" class="col-md-4 control-label">{{ 'Channel ID' }}</label>
                                        <div>
                                            <input class="form-control" name="ch_channel_id" type="text" id="ch_channel_id">
                                            {!! $errors->first('ch_channel_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-offset-4 col-md-4">
                                            <input class="btn btn-primary" type="submit" value="Create">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>


            </div>
        </div>


    </div>



@endsection

@section('footerScript')
    <!-- Required datatable js -->
    <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/plugins/datatables/jszip.min.js"></script>
    <script src="/plugins/datatables/pdfmake.min.js"></script>
    <script src="/plugins/datatables/vfs_fonts.js"></script>
    <script src="/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Selection table -->
    <script src="/plugins/datatables/dataTables.select.min.js"></script>

    <script type="text/javascript">
        $(function () {




            $('.channels_add_validation').each(function () {
                $(this).validate({
                    ignore: [],
                    errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function (e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                    rules: {
                        ch_channel_id: {
                            required: true,
                            remote: function (element) {
                                return {
                                    url: '/admin/checkChannelID',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: 'POST',
                                    data: {
                                        current_channel_id: function (form, event) {
                                            var formID = $(element).closest('form').attr('id');
                                            var currentChannelID = $('#' + formID).find('.edit_current_channel_id');
                                            return $(currentChannelID).val();
                                        }
                                    }
                                }
                            }
                        },
                    },
                    messages: {
                        ch_channel_id : {
                            required: 'Channel ID is required',
                            remote: $.validator.format("This Channel is already exist")
                        },
                    },
                });

            });

            $('#channelsList').DataTable({"bPaginate": false});




        });

    </script>
@endsection





