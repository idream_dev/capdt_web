@extends('admin.layout')

@section('title', $title)



@section('content')


    <div class="content">

        <div class="container-fluid">
            <div class="col-12 ">

                <div class="row ">
                    <div class="col-md-12">
                        @if (Session::has('flash_message'))
                            <br/>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('flash_message' ) }}</strong>
                            </div>
                        @endif
                    </div>
                </div>


                <div class="card">
                    <div class="card-body">


                        <div class="row">
                            <div class="col-12">
                                <h4 class="card-title pull-left">Manage Authors</h4>


                                <button class="btn btn-info pull-right" data-toggle="modal" data-target="#createAuthor">
                                    Create Author
                                </button>

                            </div>


                            <div id="createAuthor" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">
                                                Create Author
                                            </h4>
                                        </div>
                                        <div class="modal-body">

                                            <form method="post" action="{{route('admin_authors')}}"
                                                  class="form-horizontal" enctype="multipart/form-data">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="action" value="new"/>
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="form-group">
                                                            <label>Name</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="name"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Email</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="email"/>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Mobile</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="mobile"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Image</label>
                                                            <div class="input-group">
                                                                <input type="file" class="" name="profile_image"/>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-info">Create Author
                                                                Now
                                                            </button>
                                                        </div>


                                                    </div>
                                                </div>
                                            </form>


                                        </div>
                                        {{--                                        <div class="modal-footer">--}}
                                        {{--                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                                        {{--                                        </div>--}}
                                    </div>

                                </div>
                            </div>

                        </div>
                        <br/>
                        <div class="row">


                            <div class="col-12">


                                <table id="bannerslist" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                    @if(count($authors)>0)
                                        @foreach($authors as $item)

                                            <?php
                                            //                                        dump($item);
                                            ?>

                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>
                                                    {{ $item->name }}
                                                </td>
                                                <td>
                                                    {{ $item->email }}
                                                </td>
                                                <td>
                                                    {{ $item->status }}
                                                </td>


                                                <td>

                                                    {{--                                                <a href='{{route('channelShow', $item->ch_channel_id)}}' class='btn-xs btn-default' title='SHOW' ><span class='fa fa-eye'></span></a>&nbsp;--}}

                                                    <a href="javascript:void(0)" class="" data-toggle="modal"
                                                       data-target="#viewAuthor_{{$item->id}}"
                                                       title="View Author"><i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>&nbsp

                                                    <div class="modal fade custommodal container-fluid"
                                                         id="viewAuthor_{{$item->id}}" tabindex="-1" role="model"
                                                         aria-labelledby="myModalLabel2">

                                                        <div class="modal-dialog modal-md" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-label="Close"><span
                                                                                aria-hidden="true">&times;</span>
                                                                    </button>

                                                                    <h4 class="modal-title"
                                                                        id="myModalLabel2">
                                                                        View Author #{{$item->id}}</h4>

                                                                </div>


                                                                <div class="modal-body">


                                                                    <div class="col-md-12">


                                                                        <div class="table-responsive">

                                                                            <table class="table table-borderless">
                                                                                <tbody>
                                                                                {{--                                                                                <tr>--}}
                                                                                {{--                                                                                    <th>ID</th>--}}
                                                                                {{--                                                                                    <td>{{ $item->id }}</td>--}}
                                                                                {{--                                                                                </tr>--}}

                                                                                <tr>
                                                                                    <th> Name</th>
                                                                                    <td>
                                                                                        {{ $item->name }}

                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <th>Email</th>
                                                                                    <td> {{ $item->email }} </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>Mobile</th>
                                                                                    <td> {{ $item->mobile }} </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <th> Image</th>
                                                                                    <td>
                                                                                        <?php
                                                                                        $profile_image = \App\User::getProfileImage($item->profile_image);
                                                                                        ?>
                                                                                        <div class="imagediv">
                                                                                            <a data-fancybox=""
                                                                                               class="popupimages"
                                                                                               href="{{ $profile_image }}">
                                                                                                <img src="{{$profile_image}}"
                                                                                                     class="img-fluid"/>
                                                                                            </a>
                                                                                        </div>


                                                                                    </td>
                                                                                </tr>


                                                                                <tr>
                                                                                    <th>Status</th>
                                                                                    <td> {{ ($item->status==1)?"Active":"Inactive" }} </td>
                                                                                </tr>

                                                                                </tbody>
                                                                            </table>


                                                                        </div>

                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>

                                                    </div>


                                                    <a href="javascript:void(0)" class="btn-xs btn-default"
                                                       data-toggle="modal"
                                                       data-target="#editAuthor_{{$item->id}}"
                                                       title="Edit Author"><span class='fa fa-pencil-square-o'></span>
                                                    </a>&nbsp;
                                                    <div class="modal fade custommodal container-fluid"
                                                         id="editAuthor_{{$item->id}}" tabindex="-1" role="model"
                                                         aria-labelledby="myModalLabel2">


                                                        <div class="modal-dialog modal-md" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-label="Close"><span
                                                                                aria-hidden="true">&times;</span>
                                                                    </button>

                                                                    <h4 class="modal-title"
                                                                        id="myModalLabel2">
                                                                        Edit Author #{{$item->id}}</h4>

                                                                </div>

                                                                <form method="post" class="banner_add_validation"
                                                                      id="editForm_{{$item->id}}"
                                                                      enctype="multipart/form-data"
                                                                      action="">
                                                                    {{ csrf_field() }}

                                                                    <input type="hidden" name="action" value="edit"/>
                                                                    <input type="hidden" name="id"
                                                                           value="{{$item->id}}"/>

                                                                    <div class="col-lg-12">


                                                                        <div class="col-lg-12">
                                                                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                                                                <label for="b_channel_name"
                                                                                       class="col-md-4 control-label">Name</label>
                                                                                <div>
                                                                                    <input class="form-control"
                                                                                           name="name"
                                                                                           type="text"
                                                                                           value="{{ (isset($item->name)) ? $item->name : old('name')}}"/>
                                                                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-lg-12">
                                                                            <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
                                                                                <label for="email"
                                                                                       class="col-md-4 control-label">mobile</label>
                                                                                <div>
                                                                                    <input class="form-control"
                                                                                           name="mobile" type="text"
                                                                                           id="mobile"
                                                                                           value="{{ (isset($item->mobile)) ? $item->mobile : old('mobile')}}">
                                                                                    {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-12">
                                                                            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                                                                <label for="email"
                                                                                       class="col-md-4 control-label">email</label>
                                                                                <div>
                                                                                    <input class="form-control"
                                                                                           name="email" type="text"
                                                                                           id="email"
                                                                                           value="{{ (isset($item->email)) ? $item->email : old('email')}}">
                                                                                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-12">
                                                                            <div class="form-group {{ $errors->has('profile_image') ? 'has-error' : ''}}">
                                                                                <label for="profile_image"
                                                                                       class="col-md-4 control-label">Profile
                                                                                    Image</label>
                                                                                <div>
                                                                                    <input class="form-control"
                                                                                           name="profile_image"
                                                                                           type="file"
                                                                                           id="profile_image">
                                                                                    {!! $errors->first('profile_image', '<p class="help-block">:message</p>') !!}
                                                                                </div>
                                                                            </div>

                                                                            <?php
                                                                            $profile_image = \App\User::getProfileImage($item->profile_image);
                                                                            ?>
                                                                            <div class="imagediv">
                                                                                <a data-fancybox=""
                                                                                   class="popupimages"
                                                                                   href="{{ $profile_image }}">
                                                                                    <img src="{{$profile_image}}"
                                                                                         class="img-fluid"/>
                                                                                </a>
                                                                            </div>

                                                                        </div>


                                                                        <div class="col-lg-12">
                                                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                                                                                <label for="status"
                                                                                       class="col-md-4 control-label">status</label>
                                                                                <div>
                                                                                    <select name="status"
                                                                                            class="form-control"
                                                                                            id="status">
                                                                                        @foreach (json_decode('{"1": "Active","2": "Inactive"}', true) as $optionKey => $optionValue)
                                                                                            <option value="{{ $optionKey }}" {{ (isset($item->status) && $item->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-4 col-md-4">
                                                                                <input class="btn btn-primary"
                                                                                       type="submit" value="Update Now">
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </form>

                                                            </div>
                                                        </div>
                                                    </div>


{{--                                                    <a href="{{route('BannerDelete')}}" class="btn-xs btn-default"--}}
{{--                                                       onclick='if (confirm("Confirm delete?")){ event.preventDefault(); document.getElementById("delete-form{{ $item->b_id }}").submit();}else{ return false; };'>--}}
{{--                                                        <span class="fa fa-trash-o"></span>--}}
{{--                                                    </a>--}}


                                                    <form id="delete-form{{ $item->id }}" method="POST"
                                                          action=""
                                                          accept-charset="UTF-8" >
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="action" value="remove"/>
                                                        <input type="hidden" name="id" value="{{ $item->id }}"/>
                                                        <button type="submit" class="btn btn-info"
                                                                title="Delete Banner"
                                                                onclick="return confirm('Confirm delete')"><i
                                                                    class="fa fa-trash-o" aria-hidden="true"></i>

                                                        </button>
                                                    </form>


                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr style="text-align: center">
                                            <td colspan="10">No Result Found</td>
                                        </tr>
                                    @endif
                                    </tbody>

                                </table>
                                <div class="pagination-wrapper"
                                     style="float: right;"> {!! $authors->appends(['search' => Request::get('search')])->render() !!}
                                </div>


                            </div>


                        </div>


                    </div>
                </div>


            </div>
        </div>


    </div>



@endsection

@section('footerScript')


    <script type="text/javascript">
        $(function () {


            $('.banner_add_validation').each(function () {
                $(this).validate({
                    ignore: [],
                    errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function (e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                    rules: {
                        b_title: {
                            required: true,
                            minlength: 2,
                            maxlength: 99,
                        }
                    },
                    messages: {
                        b_title: {
                            required: 'Banner title is required',
                            minlength: 'Your Category name must consist of at least {0} characters',
                            maxlength: 'Your Category name must consist of at least {0} characters'
                        }
                    },
                });

            });

            bannerimageRules();


            function bannerimageRules() {
                $(".b_banner_add").rules("add", {
                    required: true,
                    accept: 'jpg,png,jpeg',
                    messages: {
                        required: "Upload Banner Image",
                        accept: "Please Upload jpg,png,jpeg file formats only",
                    }
                });
            }

            $("[data-fancybox]").fancybox();


        });

    </script>


@endsection





