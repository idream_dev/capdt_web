@extends('admin.layout')

@section('title', $title)



@section('content')


    <div class="content-wrapper">

        <div class="col-lg-12 col-md-2">
            <div class="white-box">

                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('flash_message'))
                            <br/>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('flash_message' ) }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">


                    <div class="col-lg-9">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">Manage Banners</h4>

                                <table id="bannerslist" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Banner Image</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($banners)>0)
                                    @foreach($banners as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->b_title }}</td>
                                            <td>@if(isset($item->b_banner))
                                            @php
                                                $scode='';
                                                $image= ($item->b_banner) ? '<img src="/uploads/banners/'.$item->b_banner.'" width="50px" />' : '-';
                                            @endphp
                                            @if($image!='-')
                                                <div class="imagediv">
                                                    <a data-fancybox="" class="popupimages"
                                                       href="{{url('/uploads/banners/'.$item->b_banner)}}">
                                                        {!! $image !!}
                                                    </a>
                                                </div>
                                            @endif
                                            @endif</td>
                                            <td>{{ ($item->b_status==1)?"Active":"Inactive" }}</td>

                                            <td>

{{--                                                <a href='{{route('channelShow', $item->ch_channel_id)}}' class='btn-xs btn-default' title='SHOW' ><span class='fa fa-eye'></span></a>&nbsp;--}}

                                                <a href="javascript:void(0)" class="" data-toggle="modal"
                                                   data-target="#viewbanner_{{$item->b_id}}"
                                                   title="View Banner"><i class="fa fa-eye" aria-hidden="true"></i>
                                                </a>&nbsp

                                                <div class="modal fade custommodal container-fluid"
                                                     id="viewbanner_{{$item->b_id}}" tabindex="-1" role="model"
                                                     aria-labelledby="myModalLabel2">

                                                    <div class="modal-dialog modal-md" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal"
                                                                        aria-label="Close"><span
                                                                            aria-hidden="true">&times;</span>
                                                                </button>

                                                                <h4 class="modal-title"
                                                                    id="myModalLabel2">
                                                                    View Banner #{{$item->b_id}}</h4>

                                                            </div>


                                                            <div class="modal-body">


                                                                <div class="col-md-12">


                                                                    <div class="table-responsive">

                                                                        <table class="table table-borderless">
                                                                            <tbody>
                                                                            <tr>
                                                                                <th>ID</th>
                                                                                <td>{{ $item->b_id }}</td>
                                                                            </tr>

                                                                            <tr>
                                                                                <th>Channel Name</th>
                                                                                <td>
                                                                                    @if(isset($item->b_channel_name))
                                                                                        {{ $item->b_channel_name }}
                                                                                    @else
                                                                                        {{ '---' }}
                                                                                    @endif
                                                                                </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <th>Title</th>
                                                                                <td> {{ $item->b_title }} </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <th>Banner Image</th>
                                                                                <td>@if(isset($item->b_banner))
                                                                                        @php
                                                                                            $scode='';
                                                                                            $image= ($item->b_banner) ? '<img src="/uploads/banners/'.$item->b_banner.'" width="50px" />' : '-';
                                                                                        @endphp
                                                                                        @if($image!='-')
                                                                                            <div class="imagediv">
                                                                                                <a data-fancybox="" class="popupimages" href="{{url('/uploads/banners/'.$item->b_banner)}}">
                                                                                                    {!! $image !!}
                                                                                                </a>
                                                                                            </div>
                                                                                        @endif
                                                                                    @endif</td>
                                                                            </tr>



                                                                            <tr>
                                                                                <th>Description</th>
                                                                                <td>
                                                                                    @if(isset($item->b_description))
                                                                                        {{ $item->b_description }}
                                                                                    @else
                                                                                        {{ '---' }}
                                                                                    @endif
                                                                                </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <th>Link</th>
                                                                                <td>
                                                                                    @if(isset($item->b_link))
                                                                                    {{ $item->b_link }}
                                                                                    @else
                                                                                    {{ '---' }}
                                                                                    @endif
                                                                                </td>
                                                                            </tr>


                                                                            <tr>
                                                                                <th>Status</th>
                                                                                <td> {{ ($item->b_status==1)?"Active":"Inactive" }} </td>
                                                                            </tr>

                                                                            </tbody>
                                                                        </table>


                                                                    </div>

                                                                </div>
                                                            </div>



                                                        </div>
                                                    </div>

                                                </div>



                                                <a href="javascript:void(0)" class="btn-xs btn-default" data-toggle="modal"
                                                   data-target="#editbanner{{$item->b_id}}"
                                                   title="Edit Meme"><span class='fa fa-pencil-square-o'></span>
                                                </a>&nbsp;
                                                <div class="modal fade custommodal container-fluid"
                                                     id="editbanner{{$item->b_id}}" tabindex="-1" role="model"
                                                     aria-labelledby="myModalLabel2">


                                                    <div class="modal-dialog modal-md" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal"
                                                                        aria-label="Close"><span
                                                                            aria-hidden="true">&times;</span>
                                                                </button>

                                                                <h4 class="modal-title"
                                                                    id="myModalLabel2">
                                                                    Edit Banner #{{$item->b_id}}</h4>

                                                            </div>

                                                            <form method="post" class="banner_add_validation"
                                                                  id="editForm_{{$item->b_id}}"
                                                                  enctype="multipart/form-data" action="{{route('editBanner')}}">
                                                                {{ csrf_field() }}

                                                                <div class="col-lg-12">
                                                                    <br/>
                                                                <input name="b_id" type="hidden"
                                                                       id="b_id"
                                                                       value="{{$item->b_id}}"
                                                                       class="form-control">

                                                                    <div class="col-lg-12">
                                                                        <div class="form-group {{ $errors->has('b_channel_name') ? 'has-error' : ''}}">
                                                                            <label for="b_channel_name"
                                                                                   class="col-md-4 control-label">{{ 'Channel Name' }}</label>
                                                                            <div>
                                                                                <input class="form-control"
                                                                                       name="b_channel_name" type="text"
                                                                                       id="b_channel_name"
                                                                                       value="{{ (isset($item->b_channel_name)) ? $item->b_channel_name : old('b_channel_name')}}">
                                                                                {!! $errors->first('b_channel_name', '<p class="help-block">:message</p>') !!}
                                                                            </div>
                                                                        </div>
                                                                    </div>




                                                                <div class="col-lg-12">
                                                                    <div class="form-group {{ $errors->has('b_title') ? 'has-error' : ''}}">
                                                                        <label for="b_title"
                                                                               class="col-md-4 control-label">{{ 'Title' }}</label>
                                                                        <div>
                                                                            <input class="form-control"
                                                                                   name="b_title" type="text"
                                                                                   id="b_title"
                                                                                   value="{{ (isset($item->b_title)) ? $item->b_title : old('b_title')}}">
                                                                            {!! $errors->first('b_title', '<p class="help-block">:message</p>') !!}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                    <div class="col-lg-12">
                                                                        <div class="form-group {{ $errors->has('b_banner') ? 'has-error' : ''}}">
                                                                            <label for="b_banner"
                                                                                   class="col-md-4 control-label">{{ 'Banner Image' }}</label>
                                                                            <div>
                                                                                <input class="form-control"
                                                                                       name="b_banner" type="file"
                                                                                       id="b_banner">
                                                                                {!! $errors->first('b_banner', '<p class="help-block">:message</p>') !!}
                                                                            </div>
                                                                        </div>
                                                                        @if(isset($item->b_id))
                                                                            @php
                                                                                $scode='';
                                                                                $image= ($item->b_banner) ? '<img src="/uploads/banners/'.$item->b_banner.'" width="100px"  />' : '-';
                                                                            @endphp
                                                                            @if($image!='-')
                                                                                <div class="imagediv">
                                                                                    <a data-fancybox=""
                                                                                       class="popupimages"
                                                                                       href="{{url('/uploads/banners/'.$item->b_banner)}}">
                                                                                        {!! $image !!}
                                                                                    </a>
                                                                                </div>
                                                                            @endif
                                                                        @endif
                                                                    </div>


                                                                    <div class="col-lg-12">
                                                                        <div class="form-group {{ $errors->has('b_description') ? 'has-error' : ''}}">
                                                                            <label for="b_description"
                                                                                   class="col-md-4 control-label">{{ 'Description' }}</label>
                                                                            <div>
                                                                                <textarea name="b_description" class="form-control" rows="5">{{ (isset($item->b_description)) ? $item->b_description : old('b_description')}}</textarea>

                                                                                {!! $errors->first('b_description', '<p class="help-block">:message</p>') !!}
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-lg-12">
                                                                        <div class="form-group {{ $errors->has('b_link') ? 'has-error' : ''}}">
                                                                            <label for="b_link"
                                                                                   class="col-md-4 control-label">{{ 'Link' }}</label>
                                                                            <div>
                                                                                <input class="form-control"
                                                                                       name="b_link" type="text"
                                                                                       id="b_link"
                                                                                       value="{{ (isset($item->b_link)) ? $item->b_link : old('b_link')}}">
                                                                                {!! $errors->first('b_link', '<p class="help-block">:message</p>') !!}
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-lg-12">
                                                                        <div class="form-group {{ $errors->has('b_status') ? 'has-error' : ''}}">
                                                                            <label for="b_status"
                                                                                   class="col-md-4 control-label">{{ 'Status' }}</label>
                                                                            <div>
                                                                                <select name="b_status"
                                                                                        class="form-control"
                                                                                        id="b_status">
                                                                                    @foreach (json_decode('{"1": "Active","0": "Inactive"}', true) as $optionKey => $optionValue)
                                                                                        <option value="{{ $optionKey }}" {{ (isset($item->b_status) && $item->b_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                {!! $errors->first('b_status', '<p class="help-block">:message</p>') !!}
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                <div class="form-group">
                                                                    <div class="col-md-offset-4 col-md-4">
                                                                        <input class="btn btn-primary"
                                                                               type="submit" value="Update">
                                                                    </div>
                                                                </div>

                                                                </div>

                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>


                                                <a href="{{route('BannerDelete')}}" class="btn-xs btn-default" onclick='if (confirm("Confirm delete?")){ event.preventDefault(); document.getElementById("delete-form{{ $item->b_id }}").submit();}else{ return false; };'>
                                                    <span class="fa fa-trash-o"></span>
                                                </a>


                                                <form id="delete-form{{ $item->b_id }}" method="POST" action="{{route('BannerDelete')}}"
                                                      accept-charset="UTF-8" style="display:none">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="b_id" value="{{ $item->b_id }}"/>
                                                    <button type="submit" class=""
                                                            title="Delete Banner"
                                                            onclick="return confirm('Confirm delete')"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                    </button>
                                                </form>



                                            </td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr style="text-align: center">
                                            <td colspan="10">No Result Found</td>
                                        </tr>
                                    @endif
                                    </tbody>

                                </table>
                                <div class="pagination-wrapper"
                                     style="float: right;"> {!! $banners->appends(['search' => Request::get('search')])->render() !!}
                                </div>




                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Add New Banner</h4>
                                <form method="POST" id="add_form" action="{{route('createBanner')}}"
                                      accept-charset="UTF-8" class="banner_add_validation"
                                      enctype="multipart/form-data">

                                    {{ csrf_field() }}

                                    <div class="form-group {{ $errors->has('b_channel_name') ? 'has-error' : ''}}">
                                        <label for="b_channel_name" class="control-label">{{ 'Channel Name' }}</label>
                                        <div>
                                            <input class="form-control" name="b_channel_name" type="text" id="b_channel_name">
                                            {!! $errors->first('b_channel_name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('b_title') ? 'has-error' : ''}}">
                                        <label for="b_title" class="control-label">{{ 'Title' }}</label>
                                        <div>
                                            <input class="form-control" name="b_title" type="text" id="b_title">
                                            {!! $errors->first('b_title', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('b_banner') ? 'has-error' : ''}}">
                                        <label for="b_banner" class=" control-label">{{ 'Banner Image' }}</label>
                                        <div>
                                            <input class="form-control b_banner_add" name="b_banner" type="file" id="b_banner">
                                            {!! $errors->first('b_banner', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('b_description') ? 'has-error' : ''}}">
                                        <label for="b_description" class="control-label">{{ 'Description' }}</label>
                                        <div>
                                            <textarea name="b_description" class="form-control" rows="5"></textarea>
                                            {!! $errors->first('b_description', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('b_link') ? 'has-error' : ''}}">
                                        <label for="b_link" class="control-label">{{ 'Link' }}</label>
                                        <div>
                                            <input class="form-control" name="b_link" type="text" id="b_link">
                                            {!! $errors->first('b_link', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('b_status') ? 'has-error' : ''}}">
                                        <label for="b_status" class="control-label">{{ 'Status' }}</label>
                                        <div>
                                            <select name="b_status" class="form-control" id="b_status">
                                                @foreach (json_decode('{"1": "Active","0": "Inactive"}', true) as $optionKey => $optionValue)
                                                    <option value="{{ $optionKey }}" >{{ $optionValue }}</option>
                                                @endforeach
                                            </select>
                                            {!! $errors->first('b_status', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-offset-4 col-md-4">
                                            <input class="btn btn-primary" type="submit" value="Create">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>


            </div>
        </div>


    </div>



@endsection

@section('footerScript')
    <!-- Required datatable js -->
    <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/plugins/datatables/jszip.min.js"></script>
    <script src="/plugins/datatables/pdfmake.min.js"></script>
    <script src="/plugins/datatables/vfs_fonts.js"></script>
    <script src="/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Selection table -->
    <script src="/plugins/datatables/dataTables.select.min.js"></script>

    <script type="text/javascript">
        $(function () {





            $('.banner_add_validation').each(function () {
                $(this).validate({
                    ignore: [],
                    errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function (e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                    rules: {
                        b_title: {
                            required: true,
                            minlength: 2,
                            maxlength: 99,
                        }
                    },
                    messages: {
                        b_title : {
                            required: 'Banner title is required',
                            minlength: 'Your Category name must consist of at least {0} characters',
                            maxlength: 'Your Category name must consist of at least {0} characters'
                        }
                    },
                });

            });

            bannerimageRules();


            function bannerimageRules() {
                $(".b_banner_add").rules("add", {
                    required: true,
                    accept: 'jpg,png,jpeg',
                    messages: {
                        required: "Upload Banner Image",
                        accept: "Please Upload jpg,png,jpeg file formats only",
                    }
                });
            }

            $("[data-fancybox]").fancybox();

            $('#bannerslist').DataTable({"bPaginate": false});


        });

    </script>


@endsection





