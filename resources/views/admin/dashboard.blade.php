@extends('admin.layout')
@section('title', $title)


@section('headerstyle')

@endsection



@section('content')

    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="header-title mb-4">CAPDT Overview</h4>

                        <div class="row">
                            <div class="col-sm-6 col-lg-6 col-xl-3">
                                <div class="card-box mb-0 widget-chart-two">


                                    <div class="widget-chart-two-content">
                                        <p class="text-muted mb-0 mt-2">
                                            Total Mems
                                        </p>
                                        <h3 class="">
                                            {{ count($mems) }}
                                        </h3>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6 col-xl-3">
                                <div class="card-box mb-0 widget-chart-two">

                                    <div class="widget-chart-two-content">
                                        <p class="text-muted mb-0 mt-2">
                                            Stories
                                        </p>
                                        <h3 class="">
                                            {{ $storiesCount }}
                                        </h3>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6 col-xl-3">
                                <div class="card-box mb-0 widget-chart-two">

                                    <div class="widget-chart-two-content">
                                        <p class="text-muted mb-0 mt-2">
                                            Videos
                                        </p>
                                        <h3 class="">
                                            {{ $videoscount }}
                                        </h3>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                    <!-- end row -->
                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-xl-8">
                <div class="card-box">
                    <h4 class="header-title mb-3">Latest Videos</h4>

                    <?php
                    //                    dump($videosLatest);
                    ?>

                    <div class="table-responsive">
                        <table class="table table-hover table-centered m-0">

                            <thead>
                            <tr>
                                <th>image</th>
                                <th>name</th>
                                <th>Duration</th>
                                <th>Upload Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($videosLatest)> 0)
                                @foreach($videosLatest AS $videos)
                                    <tr>
                                        <td>

                                            <?php
                                            $imageData = json_decode($videos->yt_thumbnails);

                                            //                                                                                        dump($videos);
                                            ?>
                                            @if (isset($imageData) )

                                                <a href="https://www.youtube.com/watch?v={{$videos->yt_video_id}}"
                                                   target="_blank">
                                                    <img src="{{ $imageData->default->url  }}" alt="contact-img"
                                                         title="contact-img" class="rounded-circle thumb-sm"/>
                                                </a>

                                            @endif


                                            {{--                                            {{  }}--}}


                                        </td>

                                        <td>
                                            <h5 class="m-0 font-weight-normal">
                                                <a href="https://www.youtube.com/watch?v={{$videos->yt_video_id}}"
                                                   target="_blank">
                                                    {{ $videos->yt_title }}
                                                </a>
                                            </h5>

                                        </td>

                                        <td>

                                            {{ getVideoDuration($videos->yt_video_duration) }}
                                        </td>

                                        <td>
                                            {{ $videos->yt_time_uploaded }}
                                        </td>


                                        <td>
                                            <a href="#" class="btn btn-sm btn-custom"><i class="mdi mdi-plus"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger"><i class="mdi mdi-minus"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif


                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="col-xl-4">
                <div class="card-box">
                    <h4 class="header-title mb-3">Latest Stories</h4>

                    <?php
                    //                    dump($videosLatest);
                    ?>

                    <div class="table-responsive">
                        <table class="table table-hover table-centered m-0">

                            <thead>
                            <tr>
                                <th>image</th>
                                <th>name</th>

                                <th>Upload Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($storiesLatest)> 0)
                                @foreach($storiesLatest AS $list)

                                    <tr>
                                        <td>

                                            <?php
                                            $imageDataUrl = \App\Models\Stories::getImage($list->s_poster);
                                            ?>
                                            @if (isset($imageDataUrl) )

                                                <a href="" target="_blank">
                                                    <img src="{{ $imageDataUrl  }}" alt="contact-img" title="contact-img" class="rounded-circle thumb-sm"/>
                                                </a>

                                            @endif

                                        </td>

                                        <td>
                                            <h5 class="m-0 font-weight-normal">

                                                {{ $list->s_title }}

                                            </h5>

                                        </td>


                                        <td>
                                            {{ $list->created_at }}
                                        </td>


                                        <td>
                                            <a href="#" class="btn btn-sm btn-custom"><i class="mdi mdi-plus"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger"><i class="mdi mdi-minus"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif


                            </tbody>
                        </table>
                    </div>
                </div>

            </div>


        </div>
        <!-- end row -->


    </div> <!-- container -->

    </div> <!-- content -->

@endsection


@section('footersscript')
    <script src="/admin/pages/jquery.dashboard.init.js"></script>
@endsection