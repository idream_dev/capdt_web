@extends('admin.layout')

@section('title', $title)



@section('content')


    <div class="content-wrapper">

        <div class="col-lg-12 col-md-2">
            <div class="white-box">

                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('flash_message'))
                            <br/>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('flash_message' ) }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">


                    <div class="col-lg-9">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">Manage Memes</h4>

                                <table id="memesList" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Meme</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($memes)>0)
                                    @foreach($memes as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->m_title }}</td>
                                            <td>@if(isset($item->m_meme))
                                            @php
                                                $scode='';
                                                $image= ($item->m_meme) ? '<img src="/uploads/memes/'.$item->m_meme.'" width="50px" />' : '-';
                                            @endphp
                                            @if($image!='-')
                                                <div class="imagediv">
                                                    <a data-fancybox="" class="popupimages"
                                                       href="{{url('/uploads/memes/'.$item->m_meme)}}">
                                                        {!! $image !!}
                                                    </a>
                                                </div>
                                            @endif
                                            @endif</td>
                                            <td>{{ ($item->m_status==1)?"Active":"Inactive" }}</td>

                                            <td>

{{--                                                <a href='{{route('channelShow', $item->ch_channel_id)}}' class='btn-xs btn-default' title='SHOW' ><span class='fa fa-eye'></span></a>&nbsp;--}}

                                                <a href="javascript:void(0)" class="" data-toggle="modal"
                                                   data-target="#viewmeme_{{$item->m_id}}"
                                                   title="View Meme"><i class="fa fa-eye" aria-hidden="true"></i>
                                                </a>&nbsp

                                                <div class="modal fade custommodal container-fluid"
                                                     id="viewmeme_{{$item->m_id}}" tabindex="-1" role="model"
                                                     aria-labelledby="myModalLabel2">

                                                    <div class="modal-dialog modal-md" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal"
                                                                        aria-label="Close"><span
                                                                            aria-hidden="true">&times;</span>
                                                                </button>

                                                                <h4 class="modal-title"
                                                                    id="myModalLabel2">
                                                                    View Meme #{{$item->m_id}}</h4>

                                                            </div>


                                                            <div class="modal-body">


                                                                <div class="col-md-12">


                                                                    <div class="table-responsive">

                                                                        <table class="table table-borderless">
                                                                            <tbody>
                                                                            <tr>
                                                                                <th>ID</th>
                                                                                <td>{{ $item->m_id }}</td>
                                                                            </tr>

                                                                            <tr>
                                                                                <th>Title</th>
                                                                                <td> {{ $item->m_title }} </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <th>Meme</th>
                                                                                <td>@if(isset($item->m_meme))
                                                                                        @php
                                                                                            $scode='';
                                                                                            $image= ($item->m_meme) ? '<img src="/uploads/memes/'.$item->m_meme.'" width="50px" />' : '-';
                                                                                        @endphp
                                                                                        @if($image!='-')
                                                                                            <div class="imagediv">
                                                                                                <a data-fancybox="" class="popupimages" href="{{url('/uploads/memes/'.$item->m_meme)}}">
                                                                                                    {!! $image !!}
                                                                                                </a>
                                                                                            </div>
                                                                                        @endif
                                                                                    @endif</td>
                                                                            </tr>



                                                                            <tr>
                                                                                <th>Status</th>
                                                                                <td> {{ ($item->m_status==1)?"Active":"Inactive" }} </td>
                                                                            </tr>

                                                                            </tbody>
                                                                        </table>


                                                                    </div>

                                                                </div>
                                                            </div>



                                                        </div>
                                                    </div>

                                                </div>



                                                <a href="javascript:void(0)" class="btn-xs btn-default" data-toggle="modal"
                                                   data-target="#editmeme{{$item->m_id}}"
                                                   title="Edit Meme"><span class='fa fa-pencil-square-o'></span>
                                                </a>&nbsp;
                                                <div class="modal fade custommodal container-fluid"
                                                     id="editmeme{{$item->m_id}}" tabindex="-1" role="model"
                                                     aria-labelledby="myModalLabel2">


                                                    <div class="modal-dialog modal-md" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal"
                                                                        aria-label="Close"><span
                                                                            aria-hidden="true">&times;</span>
                                                                </button>

                                                                <h4 class="modal-title"
                                                                    id="myModalLabel2">
                                                                    Edit Meme #{{$item->m_id}}</h4>

                                                            </div>

                                                            <form method="post" class="meme_add_validation"
                                                                  id="editForm_{{$item->m_id}}"
                                                                  enctype="multipart/form-data" action="{{route('editMeme')}}">
                                                                {{ csrf_field() }}

                                                                <div class="col-lg-12">
                                                                    <br/>
                                                                <input name="m_id" type="hidden"
                                                                       id="m_id"
                                                                       value="{{$item->m_id}}"
                                                                       class="form-control">




                                                                <div class="col-lg-12">
                                                                    <div class="form-group {{ $errors->has('m_title') ? 'has-error' : ''}}">
                                                                        <label for="m_title"
                                                                               class="col-md-4 control-label">{{ 'Meme Title' }}</label>
                                                                        <div>
                                                                            <input class="form-control"
                                                                                   name="m_title" type="text"
                                                                                   id="m_title"
                                                                                   value="{{ (isset($item->m_title)) ? $item->m_title : old('m_title')}}">
                                                                            {!! $errors->first('m_title', '<p class="help-block">:message</p>') !!}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                    <div class="col-lg-12">
                                                                        <div class="form-group {{ $errors->has('m_meme') ? 'has-error' : ''}}">
                                                                            <label for="m_meme"
                                                                                   class="col-md-4 control-label">{{ 'Meme Image' }}</label>
                                                                            <div>
                                                                                <input class="form-control"
                                                                                       name="m_meme" type="file"
                                                                                       id="m_meme">
                                                                                {!! $errors->first('m_meme', '<p class="help-block">:message</p>') !!}
                                                                            </div>
                                                                        </div>
                                                                        @if(isset($item->m_id))
                                                                            @php
                                                                                $scode='';
                                                                                $image= ($item->m_meme) ? '<img src="/uploads/memes/'.$item->m_meme.'" width="100px"  />' : '-';
                                                                            @endphp
                                                                            @if($image!='-')
                                                                                <div class="imagediv">
                                                                                    <a data-fancybox=""
                                                                                       class="popupimages"
                                                                                       href="{{url('/uploads/memes/'.$item->m_meme)}}">
                                                                                        {!! $image !!}
                                                                                    </a>
                                                                                </div>
                                                                            @endif
                                                                        @endif
                                                                    </div>

                                                                    <div class="col-lg-12">
                                                                        <div class="form-group {{ $errors->has('m_status') ? 'has-error' : ''}}">
                                                                            <label for="m_status"
                                                                                   class="col-md-4 control-label">{{ 'Status' }}</label>
                                                                            <div>
                                                                                <select name="m_status"
                                                                                        class="form-control"
                                                                                        id="m_status">
                                                                                    @foreach (json_decode('{"1": "Active","0": "Inactive"}', true) as $optionKey => $optionValue)
                                                                                        <option value="{{ $optionKey }}" {{ (isset($item->m_status) && $item->m_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                {!! $errors->first('m_status', '<p class="help-block">:message</p>') !!}
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                <div class="form-group">
                                                                    <div class="col-md-offset-4 col-md-4">
                                                                        <input class="btn btn-primary"
                                                                               type="submit" value="Update">
                                                                    </div>
                                                                </div>

                                                                </div>

                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>


                                                <a href="{{route('memeDelete')}}" class="btn-xs btn-default" onclick="if (confirm('Confirm delete?')){ event.preventDefault(); document.getElementById('delete-form{{ $item->m_id }}').submit();}else{ return false; };">
                                                    <span class="fa fa-trash-o"></span>
                                                </a>


                                                <form id="delete-form{{ $item->m_id }}" method="POST" action="{{route('memeDelete')}}"
                                                      accept-charset="UTF-8" style="display:none">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="m_id" value="{{ $item->m_id }}"/>
                                                    <button type="submit" class=""
                                                            title="Delete Meme"
                                                            onclick="return confirm('Confirm delete')"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                    </button>
                                                </form>



                                            </td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr style="text-align: center">
                                            <td colspan="10">No Result Found</td>
                                        </tr>
                                    @endif
                                    </tbody>

                                </table>
                                <div class="pagination-wrapper"
                                     style="float: right;"> {!! $memes->appends(['search' => Request::get('search')])->render() !!} </div>




                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Add New Meme</h4>
                                <form method="POST" id="add_form" action="{{route('createMeme')}}"
                                      accept-charset="UTF-8" class="meme_add_validation"
                                      enctype="multipart/form-data">

                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('m_title') ? 'has-error' : ''}}">
                                        <label for="m_title" class="control-label">{{ 'Meme Title' }}</label>
                                        <div>
                                            <input class="form-control" name="m_title" type="text" id="m_title">
                                            {!! $errors->first('m_title', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('m_meme') ? 'has-error' : ''}}">
                                        <label for="m_meme" class=" control-label">{{ 'Meme Image' }}</label>
                                        <div>
                                            <input class="form-control m_meme_add" name="m_meme" type="file" id="m_meme">
                                            {!! $errors->first('m_meme', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('m_status') ? 'has-error' : ''}}">
                                        <label for="m_status" class="control-label">{{ 'Status' }}</label>
                                        <div>
                                            <select name="m_status" class="form-control" id="m_status">
                                                @foreach (json_decode('{"1": "Active","0": "Inactive"}', true) as $optionKey => $optionValue)
                                                    <option value="{{ $optionKey }}" >{{ $optionValue }}</option>
                                                @endforeach
                                            </select>
                                            {!! $errors->first('m_status', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-offset-4 col-md-4">
                                            <input class="btn btn-primary" type="submit" value="Create">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>


            </div>
        </div>


    </div>



@endsection

@section('footerScript')
    <!-- Required datatable js -->
    <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/plugins/datatables/jszip.min.js"></script>
    <script src="/plugins/datatables/pdfmake.min.js"></script>
    <script src="/plugins/datatables/vfs_fonts.js"></script>
    <script src="/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Selection table -->
    <script src="/plugins/datatables/dataTables.select.min.js"></script>

    <script type="text/javascript">
        $(function () {

            $('.meme_add_validation').each(function () {
                console.log("finaltesting");
                $(this).validate({
                    ignore: [],
                    errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function (e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                    rules: {
                        // m_title: {
                        //     required: true,
                        //     minlength: 2,
                        //     maxlength: 99,
                        // }
                    },
                    messages: {
                        // m_title : {
                        //     required: 'Meme title is required',
                        //     minlength: 'Your Category name must consist of at least {0} characters',
                        //     maxlength: 'Your Category name must consist of at least {0} characters'
                        // }
                    },
                });

            });

            memeimageRules();


            function memeimageRules() {
                $(".m_meme_add").rules("add", {
                    required: true,
                    accept: 'jpg,png,jpeg',
                    messages: {
                        required: "Upload Poster Image",
                        accept: "Please Upload jpg,png,jpeg file formats only",
                    }
                });
            }

            $("[data-fancybox]").fancybox();

            $('#memesList').DataTable({"bPaginate": false});


        });

    </script>


@endsection





