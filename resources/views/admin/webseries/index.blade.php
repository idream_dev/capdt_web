@extends('admin.layout')

@section('title', $title)

@section('headersscript')
    <script src="{{url('js/jquery.validate.min.js')}}"></script>
    <script src="{{url('admin/modules/categories/js/webseries.js')}}"></script>
@endsection

@section('content')


    <div class="content-wrapper">

        <div class="col-lg-12 col-md-2">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('flash_message'))
                            <br/>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('flash_message' ) }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">


                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">Manage Webseries</h4>


                                <table id="webseriesList" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Alias</th>
                                        <th>Poster</th>
                                        <th>No of Videos</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($webseries as $item)
                                        <tr>
                                            <td>{{ $item->w_id }}</td>
                                            <td>{{ $item->w_title }}</td>
                                            <td>{{ $item->w_alias }}</td>
                                            <td>@if(isset($item->w_poster))
                                                    @php
                                                        $scode='';
                                                        $image= ($item->w_poster) ? '<img src="/uploads/webseries/'.$item->w_poster.'" width="50px" />' : '-';
                                                    @endphp
                                                    @if($image!='-')
                                                        <div class="imagediv">
                                                            <a data-fancybox="" class="popupimages"
                                                               href="{{url('/uploads/webseries/'.$item->w_poster)}}">
                                                                {!! $image !!}
                                                            </a>
                                                        </div>
                                                    @endif
                                                @endif</td>
                                            <td>{{ $item->WebseriesWiseVideos->count() }}</td>
                                            <td>{{ ($item->w_status==1)?"Active":"Inactive" }}</td>
                                            <td>


                                                <a href='{{route('WebseriesShow', $item->w_id)}}' class='btn-xs btn-default' title='SHOW' ><span class='fa fa-eye'></span></a>&nbsp





                                                <a href="javascript:void(0)" class="" data-toggle="modal" data-target="#editwebseries{{$item->w_id}}" title="Edit Webseries" >
                                                   <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>&nbsp

                                                <div class="modal fade custommodal container-fluid"
                                                     id="editwebseries{{$item->w_id}}" tabindex="-1" role="model"
                                                     aria-labelledby="myModalLabel2">


                                                    <div class="modal-dialog modal-md" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal"
                                                                        aria-label="Close"><span
                                                                            aria-hidden="true">&times;</span>
                                                                </button>

                                                                <h4 class="modal-title"
                                                                    id="myModalLabel2">
                                                                    Edit Webseries #{{$item->w_id}}</h4>

                                                            </div>

                                                            <div class="modal-body">


                                                                <div class="col-md-12">

                                                                    <form method="post" class="webseries_add_validation"
                                                                          id="editForm_{{$item->w_id}}"
                                                                          enctype="multipart/form-data" action="{{route('editWebseries')}}">
                                                                        {{ csrf_field() }}

                                                                        <input name="w_id" type="hidden"
                                                                               id="w_id"
                                                                               value="{{$item->w_id}}"
                                                                               class="form-control current_webseries_id">


                                                                        <input type="hidden"
                                                                               class="edit_current_webseries_alias"
                                                                               value="{{ (isset($item->w_alias)) ? $item->w_alias : old('w_alias')}}"/>


                                                                        <input type="hidden"
                                                                               class="edit_current_webseries_poster"
                                                                               value="{{ (isset($item->w_poster)) ? $item->w_poster : old('w_poster')}}"/>


                                                                        <div class="col-lg-12">
                                                                            <div class="form-group {{ $errors->has('w_title') ? 'has-error' : ''}}">
                                                                                <label for="w_title"
                                                                                       class="col-md-4 control-label">{{ 'Webseries Title' }}</label>
                                                                                <div>
                                                                                    <input class="form-control titleCreateAlias"
                                                                                           name="w_title" type="text"
                                                                                           id="w_title_edit"
                                                                                           data-aliasbox="w_alias_edit_{{$item->w_id}}"
                                                                                           value="{{ (isset($item->w_title)) ? $item->w_title : old('w_title')}}">
                                                                                    {!! $errors->first('w_title', '<p class="help-block">:message</p>') !!}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-12">
                                                                            <input type="hidden"
                                                                                   class="edit_current_webseries_alias"
                                                                                   value="{{ (isset($item->w_alias)) ? $item->w_alias : old('w_alias')}}"/>

                                                                            <div class="form-group {{ $errors->has('w_alias') ? 'has-error' : ''}}">
                                                                                <label for="w_alias"
                                                                                       class="col-md-4 control-label">{{ 'Alias' }}</label>
                                                                                <div>
                                                                                    <input class="form-control"
                                                                                           name="w_alias" type="text"
                                                                                           id="w_alias_edit_{{$item->w_id}}"
                                                                                           value="{{ (isset($item->w_alias)) ? $item->w_alias : old('w_alias')}}">
                                                                                    {!! $errors->first('w_alias', '<p class="help-block">:message</p>') !!}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-12">
                                                                            <div class="form-group {{ $errors->has('w_poster') ? 'has-error' : ''}}">
                                                                                <label for="w_poster"
                                                                                       class="col-md-4 control-label">{{ 'Poster' }}</label>
                                                                                <div>
                                                                                    <input class="form-control"
                                                                                           name="w_poster" type="file"
                                                                                           id="w_poster">
                                                                                    {!! $errors->first('w_poster', '<p class="help-block">:message</p>') !!}
                                                                                </div>
                                                                            </div>
                                                                            @if(isset($item->w_id))
                                                                                @php
                                                                                    $scode='';
                                                                                    $image= ($item->w_poster) ? '<img src="/uploads/webseries/'.$item->w_poster.'" width="100px"  />' : '-';
                                                                                @endphp
                                                                                @if($image!='-')
                                                                                    <div class="imagediv">
                                                                                        <a data-fancybox=""
                                                                                           class="popupimages"
                                                                                           href="{{url('/uploads/webseries/'.$item->w_poster)}}">
                                                                                            {!! $image !!}
                                                                                        </a>
                                                                                    </div>
                                                                                @endif
                                                                            @endif
                                                                        </div>


                                                                        <div class="col-lg-12">
                                                                            <div class="form-group {{ $errors->has('w_description') ? 'has-error' : ''}}">
                                                                                <label for="w_description"
                                                                                       class="col-md-4 control-label">{{ 'Description' }}</label>
                                                                                <div>
        <textarea class="form-control" rows="4" name="w_description" type="textarea"
                  id="w_description">{{ (isset($item->w_description)) ? $item->w_description : old('w_description')}}</textarea>
                                                                                    {!! $errors->first('w_description', '<p class="help-block">:message</p>') !!}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-12">
                                                                            <div class="form-group {{ $errors->has('w_meta_keys') ? 'has-error' : ''}}">
                                                                                <label for="w_meta_keys"
                                                                                       class="col-md-4 control-label">{{ 'Meta Keywords' }}</label>
                                                                                <div>
        <textarea class="form-control" rows="4" name="w_meta_keys" type="textarea"
                  id="w_meta_keys">{{ (isset($item->w_meta_keys)) ? $item->w_meta_keys : old('w_meta_keys')}}</textarea>
                                                                                    {!! $errors->first('w_meta_keys', '<p class="help-block">:message</p>') !!}
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-12">
                                                                            <div class="form-group {{ $errors->has('w_status') ? 'has-error' : ''}}">
                                                                                <label for="w_status"
                                                                                       class="col-md-4 control-label">{{ 'Status' }}</label>
                                                                                <div>
                                                                                    <select name="w_status"
                                                                                            class="form-control"
                                                                                            id="w_status">
                                                                                        @foreach (json_decode('{"1": "Active","0": "Inactive"}', true) as $optionKey => $optionValue)
                                                                                            <option value="{{ $optionKey }}" {{ (isset($item->w_status) && $item->w_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    {!! $errors->first('w_status', '<p class="help-block">:message</p>') !!}
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-4 col-md-4">
                                                                                <input class="btn btn-primary"
                                                                                       type="submit" value="Update">
                                                                            </div>
                                                                        </div>


                                                                    </form>

                                                                </div>


                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- modal-content -->
                                                </div>


                                                <a href="{{route('webseriesDelete')}}" class="btn-xs btn-default" onclick="if (confirm('Confirm delete?')){ event.preventDefault(); document.getElementById('delete-form{{ $item->w_id }}').submit();}else{ return false; };">
                                                    <span class="fa fa-trash-o"></span>
                                                </a>

                                                <form id="delete-form{{ $item->w_id }}" method="POST" action="{{route('webseriesDelete')}}"
                                                      accept-charset="UTF-8" style="display:none">
{{--                                                    {{ method_field('DELETE') }}--}}
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="w_id" value="{{ $item->w_id }}"/>
                                                    <button type="submit" class=""
                                                            title="Delete Webseries"
                                                            onclick="return confirm('Confirm delete')"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <div class="pagination-wrapper"
                                     style="float: right;"> {!! $webseries->appends(['search' => Request::get('search')])->render() !!} </div>


                            </div>

                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Add New Webseries</h4>
                                <form method="POST" id="add_form" action="{{route('createWebseries')}}"
                                      accept-charset="UTF-8" class="webseries_add_validation"
                                      enctype="multipart/form-data">

                                    {{ csrf_field() }}
                                    @include ('admin.webseries.form')
                                </form>
                            </div>
                        </div>
                    </div>


                </div>


            </div>
        </div>


    </div>



@endsection

@section('footerScript')
    <!-- Required datatable js -->
    <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/plugins/datatables/jszip.min.js"></script>
    <script src="/plugins/datatables/pdfmake.min.js"></script>
    <script src="/plugins/datatables/vfs_fonts.js"></script>
    <script src="/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Selection table -->
    <script src="/plugins/datatables/dataTables.select.min.js"></script>

    <script type="text/javascript">
        $(function () {


            $('.titleCreateAlias').on('input', function () {
                var aliasbox = $(this).data('aliasbox');
                var webseries_name = $(this).val();
                var alias = webseries_name.trim().replace(/[^a-zA-Z0-9]/g, '-').replace(/-{2,}/g, '-').toLowerCase();
                $('#' + aliasbox).val(alias);
            });




            $('.webseries_add_validation').each(function () {
                $(this).validate({
                    ignore: [],
                    errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function (e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                    rules: {
                        w_title: {
                            required: true,
                            minlength: 2,
                            maxlength: 99,
                        },
                        w_alias: {
                            required: true,
                            remote: function (element) {
                                return {
                                    url: '/admin/checkwebseriesAlias',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: 'POST',
                                    data: {
                                        current_webseries_alias: function (form, event) {
                                            var formID = $(element).closest('form').attr('id');
                                            var currentAlias = $('#' + formID).find('.edit_current_webseries_alias');
                                            return $(currentAlias).val();
                                        },
                                        type: function (form, event) {
                                            return 'edit';
                                        }
                                    }
                                }
                            }
                        },
                    },
                    messages: {
                        w_title : {
                            required: 'Webseries title is required',
                            minlength: 'Your Category name must consist of at least {0} characters',
                            maxlength: 'Your Category name must consist of at least {0} characters'
                        },
                        w_alias : {
                            required: 'Alias is required',
                            remote: $.validator.format("This alias is already exist")
                        },
                    },
                });

            });

            posterimageRules();


            function posterimageRules() {
                $(".w_poster_add").rules("add", {
                    required: true,
                    accept: 'jpg,png,jpeg',
                    messages: {
                        required: "Upload Poster Image",
                        accept: "Please Upload jpg,png,jpeg file formats only",
                    }
                });
            }

            $("[data-fancybox]").fancybox();

            $('#webseriesList').DataTable({"bPaginate": false});


        });

    </script>


@endsection





