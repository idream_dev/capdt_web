
<div class="form-group {{ $errors->has('w_title') ? 'has-error' : ''}}">
    <label for="w_title" class="col-md-4 control-label">{{ 'Webseries Title' }}</label>
    <div>
        <input class="form-control titleCreateAlias" name="w_title" type="text" id="w_title"  data-aliasbox="w_alias_new"
               value="{{ (isset($webseries->w_title)) ? $webseries->w_title : old('w_title')}}">
        {!! $errors->first('w_title', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('w_alias') ? 'has-error' : ''}}">
    <label for="w_alias" class="col-md-4 control-label">{{ 'Alias' }}</label>
    <div>
        <input class="form-control" name="w_alias" type="text" id="w_alias_new"
               value="{{ (isset($webseries->w_alias)) ? $webseries->w_alias : old('w_alias')}}">
        {!! $errors->first('w_alias', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('w_poster') ? 'has-error' : ''}}">
    <label for="w_poster" class="col-md-4 control-label">{{ 'Poster' }}</label>
    <div>
        <input class="form-control w_poster_add" name="w_poster" type="file" id="w_poster">
        {!! $errors->first('w_poster', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(isset($webseries->w_id))
    @php
        $scode='';
        $image= ($webseries->w_poster) ? '<img src="/uploads/webseries/'.$webseries->w_poster.'" width="200px" height="200px" />' : '-';
    @endphp
    @if($image!='-')
        <div class="imagediv">
            <a data-fancybox="" class="popupimages" href="{{url('/uploads/webseries/'.$webseries->w_poster)}}">
                {!! $image !!}
            </a>
        </div>
    @endif
@endif





<div class="form-group {{ $errors->has('w_description') ? 'has-error' : ''}}">
    <label for="w_description" class="col-md-4 control-label">{{ 'Description' }}</label>
    <div>
        <textarea class="form-control" rows="5" name="w_description" type="textarea"
                  id="w_description">{{ (isset($webseries->w_description)) ? $webseries->w_description : old('w_description')}}</textarea>
        {!! $errors->first('w_description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('w_meta_keys') ? 'has-error' : ''}}">
    <label for="w_meta_keys" class="col-md-4 control-label">{{ 'Meta Keywords' }}</label>
    <div>
        <textarea class="form-control" rows="5" name="w_meta_keys" type="textarea"
                  id="w_meta_keys">{{ (isset($webseries->w_meta_keys)) ? $webseries->w_meta_keys : old('w_meta_keys')}}</textarea>
        {!! $errors->first('w_meta_keys', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('w_status') ? 'has-error' : ''}}">
    <label for="w_status" class="col-md-4 control-label">{{ 'Status' }}</label>
    <div>
        <select name="w_status" class="form-control" id="w_status">
            @foreach (json_decode('{"1": "Active","0": "Inactive"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($webseries->w_status) && $webseries->w_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('w_status', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ (isset($webseries->w_id)) ? $submitButtonText : 'Create' }}">
    </div>
</div>
