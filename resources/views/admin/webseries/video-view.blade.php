@extends('admin.layout')
@section('title', $title)


@section('content')


    <div class="content mt-3">
        <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-9">
                <div class="box">
                    <div class="card">
                        <div class="card card-box">

                            <div class="card-body">

                                <div class="modal-header">

                                        <h5 class="card-box" style="font-weight: bold">Title
                                            : {{$videoInfo->snippet->title}}</h5>


                                </div>

                                <div class="modal-body">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <strong>Channel</strong> : {{$videoInfo->snippet->channelTitle}} <br/><br/>
                                            <strong>View Count</strong> : {{$videoInfo->statistics->viewCount}} <br/><br/>
                                            <strong>Publish Date</strong> : {{ date('j M Y h:i a', strtotime($videoInfo->snippet->publishedAt)) }} <br/><br/>
                                            <strong>Description</strong> : {{$videoInfo->snippet->description}}
                                        </div>
                                        <div class="col-md-6">
                                            <p class="card-text">
                                                <embed src="http://www.youtube.com/v/{{$videoInfo->id}}?version=3&amp;hl=en_US&amp;rel=0&amp;autohide=1&amp;autoplay=1" wmode="transparent" type="application/x-shockwave-flash"  width="560" height="349" allowfullscreen="true" title="Adobe Flash Player">
                                            </p>

                                            <a href="https://www.youtube.com/watch?v={{$videoInfo->id}}"
                                               title="{{$videoInfo->id}}" target="_blank"
                                               class="btn btn-primary">Go TO Youtube Video</a>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>




            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
    </div>




@endsection

