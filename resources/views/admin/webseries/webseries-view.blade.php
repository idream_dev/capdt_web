@extends('admin.layout')
@section('title', $title)

@section('headerstyle')
    <link rel="stylesheet" type="text/css" href="/plugins/datatables-new/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/plugins/datatables-new/bootstrap/dataTables.bootstrap4.css">
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {display:none;}

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endsection



@section('content')


    <div class="content">
        <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-9">
                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('flash_message' ) }}</strong>
                    </div>
                @endif
                <div class="box">
                    <div class="card card-box">
                        <div class="card-body ">
                            <strong> Webseries Title : {{$webseriesInfo->w_title}} </strong>
                            <hr>
                            <br/>
                            <table id="dataTable_ChannelsList" class="display full-width">

                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box">
                    <div class="card card-box">
                        <div class="card-body">

                            <strong>Webseries Info</strong>
                            <hr>
                            <br/>



                            <p><strong>Poster</strong> : @if(isset($webseriesInfo->w_poster))
                                @php
                                    $scode='';
                                    $image= ($webseriesInfo->w_poster) ? '<img src="/uploads/webseries/'.$webseriesInfo->w_poster.'" width="150px" />' : '-';
                                @endphp
                                @if($image!='-')
                                    <div class="imagediv">
                                        <a data-fancybox="" class="popupimages" href="{{url('/uploads/webseries/'.$webseriesInfo->w_poster)}}">
                                            {!! $image !!}
                                        </a>
                                    </div>
                                    @endif
                                    @endif</p>
                            <p><strong>Title</strong> : {{$webseriesInfo->w_title}}</p>
                            <p><strong>Video Count</strong> : {{ $webseriesInfo->WebseriesWiseVideos->count() }}</p>
                            <p><strong>Description</strong> : {{ $webseriesInfo->w_description }}</p>
                        </div>
                    </div>
                </div>
            </div>




            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
    </div>




@endsection

@section('footerScript')

    <script src="/plugins/datatables-new/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables-new/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables-new/bootstrap/dataTables.bootstrap4.js"></script>

    <script>
        $(function () {

            $("[data-fancybox]").fancybox();


            var channels_list = $('#dataTable_ChannelsList').DataTable({
                "order": [[0, "desc"]],
                responsive: true,
                // "scrollX": true,
                // data: dataSet,
                "processing": true,
                "language":
                    {
                        "processing": "<img style='width:50px; height:50px; bottom: 50px;' src='/backend/images/ripple-loader.gif' />",
                    },
                "serverSide": true,
                "ajax": {
                    "url": "{{ route('WebseriesVideos') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {wid:"{{$webseriesInfo->w_id}}", _token: "{{csrf_token()}}"}
                },
                "rowCallback": function (row, data, index) {

                    $(row).find('.delete').on('click', function () {
                        console.log('asdasas');

                        var x = confirm("Are you sure you want to delete from this webseries?");
                        if (x)   {

                            var videoId = $(this).data('vid');


                            $.ajax({
                                type: 'POST',
                                url: "{{route('WebseriesVideoRemove')}}",
                                data: {vid: videoId, _token: "{{csrf_token()}}"},
                                success: function (data, resonce) {


                                }
                            });

                            var row = $(this).closest("tr").get(0);
                            row.remove();

                            // return true;
                        }


                        console.log($(this).data('cid'))
                    });




                },
                columns: [
                    {"data": "yt_vid", title: "id"},
                    {"data": "yt_video_id", title: "Video Id"},
                    {"data": "yt_title", title: "Title"},
                    {"data": "yt_time_uploaded", title: "Upload Time"},
                    {"data": "created_at", title: "Created Time"},
                    // {"data": "Trending", title: "Trending"},
                    {"data": "options", title: "Actions"}
                ]


            });


        })

        $(function () {
            $('#dataTable_ChannelsList #delete').on('click', function () {
                console.log('delete');


            });



        })


    </script>

@endsection