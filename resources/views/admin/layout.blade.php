<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    @include('admin.common.header')
    @yield('headerstyle')
</head>
<body>
<!-- Begin page -->
<div id="wrapper">
    @include('admin.common.sidebar')
    <div class="content-page">
        @include('admin.common.topbar')
        @yield('content')
        @include('admin.common.footer')
    </div>
</div>
@include('admin.common.footerscripts')
@yield('footerScript')
</body>

</html>


<!-- Left side column. contains the logo and sidebar -->
