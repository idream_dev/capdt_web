<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    @yield('metaheader')
    @include('frontend.includes.headerStyle')
    @yield('css')

</head>


<body>

@include('frontend.includes.header')

@yield('content')



@include('frontend.includes.footer')
@include('frontend.includes.footerScripts')
@yield('foo_script')
</body>

</html>
