@extends('layouts.app')
@section('title', $title)
@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-8">
                            <article class="pagetitle">
                                <h1>Dashbord</h1>
                                <p> {{ \Illuminate\Support\Facades\Auth::user()->name }}</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-4 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Author Dashbord</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-2">
                            @include('author.includs.sidebar')
                        </div>

                        <div class="col-lg-10 right-author">

                            <?php
                            $authorInfo = \Illuminate\Support\Facades\Auth::user();
//                            dump($authorInfo);
                            $profile_image = \App\User::getProfileImage($authorInfo->profile_image);
                            ?>
                                <div class="text-right pb-3">
                                    <a href="{{route('authorprofileEdit',['id'=>$authorInfo->id])}}" class="btn btn-info "> Edit Profile </a>
                                </div>

                            <table class="table">
                                <tr>
                                    <td>image:</td>
                                    <td><img src="{{$profile_image}}" class="img-fluid author-img" /></td>
                                </tr>
                                <tr>
                                    <td>Name:</td>
                                    <td> {{ $authorInfo->name }}</td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td> {{ $authorInfo->email }} </td>
                                </tr>
                                <tr>
                                    <td>Mobile:</td>
                                    <td> {{ $authorInfo->mobile }} </td>
                                </tr>

                            </table>

                        </div>
                    </div>


                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection































