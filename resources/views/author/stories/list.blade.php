@extends('layouts.app')
@section('title', $title)
@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-8">
                            <article class="pagetitle">
                                <h1>Dashbord</h1>
                                <p> {{ \Illuminate\Support\Facades\Auth::user()->name }}</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-4 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Author Stories</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-2 ">
                            @include('author.includs.sidebar')
                        </div>
                        <div class="col-lg-10 right-author">
                            <div class="text-right pb-3">
                                <a href="{{route('authorStoriesManage')}}" class="btn btn-info "> Create Now </a>
                            </div>

                            <?php
                            //dump($stories);
                            ?>

                            <div class="row">
                                <div class="col-md-12">
                                    @if (Session::has('flash_message'))
                                        <br/>
                                        <div class="alert alert-success alert-dismissable">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>{{ Session::get('flash_message' ) }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>


                            <table class="table">
                                <thead>
                                <tr>
                                    <td>Title</td>
                                    <td>image</td>
                                    <td>status</td>
                                    <td align="right">action</td>
                                </tr>

                                </thead>
                                <tbody>
                                @if(count($stories)>0)
                                    @foreach($stories AS $storie)

                                        <tr>
                                            <td>
                                                {{ $storie->s_title }}
                                            </td>
                                            <td>

                                                <?php
                                                if (isset($storie->s_poster)) {
                                                    $imageurl = \App\Models\Stories::getImage($storie->s_poster);
                                                }
                                                ?>

                                                @if(isset($imageurl))
                                                    <img width="100" src="{{ $imageurl }}" class="img-fluid"/>
                                                @endif
                                            </td>

                                            <td>
                                                {{ storiesStatus($storie->s_status) }}
                                            </td>

                                            <td align="right">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view_{{ $storie->s_id }}">View</button>
                                                <a href="{{ route('authorStoriesManage', ['id'=>$storie->s_id]) }}" class="btn btn-primary float-right"> Edit </a>

                                                <form class="float-right mx-1" method="post" action="{{route('global_remove_stories')}}" onsubmit="return confirm('Do you really want to Remove This Article?');">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="s_id" value="{{ $storie->s_id }}"/>
                                                    <button class="btn btn-danger" type="submit"> Remove</button>
                                                </form>

                                                <!-- The Modal -->
                                                <div class="modal" id="view_{{ $storie->s_id }}">
                                                    <div class="modal-dialog modal-lg modal-author">
                                                        <div class="modal-content ">

                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">
                                                                    {{ $storie->s_title }}
                                                                </h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>

                                                            <!-- Modal body -->
                                                            <div class="modal-body storybody-author">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <img class="img-fluid" src="{{ \App\Models\Stories::getImage($storie->s_poster) }}"/>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12 py-3">
                                                                       <p> {!! $storie->s_description !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <p>Status : <span class="statusspan">{{ storiesStatus($storie->s_status) }}</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            {{----}}
                                                            {{--<div class="modal-footer">--}}
                                                                {{--<button type="button" class="btn btn-danger"--}}
                                                                        {{--data-dismiss="modal">Close--}}
                                                                {{--</button>--}}
                                                            {{--</div>--}}

                                                        </div>
                                                    </div>
                                                </div>


                                            </td>
                                        </tr>

                                    @endforeach
                                </tbody>

                                @endif


                            </table>


                        </div>
                    </div>


                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection































