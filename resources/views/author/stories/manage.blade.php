@extends('layouts.app')
@section('title', $title)

@section('css')

@endsection

@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-8">
                            <article class="pagetitle">
                                <h1>Dashbord</h1>
                                <p> {{ \Illuminate\Support\Facades\Auth::user()->name }}</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-4 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Author Dashbord</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-2 ">
                            @include('author.includs.sidebar')
                        </div>

                        <div class="col-lg-10 right-author">
                            <div class="row">
                                <div class="col-md-12">
                                    @if (Session::has('flash_message'))
                                        <br/>
                                        <div class="alert alert-success alert-dismissable">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>{{ Session::get('flash_message' ) }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <?php
                            // dump($item);
                            ?>

                            <form method="post" class="stories_add_validation"
                                  id="manage"
                                  enctype="multipart/form-data"
                                  action="{{route('authorStoriesManage')}}">
                                {{ csrf_field() }}

                                <input type="hidden" name="s_id" value="{{ $id }}"/>


                                <div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('s_title') ? 'has-error' : ''}}">
                                        <label for="s_title"
                                               class="col-md-4 control-label">{{ 'Story Title' }}</label>
                                        <div>
                                            <input class="form-control titleCreateAlias"
                                                   name="s_title" type="text"
                                                   id="s_title_edit"

                                                   value="{{ (isset($item->s_title)) ? $item->s_title : old('s_title')}}">
                                            {!! $errors->first('s_title', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('s_poster') ? 'has-error' : ''}}">
                                        <label for="s_poster"
                                               class="col-md-4 control-label">{{ 'Poster' }}</label>
                                        <div>
                                            <input class="form-control"
                                                   name="s_poster" type="file"
                                                   id="s_poster">
                                            {!! $errors->first('s_poster', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <?php

                                    if (isset($item->s_poster)) {
                                        $imageurl = \App\Models\Stories::getImage($item->s_poster);
                                    }

                                    ?>
                                    @if(isset($imageurl))
                                        <img class="img-fluid"
                                             src="{{$imageurl  }}"/>
                                    @endif
                                </div>


                                <div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('s_description') ? 'has-error' : ''}}">
                                        <label for="s_description" class="col-md-4 control-label">{{ 'Description' }}</label>
                                        <div>
                                            <textarea class="form-control texteditor" rows="4" name="s_description" type="textarea" id="s_description">
                                                {{ (isset($item->s_description)) ? $item->s_description : old('s_description')}}</textarea>
                                            {!! $errors->first('s_description', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>

                                {{-- <div class="col-lg-12">
                                     <div class="form-group {{ $errors->has('s_meta_keys') ? 'has-error' : ''}}">
                                         <label for="s_meta_keys"
                                                class="col-md-4 control-label">{{ 'Meta Keywords' }}</label>
                                         <div>
         <textarea class="form-control" rows="4" name="s_meta_keys" type="textarea"
                   id="s_meta_keys">{{ (isset($item->s_meta_keys)) ? $item->s_meta_keys : old('s_meta_keys')}}</textarea>
                                             {!! $errors->first('s_meta_keys', '<p class="help-block">:message</p>') !!}
                                         </div>
                                     </div>
                                 </div>--}}

                                <div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('s_status') ? 'has-error' : ''}}">
                                        <label for="s_status" class="col-md-4 control-label">{{ 'Status' }}</label>
                                        <div>
                                            <select name="s_status"
                                                    class="form-control"
                                                    id="s_status">
                                                @foreach (json_decode('{"1": "Active","0": "Inactive"}', true) as $optionKey => $optionValue)
                                                    <option value="{{ $optionKey }}" {{ (isset($item->s_status) && $item->s_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                                @endforeach
                                            </select>
                                            {!! $errors->first('s_status', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-4">
                                        <input class="btn btn-primary" type="submit" value="Update">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection



@section('foo_script')





@endsection


























