@extends('layouts.app')
@section('title', $title)

@section('css')

@endsection

@section('content')
    <main class="subpagemain">
        <!-- sub page -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 col-sm-8">
                            <article class="pagetitle">
                                <h1>Dashbord</h1>
                                <p> {{ \Illuminate\Support\Facades\Auth::user()->name }}</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 col-sm-4 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Author Dashbord</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!-- sub page body -->
            <section class="subpagebody">
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-2 ">
                            @include('author.includs.sidebar')
                        </div>

                        <div class="col-lg-10 right-author">
                            <div class="row">
                                <div class="col-md-12">
                                    @if (Session::has('flash_message'))
                                        <br/>
                                        <div class="alert alert-success alert-dismissable">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>{{ Session::get('flash_message' ) }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>


                            <div class="text-right pb-3">
                                <a href="{{route('author_dashboard')}}" class="btn btn-info "> Back </a>
                            </div>

                            <?php
                            // dump($item);
                            ?>

                            <form method="post" class="profile_edit_validation"
                                  id="manage"
                                  enctype="multipart/form-data"
                                  action="{{route('authorprofileEdit',['id'=>$profile->id])}}">
                                {{ csrf_field() }}

                                <input type="hidden" name="action" value="edit"/>
                                <input type="hidden" name="id" value="{{ (isset($profile->id)) ? $profile->id : old('id')}}"/>


                                <div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                        <label for="name"
                                               class="col-md-4 control-label">{{ 'Name' }}</label>
                                        <div>
                                            <input class="form-control" required
                                                   name="name" type="text"
                                                   id="name"

                                                   value="{{ (isset($profile->name)) ? $profile->name : old('name')}}">
                                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('profile_image') ? 'has-error' : ''}}">
                                        <label for="profile_image"
                                               class="col-md-4 control-label">{{ 'Image' }}</label>
                                        <div>
                                            <input class="form-control"
                                                   name="profile_image" type="file"
                                                   id="profile_image">
                                            {!! $errors->first('profile_image', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    @if(isset($profile->id))
                                        @php
                                            $scode='';
                                            $image= ($profile->profile_image) ? '<img src="/uploads/users/'.$profile->profile_image.'" width="100px"  />' : '-';
                                        @endphp
                                        @if($image!='-')
                                            <div class="imagediv">
                                                <a data-fancybox=""
                                                   class="popupimages"
                                                   href="{{url('/uploads/users/'.$profile->profile_image)}}">
                                                    {!! $image !!}
                                                </a>
                                            </div>
                                        @endif
                                    @endif
                                </div>


                                <div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                        <label for="email"
                                               class="col-md-4 control-label">{{ 'Email' }}</label>
                                        <div>
                                            <input class="form-control" required
                                                   name="email" type="text"
                                                   id="email"

                                                   value="{{ (isset($profile->email)) ? $profile->email : old('email')}}">
                                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div><div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
                                        <label for="mobile"
                                               class="col-md-4 control-label">{{ 'Mobile' }}</label>
                                        <div>
                                            <input class="form-control"
                                                   name="mobile" type="text"
                                                   id="mobile" required

                                                   value="{{ (isset($profile->mobile)) ? $profile->mobile : old('mobile')}}">
                                            {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-4">
                                        <input class="btn btn-primary" type="submit" value="Update">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page body -->
        </section>
        <!--/ sub page -->
    </main>

@endsection



@section('foo_script')






@endsection


























