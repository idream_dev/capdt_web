<nav id="sidebar" class="author-leftnav">

    <h5 class="h5">Author Info </h5>
    <ul class="list-unstyled components">

        <li class="{{($active == 'profile')?'active':''}} ">
            <a href="{{ route('author_dashboard') }}">Profile</a>
        </li>
        <li class="{{($active == 'mystories')?'active':''}} ">
            <a href="{{ route('author_stories') }}">My Stories</a>
        </li>
        <li>
            <a href="{{ route('logout') }}" class=""  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fi-power"></i> <span>Logout</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>

    </ul>
</nav>