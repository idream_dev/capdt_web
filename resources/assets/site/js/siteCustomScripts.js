$(function () {

    $('.editor').summernote({
        placeholder: 'Enter Message..',
        tabsize: 2,
        height: 500
    });


    $('#subscriptionForm').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        // errorPlacement: function (error, e) {
        //     e.parents('.row').append(error);
        // },
        // highlight: function (e) {
        //     // $(e).closest('.row').removeClass('has-success has-error').addClass('has-error');
        //     $(e).closest('.text-danger').remove();
        // },
        // success: function (e) {
        //     // You can use the following if you would like to highlight with green color the input after successful validation!
        //     e.closest('.row').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
        //     e.closest('.text-danger').remove();
        // },
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                // required: 'Enter email',
                email: 'Enter a valid email'
            }
        },
        submitHandler: function (form) {
            $('.subscriptionMessage').html('');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: '/subscription/save',
                type: 'POST',
                data: $(form).serialize(),
                success: function (response) {
                    if (response!= 0) {
                        $('.subscriptionMessage').html('<div class="bg-success">Thank you for your participation</div>');
                    } else {
                        $('.subscriptionMessage').html('<div class="bg-danger">This email already subscribed...</div>');
                    }
                    $('#subscriptionForm')[0].reset();
                }
            });

            return false; // required to block normal submit since you used ajax
        }
    });


    $('#contactform').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.text-danger').remove();
        },
        rules: {
            email: {
                required: true,
                email: true
            },
            contact_number: {
                required: true,
                number: true
            }
        },
        messages: {
            email: {
                required: 'Enter email',
                email: 'Enter a valid email'
            },
            contact_number: {
                required: 'Enter contact number',
                number: 'Enter a valid number'
            }
        }
    });
});


