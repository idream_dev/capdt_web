<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

//        dump($guard);
//        dd(Auth::guard($guard)->check());

        if ($guard == "web" && Auth::guard($guard)->check()) {
            return redirect('/admin');
        }
        if ($guard == "author" && Auth::guard($guard)->check()) {
            return redirect('/author');
        }

        if (Auth::guard($guard)->check()) {
            return redirect('/');
        }

        return $next($request);
    }
}
