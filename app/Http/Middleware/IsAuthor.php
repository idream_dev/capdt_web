<?php

namespace App\Http\Middleware;

use App\Author;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd(Auth::user()->role);

        if (Auth::user()->role != 2) {

            if (Auth::user()->role == 1) {
                return redirect('/admin');
            } else {
                return redirect('/');

            }
        }


//        dd(Auth::user()->role);

        return $next($request);
    }
}
