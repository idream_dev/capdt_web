<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\settings;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $settings = settings::all();
        $data = array();
        $data['title'] = 'Settings';
        $data['settings'] = $settings;
        $data['active'] = 'settings';
        $data['main_active'] = 'settingsmenu';
        return view('admin.settings.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $data = array();
        $data['title'] = 'Settings';
        $data['active'] = 'settings';
        $data['main_active'] = 'settingsmenu';
        return view('admin.settings.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            's_logo' => 'required',
            's_address' => 'required',
            's_facebook' => 'required'
        ]);

        $requestData = $request->all();


        if ($request->hasFile('s_logo')) {
            foreach ($request['s_logo'] as $file) {
                $uploadPath = public_path('/uploads/settings');

                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['s_logo'] = $fileName;
            }
        }

        settings::create($requestData);

        return redirect('admin/settings')->with('flash_message', 'settings added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $settings = settings::findOrFail($id);
        $data = array();
        $data['title'] = 'Settings';
        $data['active'] = 'settings';
        $data['main_active'] = 'settingsmenu';
        $data['settings'] = $settings;
        return view('admin.settings.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $settings = settings::findOrFail($id);
        $data = array();
        $data['title'] = 'Settings';
        $data['active'] = 'settings';
        $data['main_active'] = 'settingsmenu';
        $data['settings'] = $settings;
        return view('admin.settings.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            's_address' => 'required',
            's_facebook' => 'required'
        ]);

        $requestData = $request->all();

        $settings = settings::findOrFail($id);

        if ($request->hasFile('s_logo')) {
            File::delete('uploads/settings/' . $settings->s_logo);
            foreach ($request['s_logo'] as $file) {
                $uploadPath = public_path('/uploads/settings');

                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['s_logo'] = $fileName;
            }
        }

        $settings->update($requestData);

        return redirect('admin/settings')->with('flash_message', 'settings updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $settings = settings::findOrFail($id);
        File::delete('uploads/settings/' . $settings->s_logo);
        settings::destroy($id);
        return redirect('admin/settings')->with('flash_message', 'settings deleted!');
    }
}
