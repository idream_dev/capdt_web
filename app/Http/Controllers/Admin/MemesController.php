<?php

namespace App\Http\Controllers\Admin;


use App\Models\Memes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class MemesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = 100;
        $memes = Memes::paginate($perPage);
        $data = array();
        $data['title'] = 'Memes';
        $data['memes'] = $memes;
        return view('admin.memes.index', $data);
    }



    public function addMemes(Request $request)
    {




        if ($request->isMethod('post')) {



            $request->validate([
                'm_title' => 'required',
                'm_status' => 'required'
            ],
                [
                    'm_title.required' => 'Title is required',
                    'm_status.required' => 'Status is required',
                ]);

                $requestData = $request->all();

                if ($request->hasFile('m_meme')) {
                    $file = $request['m_meme'];
                    $uploadPath = public_path('/uploads/memes');
                    $extension = $file->getClientOriginalExtension();
                    $fileName = time() . '.' . $extension;
                    $file->move($uploadPath, $fileName);
                    $requestData['m_meme'] = $fileName;
                }


                Memes::create($requestData);


                return redirect('admin/memes')->with('flash_message', 'Meme added!');

            }

    }

    public function MemesEdit(Request $request)
    {

        $request->validate([
            'm_title' => 'required',
            'm_status' => 'required'
        ],
            [
                'm_title.required' => 'Title is required',
                'm_status.required' => 'Status is required',
            ]);

        $id = $request->m_id;
        $requestData = $request->all();


        $memes = Memes::findOrFail($id);

        if ($request->hasFile('m_meme')) {
            $file = $request['m_meme'];
            $uploadPath = public_path('/uploads/memes');
            $extension = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            $file->move($uploadPath, $fileName);
            $requestData['m_meme'] = $fileName;
        }

        $memes->update($requestData);
        return redirect('admin/memes')->with('flash_message', 'Memes Updated!');

    }


    public function memeDelete(Request $request)
    {
            $requestData = $request->all();
            Memes::destroy($requestData['m_id']);
            return redirect()->route('memesindex')->with('flash_message', 'Meme deleted successfully!');

    }


}
