<?php

namespace App\Http\Controllers\Admin;

use App\Models\Videos;
use Youtube;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Channels;

class ChannelsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $perPage = 25;
        $channels = Channels::with('channelWiseVideos')->paginate($perPage);
        $data = array();
        $data['title'] = 'WebSeries';
        $data['channels'] = $channels;
        return view('admin.channels.index', $data);
    }

    public function addChannel(Request $request)
    {


        if ($request->isMethod('post')) {
            $this->validate(request(), [
                'ch_channel_id' => 'required|unique:channels|min:5'
            ],
                [
                    'ch_channel_id.required' => 'Enter Youtube channel ID',
                    'ch_channel_id.unique' => 'This Youtube channel ID Exist',
                    'ch_channel_id.min' => 'Enter Min :min Letters'

                ]);

            $channel = Youtube::getChannelById($request['ch_channel_id'], false, ['id', 'snippet', 'contentDetails', 'statistics', 'brandingSettings', 'invideoPromotion']);

            if (isset($channel->snippet->title)) {

            } else {
                $channel = Youtube::getChannelByName($request['ch_channel_id'], false, ['id', 'snippet', 'contentDetails', 'statistics', 'brandingSettings', 'invideoPromotion']);
            }

            //echo dd($channel->brandingSettings->image);
//          dd($channel);
//          echo $channel->snippet->title;
//          echo $channel->brandingSettings->image->bannerImageUrl;
//          exit();

            if (isset($channel->snippet->title)) {
                $request['ch_title'] = $channel->snippet->title;
                $request['ch_description'] = $channel->snippet->description;
                $request['ch_thumbnails'] = json_encode($channel->snippet->thumbnails);
                $request['ch_branding_image'] = json_encode($channel->brandingSettings->image);
                $request['ch_viewCount'] = $channel->statistics->viewCount;
                $request['ch_subscriberCount'] = $channel->statistics->subscriberCount;
                $request['ch_videoCount'] = $channel->statistics->videoCount;
                $request['ch_channel_id'] = $channel->id;
                if (isset($channel->snippet->customUrl)) {
                    $request['ch_customUrl'] = $channel->snippet->customUrl;
                }

                $request['ch_publishedAt'] = $channel->snippet->publishedAt;

                Channels::create($request->all());
            } else {


                return redirect(route('channelindex'))->with('flash_message', 'Channel Not Valid!');
            }
        }


        return redirect(route('channelindex'))->with('flash_message', 'Channel Added Successfully!');

    }


    public function channelEdit(Request $request)
    {


        if ($request->isMethod('post')) {

            $this->validate(request(), [
                'ch_channel_id' => 'required|unique:channels|min:5'
            ],
                [
                    'ch_channel_id.required' => 'Enter Youtube channel ID',
                    'ch_channel_id.unique' => 'This Youtube channel ID Exist',
                    'ch_channel_id.min' => 'Enter Min :min Letters'

                ]);


            $id = $request->ch_id;

//
//            echo $id;
//            exit;

            $channel = Youtube::getChannelById($request->ch_channel_id, false, ['id', 'snippet', 'contentDetails', 'statistics', 'brandingSettings', 'invideoPromotion']);

            if (isset($channel->snippet->title)) {

            } else {
                $channel = Youtube::getChannelByName($request->ch_channel_id, false, ['id', 'snippet', 'contentDetails', 'statistics', 'brandingSettings', 'invideoPromotion']);
            }

            //echo dd($channel->brandingSettings->image);
//          dd($channel);
//          echo $channel->snippet->title;
//          echo $channel->brandingSettings->image->bannerImageUrl;
//          exit();

            if (isset($channel->snippet->title)) {
                $request['ch_title'] = $channel->snippet->title;
                $request['ch_description'] = $channel->snippet->description;
                $request['ch_thumbnails'] = json_encode($channel->snippet->thumbnails);
                $request['ch_branding_image'] = json_encode($channel->brandingSettings->image);
                $request['ch_viewCount'] = $channel->statistics->viewCount;
                $request['ch_subscriberCount'] = $channel->statistics->subscriberCount;
                $request['ch_videoCount'] = $channel->statistics->videoCount;
                $request['ch_channel_id'] = $channel->id;
                if (isset($channel->snippet->customUrl)) {
                    $request['ch_customUrl'] = $channel->snippet->customUrl;
                }

                $request['ch_publishedAt'] = $channel->snippet->publishedAt;


                $requestData = $request->all();


                $channels = Channels::findOrFail($id);

                $channels->update($requestData);




            } else {


                return redirect(route('channelindex'))->with('flash_message', 'Channel Not Valid!');
            }
        }


        return redirect(route('channelindex'))->with('flash_message', 'Channel Added Successfully!');

    }


    public function checkChannelID(Request $request){
                $current_channel_id = $request['current_channel_id'];
                if ($request['ch_channel_id'] === $current_channel_id) {
                    echo 'true';
                } else {
                    $countWeb=Channels::where('ch_channel_id',$request['ch_channel_id'])->count();
                    if ($countWeb > 0) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }
    }

    public function channelShow($id, Request $request)
    {
        $data = array();
        $channels = Channels::with('channelWiseVideos')->where('ch_channel_id', '=', "{$id}")
            ->first();
        $data['channelsInfo'] = $channels;
        $data['title'] = 'Channels View';
        $data['active_menu'] = 'channels';
        $data['sub_active_menu'] = 'channels-view';
        return view('admin.channels.channel-view', $data);
    }


    public function DatatablesServersideVideos(Request $request)
    {
        $columns = array(
            0 => 'yt_vid',
            1 => 'yt_video_id',
            2 => 'yt_title',
            3 => 'yt_time_uploaded',
            4 => 'created_at',
//            5 => 'yt_vid',
            5 => 'yt_vid',
        );
        $totalData = Videos::where('yt_channel_id', $request['cid'])->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (empty($request->input('search.value'))) {
            $posts = Videos::where('yt_channel_id', $request['cid'])->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');
            $posts = Videos::Where('yt_channel_id', $request['cid'])
                ->where(function($query) use ($search){
                    return $query->where('yt_video_id', 'LIKE', "%{$search}%")
                        ->orWhere('yt_title', 'LIKE', "%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
            $totalFiltered = Videos::where('yt_channel_id', $request['cid'])
                ->where(function($query) use ($search){
                    return $query->where('yt_video_id', 'LIKE', "%{$search}%")
                        // ->orWhere('yt_channel_name', 'LIKE', "%{$search}%")
                        ->orWhere('yt_title', 'LIKE', "%{$search}%");
                })->count();
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $show = route('VideosShow', $post->yt_video_id);
                if ($post->yt_trending == '1') {
                    $check_value = 'checked';
                } else {
                    $check_value = '';
                }
                $nestedData['yt_vid'] = $post->yt_vid;
                $nestedData['yt_video_id'] = "<img width='100' src='https://img.youtube.com/vi/".$post->yt_video_id."/0.jpg'/>";
                $nestedData['yt_title'] = $post->yt_title;
                $nestedData['yt_time_uploaded'] = date('j M Y h:i a', strtotime($post->yt_time_uploaded));
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($post->created_at));
//                $nestedData['Trending'] = "<label class='switch'>
//                                          <input type='checkbox' id='trending{$post->yt_vid}' data-cid='{$post->yt_vid}' class='trending' value='{$post->yt_trending}' $check_value >
//                                          <span class='slider'></span>
//                                        </label>";
                $nestedData['options'] = " <div class='btn-group' role='group' aria-label='Basic example'>
                                        <a href='$show' class='btn btn-sm btn-primary' title='SHOW' ><span class='fa fa-eye'></span></a>
                                        <a href='javascript:void(0)' class='btn btn-sm btn-primary delete' data-toggle='confirmation'  data-vid='{$post->yt_vid}' title='Delete' ><span class='fa fa-remove'></span></a>
                                </div>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function channelVideosimport(Request $request)
    {
        $id=$request['id'];
        $channelid=Channels::where('ch_channel_id',$id)->get();
//        $channel = Youtube::getChannelById($id);
        $channel = Youtube::getChannelById($id, false, ['id', 'snippet', 'contentDetails', 'statistics', 'brandingSettings', 'invideoPromotion']);
        if ($channelid) {
            if($channel->statistics->videoCount < 50){
                $videoList = Youtube::listChannelVideos($id, 50, 'date');
                $requestData = array();
                foreach($videoList as $videos){
                    $title = $videos->snippet->title;
                    $videoind = Youtube::getVideoInfo($videos->id->videoId);
                    $requestData['yt_title']=$videos->snippet->title;
                    $requestData['yt_video_id']=$videos->id->videoId;
                    $requestData['yt_channel_id']=$videos->snippet->channelId;
                    $requestData['yt_thumbnails']=json_encode($videos->snippet->thumbnails);
                    $requestData['yt_description']=$videos->snippet->description;
                    $requestData['yt_viewCount']=$videoind->statistics->viewCount;
                    if (isset($videoind->snippet->tags)) {
                        $requestData['yt_tags'] = implode(',', $videoind->snippet->tags);
                    }else{
                        $requestData['yt_tags'] = '';
                    }
                    $requestData['yt_status']='1';
                    $requestData['yt_time_uploaded']=date('Y-m-d H:i:s', strtotime($videos->snippet->publishedAt));
                    $requestData['yt_video_duration'] = $videoind->contentDetails->duration;
                    $postnew=Videos::where('yt_video_id',$videos->id->videoId)->get();
                    $countpost=count($postnew);
                    if($countpost == 0){
                        Videos::create($requestData);
                    }
                }
                return redirect()->back()->with("flash_message", "Videos Updated Successfully!");
            }else {
                $totalVideos = array();
                // Set default parameters
                $perpage = 50;
                $params = [
                    'type' => 'video',
                    'channelId' => $id,
                    'part' => 'id, snippet',
                    'order' => 'date',
                    'maxResults' => $perpage
                ];
                $search = Youtube::searchAdvanced($params, true);
                $totall = ceil($search['info']['totalResults'] / $perpage);
                for ($i = 0; $i < ($totall - 1); $i++) {
                    if ($i == 0) {
                        $totalVideos = array_merge($totalVideos, $search['results']);
                        if (isset($search['info']['nextPageToken'])) {
                            $params['pageToken'] = $search['info']['nextPageToken'];
                            //echo ' loop no: ' . $i . ' prepage: ' . $perpage . ' nextpage : ' . $search['info']['nextPageToken'] . ' prev: ' . $search['info']['nextPageToken'] . '<br />';
                        }
                    }
                    $search = Youtube::searchAdvanced($params, true);
                    if (isset($search['info']['nextPageToken'])) {
                        $params['pageToken'] = $search['info']['nextPageToken'];
                    }
                    if (is_array($search['results'])) {
                        $totalVideos = array_merge($totalVideos, $search['results']);
                    } else {
                    }
                }
                $requestData = array();
                foreach ($totalVideos as $videos) {
                    $title = $videos->snippet->title;
                    $videoind = Youtube::getVideoInfo($videos->id->videoId);
                    $requestData['yt_title'] = $videos->snippet->title;
                    $requestData['yt_video_id'] = $videos->id->videoId;
                    $requestData['yt_channel_id'] = $videos->snippet->channelId;
                    if (isset($videoind->snippet->tags)) {
                        $requestData['yt_tags'] = implode(',', $videoind->snippet->tags);
                    } else {
                        $requestData['yt_tags'] = '';
                    }
                    $requestData['yt_thumbnails'] = json_encode($videos->snippet->thumbnails);
                    $requestData['yt_description'] = $videos->snippet->description;
                    $requestData['yt_viewCount'] = $videoind->statistics->viewCount;
                    $requestData['yt_status'] = '1';
                    $requestData['yt_time_uploaded'] = date('Y-m-d H:i:s', strtotime($videos->snippet->publishedAt));
                    $requestData['yt_video_duration'] = $videoind->contentDetails->duration;
                    $postnew = Videos::where('yt_video_id', $videos->id->videoId)->get();
                    $countpost = count($postnew);
                    if ($countpost == 0) {
                        Videos::create($requestData);
                    }
                }
                return redirect()->back()->with("flash_message", "Videos Updated Successfully!");
            }
        } else {
            return redirect()->back()->with("flash_message", "Channel does not exist!");
        }
    }


    public function channelDelete(Request $request)
    {
        $requestData = $request->all();



        Channels::destroy($requestData['ch_id']);
        Videos::where('yt_channel_id',$requestData['ch_channel_id'])->delete();
        return redirect()->route('channelindex')->with('flash_message', 'Channel deleted successfully!');

    }
}
