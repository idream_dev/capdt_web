<?php

namespace App\Http\Controllers\Admin;

use Youtube;
use App\Models\Videos;
use App\Models\Webseries;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebseriesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = 25;
        $webseries = Webseries::with('WebseriesWiseVideos')->paginate($perPage);
        $data = array();
        $data['title'] = 'WebSeries';
        $data['webseries'] = $webseries;
        return view('admin.webseries.index', $data);
    }



    public function addWebseries(Request $request)
    {


            if ($request->isMethod('post')) {

                $this->validate(request(), [
                    'w_title' => 'required',
                    'w_alias' => 'required|unique:webseries',
                    'w_status' => 'required'
                ],
                    [
                        'w_title.required' => 'Title is required',
                        'w_alias.required' => 'Alias is required',
                        'w_alias.unique' => 'Alias Already Exist',
                        'w_status.required' => 'Status is required',
                    ]);

                $requestData = $request->all();

                if ($request->hasFile('w_poster')) {
                    $file = $request['w_poster'];
                    $uploadPath = public_path('/uploads/webseries');
                    $extension = $file->getClientOriginalExtension();
                    $fileName = time() . '.' . $extension;
                    $file->move($uploadPath, $fileName);
                    $requestData['w_poster'] = $fileName;
                }


                Webseries::create($requestData);


                return redirect('admin/webseries')->with('flash_message', 'Webseries added!');

            }

    }

    public function webseriesEdit(Request $request)
    {

        $this->validate(request(), [
            'w_title' => 'required',
            'w_alias' => 'required',
            'w_status' => 'required'
        ],
            [
                'w_title.required' => 'Title is required',
                'w_alias.required' => 'Alias is required',
                'w_status.required' => 'Status is required',

            ]);

        $id = $request->w_id;
        $requestData = $request->all();


        $category = Webseries::findOrFail($id);

        if ($request->hasFile('w_poster')) {
            $file = $request['w_poster'];
            $uploadPath = public_path('/uploads/webseries');
            $extension = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            $file->move($uploadPath, $fileName);
            $requestData['w_poster'] = $fileName;
        }

        $category->update($requestData);
        return redirect('admin/webseries')->with('flash_message', 'Webseries Updated!');

    }


    public function checkNewWebseriesAlias(Request $request){
        switch ($request['type']) {
            case 'edit':
                $current_webseries_alias = $request['current_webseries_alias'];
                if ($request['w_alias'] === $current_webseries_alias) {
                    echo 'true';
                } else {
                    $countWeb=Webseries::where('w_alias',$request['w_alias'])->count();
                    if ($countWeb > 0) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }
                break;
            case 'new':
                $countWeb=Webseries::where('w_alias',$request['w_alias'])->count();
                if ($countWeb > 0) {
                    echo 'false';
                } else {
                    echo 'true';
                }
                break;
            default:
                break;
        }
    }

    public function AllVideos()
    {
        $data = array();
        $data['title'] = 'All Videos';
        $data['active_menu'] = 'all-videos';
        $data['sub_active_menu'] = 'all-videos-view';
        return view('admin.webseries.all-videos', $data);
    }

    public function DatatablesServersideAllVideos(Request $request)
    {

        $webseries = Webseries::where('w_status', '1')->orderBy('w_id', 'desc')->pluck('w_title', 'w_id');
        $columns = array(
            0 => 'yt_vid',
            1 => 'yt_video_id',
            2 => 'yt_title',
            3 => 'yt_webseries_id',
            4 => 'yt_time_uploaded',
            5 => 'created_at',
            6 => 'yt_vid',
            7 => 'yt_vid',
        );
        $totalData = Videos::count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (empty($request->input('search.value'))) {
            $posts = Videos::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');
            $posts = Videos::where(function($query) use ($search){
                    return $query->where('yt_video_id', 'LIKE', "%{$search}%")
                        ->orWhere('yt_title', 'LIKE', "%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
            $totalFiltered = Videos::where(function($query) use ($search){
                    return $query->where('yt_video_id', 'LIKE', "%{$search}%")
                        // ->orWhere('yt_channel_name', 'LIKE', "%{$search}%")
                        ->orWhere('yt_title', 'LIKE', "%{$search}%");
                })->count();
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $show = route('VideosShow', $post->yt_video_id);
                if ($post->yt_trending == '1') {
                    $check_value = 'checked';
                } else {
                    $check_value = '';
                }
                $nestedData['yt_vid'] = $post->yt_vid;
                $nestedData['yt_video_id'] = "<img width='100' src='https://img.youtube.com/vi/".$post->yt_video_id."/0.jpg'/>";
                $nestedData['yt_title'] = $post->yt_title;
                $nestedData['yt_time_uploaded'] = date('j M Y h:i a', strtotime($post->yt_time_uploaded));
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($post->created_at));
//                $nestedData['Trending'] = "<label class='switch'>
//                                          <input type='checkbox' id='trending{$post->yt_vid}' data-cid='{$post->yt_vid}' class='trending' value='{$post->yt_trending}' $check_value >
//                                          <span class='slider'></span>
//                                        </label>";


                $html = '
             
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal_' . $post->yt_vid . '">
  Assign
</button>
<div class="modal" id="myModal_' . $post->yt_vid . '">
<div class="modal-dialog modal-lg">
<div class="modal-content " >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assign to Webseries</h4>
        </div>
        
   
<div class="modal-body">
  <h4 class=""> ' . $post->yt_title . '</h4>
<div class="row">

    <div class="col-md-6">
        <form method="post" class="assignform" id="form' . $post->yt_video_id . '" name="form' . $post->yt_video_id . '" enctype="multipart/form-data" >
    ' . csrf_field() . '  
    <div class="modal-body">
        <div class="form-group">
        <label for="simpleFormEmail">Video ID</label>
        <input name="yt_vid" id="yt_vid" type="hidden" class="form-control" value="' . $post->yt_vid . '" placeholder="Youtube Video Id">
        <input name="yt_video_id" id="yt_video_id" type="text" value="' . $post->yt_video_id . '" class="form-control" readonly placeholder="Youtube Video Id">
        </div>
        
        
        
        
        <div class="form-group">
        <label for="simpleFormEmail">Webseries</label>
        <select class="form-control select2" required id="yt_webseries_id" name="yt_webseries_id">
        <option value="">Select Webseries</option>';
                foreach ($webseries as $optionKey => $optionValue) {
                    if ($post->yt_webseries_id == $optionKey) {
                        $selectedvalue = 'selected';
                    } else {
                        $selectedvalue = '';
                    }
                    $html .= ' <option value="' . $optionKey . '" ' . $selectedvalue . '>' . $optionValue . '</option>';
                }
                $html .= '
        </select>
        </div>
    
       
        
    
    
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Assign</button>
            </div>
        </form>
    </div>
    </div>
    
    
    <div class="col-md-6">
      
            <h4 class="">Preview </h4> 
       
             <a data-fancybox class="popupimages" href="https://www.youtube.com/watch?v=' . $post->yt_video_id . '" ><img src="https://img.youtube.com/vi/' . $post->yt_video_id . '/mqdefault.jpg"></a>
       
       <Br/>

       
    </div>
    
    <span class="updatedtext"></span>
    
</div>
</div>
    </div>
  </div>
</div><script>
form_validation_assign_video("'.$post->yt_video_id.'");
    </script>';


                $nestedData['Assign'] = $html;



                $nestedData['options'] = " <div class='btn-group' role='group' aria-label='Basic example'>
                                        <a href='$show' class='btn btn-sm btn-primary' title='SHOW' ><span class='fa fa-eye'></span></a>
                                        <a href='javascript:void(0)' class='btn btn-sm btn-primary delete' data-toggle='confirmation'  data-vid='{$post->yt_vid}' title='Delete' ><span class='fa fa-remove'></span></a>
                                </div>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }




    public function UncategorizedVideos()
    {
        $data = array();
        $data['title'] = 'Uncategorized Videos';
        $data['active_menu'] = 'all-uncategorized-videos';
        $data['sub_active_menu'] = 'all-uncategorized-videos-view';
        return view('admin.webseries.uncategorized-videos', $data);
    }

    public function UncategorizedAllVideos(Request $request)
    {

        $webseries = Webseries::where('w_status', '1')->orderBy('w_id', 'desc')->pluck('w_title', 'w_id');
        $columns = array(
            0 => 'yt_vid',
            1 => 'yt_video_id',
            2 => 'yt_title',
            3 => 'yt_webseries_id',
            4 => 'yt_time_uploaded',
            5 => 'created_at',
            6 => 'yt_vid',
            7 => 'yt_vid',
        );
        $totalData = Videos::whereNull('yt_webseries_id')->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (empty($request->input('search.value'))) {
            $posts = Videos::whereNull('yt_webseries_id')->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');
            $posts = Videos::whereNull('yt_webseries_id')->where(function($query) use ($search){
                return $query->where('yt_video_id', 'LIKE', "%{$search}%")
                    ->orWhere('yt_title', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
            $totalFiltered = Videos::whereNull('yt_webseries_id')->where(function($query) use ($search){
                return $query->where('yt_video_id', 'LIKE', "%{$search}%")
                    // ->orWhere('yt_channel_name', 'LIKE', "%{$search}%")
                    ->orWhere('yt_title', 'LIKE', "%{$search}%");
            })->count();
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $show = route('VideosShow', $post->yt_video_id);
                if ($post->yt_trending == '1') {
                    $check_value = 'checked';
                } else {
                    $check_value = '';
                }
                $nestedData['yt_vid'] = $post->yt_vid;
                $nestedData['yt_video_id'] = "<img width='100' src='https://img.youtube.com/vi/".$post->yt_video_id."/0.jpg'/>";
                $nestedData['yt_title'] = $post->yt_title;
                $nestedData['yt_time_uploaded'] = date('j M Y h:i a', strtotime($post->yt_time_uploaded));
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($post->created_at));
//                $nestedData['Trending'] = "<label class='switch'>
//                                          <input type='checkbox' id='trending{$post->yt_vid}' data-cid='{$post->yt_vid}' class='trending' value='{$post->yt_trending}' $check_value >
//                                          <span class='slider'></span>
//                                        </label>";


                $html = '
             
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal_' . $post->yt_vid . '">
  Assign
</button>
<div class="modal" id="myModal_' . $post->yt_vid . '">
<div class="modal-dialog modal-lg">
<div class="modal-content " >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assign to Webseries</h4>
        </div>
        
   
<div class="modal-body">
  <h4 class=""> ' . $post->yt_title . '</h4>
<div class="row">

    <div class="col-md-6">
        <form method="post" class="assignform" id="form' . $post->yt_video_id . '" name="form' . $post->yt_video_id . '" enctype="multipart/form-data" >
    ' . csrf_field() . '  
    <div class="modal-body">
        <div class="form-group">
        <label for="simpleFormEmail">Video ID</label>
        <input name="yt_vid" id="yt_vid" type="hidden" class="form-control" value="' . $post->yt_vid . '" placeholder="Youtube Video Id">
        <input name="yt_video_id" id="yt_video_id" type="text" value="' . $post->yt_video_id . '" class="form-control" readonly placeholder="Youtube Video Id">
        </div>
        
        
        
        
        <div class="form-group">
        <label for="simpleFormEmail">Webseries</label>
        <select class="form-control select2" required id="yt_webseries_id" name="yt_webseries_id">
        <option value="">Select Webseries</option>';
                foreach ($webseries as $optionKey => $optionValue) {
                    if ($post->yt_webseries_id == $optionKey) {
                        $selectedvalue = 'selected';
                    } else {
                        $selectedvalue = '';
                    }
                    $html .= ' <option value="' . $optionKey . '" ' . $selectedvalue . '>' . $optionValue . '</option>';
                }
                $html .= '
        </select>
        </div>
    
       
        
    
    
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Assign</button>
            </div>
        </form>
    </div>
    </div>
    
    
    <div class="col-md-6">
      
            <h4 class="">Preview </h4> 
       
             <a data-fancybox class="popupimages" href="https://www.youtube.com/watch?v=' . $post->yt_video_id . '" ><img src="https://img.youtube.com/vi/' . $post->yt_video_id . '/mqdefault.jpg"></a>
       
       <Br/>

       
    </div>
    
    <span class="updatedtext"></span>
    
</div>
</div>
    </div>
  </div>
</div><script>
form_validation_assign_video("'.$post->yt_video_id.'");
    </script>';


                $nestedData['Assign'] = $html;



                $nestedData['options'] = " <div class='btn-group' role='group' aria-label='Basic example'>
                                        <a href='$show' class='btn btn-sm btn-primary' title='SHOW' ><span class='fa fa-eye'></span></a>
                                        <a href='javascript:void(0)' class='btn btn-sm btn-primary delete' data-toggle='confirmation'  data-vid='{$post->yt_vid}' title='Delete' ><span class='fa fa-remove'></span></a>
                                </div>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }





    public function VideosShow($id)
    {
        $data = array();
        $videos = Videos::where('yt_video_id', '=', "{$id}")
            ->first();
        $videos = Youtube::getVideoInfo($videos->yt_video_id);
        $data['videoInfo'] = $videos;
        $data['title'] = 'Youtube Videos';
        $data['active_menu'] = 'channels';
        $data['sub_active_menu'] = 'ytVideos';
        return view('admin.webseries.video-view', $data);
    }

    public function VideoRemove(Request $request)
    {
        $videoId = $request['vid'];
        Videos::destroy($videoId);
        echo 'true';
        exit();
    }

    public function WebseriesVideoRemove(Request $request)
    {
        $id = $request['vid'];
        $requestData['yt_webseries_id']=NULL;
        $videos = Videos::findOrFail($id);
        $videos->update($requestData);
        echo 'true';
        exit();
    }

    public function AssignedToWebseries(Request $request)
    {

        $requestData = $request->all();
        $id = $requestData['yt_vid'];
        $videos = Videos::findOrFail($id);
        $videos->update($requestData);

//        return redirect()->route('AllVideos')->with('flash_message', 'Webseries assigned successfully!');

        return response()->json(['message' => 'Webseries Updated Successfully!']);
        //return redirect()->back()->with('flash_message', 'Category Updated Successfully and it is moved to Categorized Videos list!');
    }


    public function WebseriesShow($id, Request $request)
    {
        $data = array();
        $webseries = Webseries::with('WebseriesWiseVideos')->where('w_id', '=', "{$id}")
            ->first();
        $data['webseriesInfo'] = $webseries;
        $data['title'] = 'Webseries View';
        $data['active_menu'] = 'webseries';
        $data['sub_active_menu'] = 'webseries-view';
        return view('admin.webseries.webseries-view', $data);
    }


    public function WebseriesVideos(Request $request)
    {
        $columns = array(
            0 => 'yt_vid',
            1 => 'yt_video_id',
            2 => 'yt_title',
            3 => 'yt_time_uploaded',
            4 => 'created_at',
//            5 => 'yt_vid',
            5 => 'yt_vid',
        );
        $totalData = Videos::where('yt_webseries_id', $request['wid'])->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (empty($request->input('search.value'))) {
            $posts = Videos::where('yt_webseries_id', $request['wid'])->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');
            $posts = Videos::Where('yt_webseries_id', $request['wid'])
                ->where(function($query) use ($search){
                    return $query->where('yt_video_id', 'LIKE', "%{$search}%")
                        ->orWhere('yt_title', 'LIKE', "%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
            $totalFiltered = Videos::where('yt_webseries_id', $request['wid'])
                ->where(function($query) use ($search){
                    return $query->where('yt_video_id', 'LIKE', "%{$search}%")
                        // ->orWhere('yt_channel_name', 'LIKE', "%{$search}%")
                        ->orWhere('yt_title', 'LIKE', "%{$search}%");
                })->count();
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $show = route('VideosShow', $post->yt_video_id);
                if ($post->yt_trending == '1') {
                    $check_value = 'checked';
                } else {
                    $check_value = '';
                }
                $nestedData['yt_vid'] = $post->yt_vid;
                $nestedData['yt_video_id'] = "<img width='100' src='https://img.youtube.com/vi/".$post->yt_video_id."/0.jpg'/>";
                $nestedData['yt_title'] = $post->yt_title;
                $nestedData['yt_time_uploaded'] = date('j M Y h:i a', strtotime($post->yt_time_uploaded));
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($post->created_at));
//                $nestedData['Trending'] = "<label class='switch'>
//                                          <input type='checkbox' id='trending{$post->yt_vid}' data-cid='{$post->yt_vid}' class='trending' value='{$post->yt_trending}' $check_value >
//                                          <span class='slider'></span>
//                                        </label>";
                $nestedData['options'] = " <div class='btn-group' role='group' aria-label='Basic example'>
                                        <a href='$show' class='btn btn-sm btn-primary' title='SHOW' ><span class='fa fa-eye'></span></a>
                                        <a href='javascript:void(0)' class='btn btn-sm btn-primary delete' data-toggle='confirmation'  data-vid='{$post->yt_vid}' title='Delete' ><span class='fa fa-remove'></span></a>
                                </div>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }



    public function webseriesDelete(Request $request)
    {
            $requestData = $request->all();
            Webseries::destroy($requestData['w_id']);
            return redirect()->route('webseriesindex')->with('flash_message', 'Webseries deleted successfully!');

    }


}
