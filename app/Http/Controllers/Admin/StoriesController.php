<?php

namespace App\Http\Controllers\Admin;


use App\Models\Stories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoriesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = 25;
        $stories = Stories::with('getAuthor')->orderBy('s_id', 'DESC')->paginate($perPage);
        $data = array();
        $data['title'] = 'Stories';
        $data['stories'] = $stories;
        return view('admin.stories.index', $data);
    }


    public function addStories(Request $request, $id = null)
    {



            if ($request->isMethod('post')) {

                $requestData = $request->all();

                if ($requestData['s_id'] == '') {
                    $request->validate([
                        's_user_id' => 'required',
                        's_title' => 'required|unique:stories',
                        's_status' => 'required'
                    ],
                        [
                            's_user_id.required' => 'Author is required',
                            's_title.required' => 'Title is required',
                            's_title.unique' => 'Title is already exist',
                            's_status.required' => 'Status is required',
                        ]);

                }else{
//                    echo "testes";
//                    exit;
                    $request->validate([
                        's_title' => 'required|unique:stories,s_title,'.$requestData['s_id'].',s_id',
                        's_status' => 'required'
                    ],
                        [
                            's_title.required' => 'Title is required',
                            's_title.unique' => 'Title Already Exist',
                            's_status.required' => 'Status is required',

                        ]);
                }





                $requestData['s_alias']= str_slug($requestData['s_title']);
                if ($request->hasFile('s_poster')) {
                    $file = $request['s_poster'];
                    $uploadPath = public_path('/uploads/stories');
                    $extension = $file->getClientOriginalExtension();
                    $fileName = time() . '.' . $extension;
                    $file->move($uploadPath, $fileName);
                    $requestData['s_poster'] = $fileName;
                }

                if ($requestData['s_id'] == '') {
                    Stories::create($requestData);
                    $mes = 'Story added successfully!';
                } else {
                    $stories = Stories::findOrFail($requestData['s_id']);
                    $stories->update($requestData);
                    $mes = 'Story updated successfully!';
                }
                return redirect()->route('storiesindex')->with('flash_message', $mes);

            } else {
                $data = array();
                $data['s_id'] = '';
                $data['story'] = '';
                if ($id) {
                    $data['s_id'] = $id;
                    $data['story'] = Stories::findOrFail($id);
                }
                $data['active_menu'] = 'stories';
                $data['sub_active_menu'] = 'manage-stories';
                $data['title'] = 'Manage Stories';
                return view('admin.stories.add', $data);
            }

    }

//    public function addStories(Request $request)
//    {
//
//
//        if ($request->isMethod('post')) {
//
//            $request->validate([
//                's_user_id' => 'required',
//                's_title' => 'required|unique:stories',
//                's_status' => 'required'
//            ],
//                [
//                    's_user_id.required' => 'Author is required',
//                    's_title.required' => 'Title is required',
//                    's_title.unique' => 'Title is already exist',
//                    's_status.required' => 'Status is required',
//                ]);
//
//            $requestData = $request->all();
//
//            $requestData['s_alias']= str_slug($requestData['s_title']);
////            dd($requestData);
//            if ($request->hasFile('s_poster')) {
//                $file = $request['s_poster'];
//                $uploadPath = public_path('/uploads/stories');
//                $extension = $file->getClientOriginalExtension();
//                $fileName = time() . '.' . $extension;
//                $file->move($uploadPath, $fileName);
//                $requestData['s_poster'] = $fileName;
//            }
//
//
//            Stories::create($requestData);
//
//
//            return redirect('admin/stories')->with('flash_message', 'Story added!');
//
//        }
//
//    }

    public function storyEdit(Request $request)
    {
        $id = $request->s_id;

//        'user.email' => 'required|unique:users,email,' . $eUserId . ',id',
//        'p_name' => 'required|unique:products,p_name,' . $requestData['p_id'] . ',p_id',

        $request->validate([
            's_title' => 'required|unique:stories,s_title,'.$id.',s_id',
//            's_alias' => 'required|unique:stories, s_alias,'.$id.',s_id',
            's_status' => 'required'
        ],
            [
                's_title.required' => 'Title is required',
//                's_alias.required' => 'Alias is required',
                's_title.unique' => 'Title Already Exist',
                's_status.required' => 'Status is required',

            ]);

//        dd("dsgsdgsadg");
        $requestData = $request->all();

        $requestData['s_alias']= str_slug($requestData['s_title']);

        $stories = Stories::findOrFail($id);

        if ($request->hasFile('s_poster')) {
            $file = $request['s_poster'];
            $uploadPath = public_path('/uploads/stories');
            $extension = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            $file->move($uploadPath, $fileName);
            $requestData['s_poster'] = $fileName;
        }

        $stories->update($requestData);
        return redirect('admin/stories')->with('flash_message', 'Stories Updated Successfully!');

    }


    public function checkNewStoriesAlias(Request $request)
    {
        switch ($request['type']) {
            case 'edit':
                $current_story_alias = $request['current_story_alias'];
                if ($request['s_alias'] === $current_story_alias) {
                    echo 'true';
                } else {
                    $countWeb = Stories::where('s_alias', $request['s_alias'])->count();
                    if ($countWeb > 0) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }
                break;
            case 'new':
                $countWeb = Stories::where('s_alias', $request['s_alias'])->count();
                if ($countWeb > 0) {
                    echo 'false';
                } else {
                    echo 'true';
                }
                break;
            default:
                break;
        }
    }


    public function storyDelete(Request $request)
    {
        $requestData = $request->all();
        Stories::destroy($requestData['s_id']);
        return redirect()->route('storiesindex')->with('flash_message', 'Story deleted successfully!');

    }


}
