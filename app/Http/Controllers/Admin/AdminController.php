<?php

namespace App\Http\Controllers\Admin;

use App\Models\ContactUs;
use App\Models\Memes;
use App\Models\Stories;
use App\Models\Subscriptions;
use App\Models\Videos;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $data = array();
        $data['active'] = 'dashboard';
        $data['title'] = 'dashboard';
        $data['main_active'] = 'dashboard';
        $data['mems'] = Memes::get();
        $data['storiesCount'] = Stories::count();
        $data['videoscount'] = Videos::count();
        $data['videosLatest'] = Videos::orderBy('yt_vid', 'DESC')->limit(10)->get();
        $data['storiesLatest'] = Stories::orderBy('s_id', 'DESC')->limit(10)->get();
        return view('admin.dashboard', $data);
    }

    public function author(Request $request)
    {


        if ($request->isMethod('post')) {

//            dd($request->post('action'));

            if ($request->post('action') == 'remove') {
                $userId = User::removeAuthor($request);

            } else {
                $userId = User::createAuthor($request);

            }


            return redirect()->back()->with('flash_message', 'Auther added!');


        }

        $perPage = 50;

        $data = array();
        $data['authors'] = User::where('role', 2)->paginate($perPage);
        $data['active'] = 'authors';
        $data['title'] = 'Authors Manage';
        $data['main_active'] = 'authors';
        return view('admin.authors.list', $data);
    }


    public function contactList()
    {
        $data = array();
        $data['title'] = 'Contact Mail List | Capdt.media';
        $data['active_menu'] = 'contactList';
        $data['sub_active_menu'] = 'contactList';
        $perPage = 100;
        $data['contacts'] = ContactUs::orderBy('id', 'desc')
            ->paginate($perPage);
        return view('admin.contactList', $data);
    }


    public function subscriptions()
    {
        $data = array();
        $data['title'] = 'Subscriptions | Capdt.media';
        $data['active_menu'] = 'subscriptions';
        $data['sub_active_menu'] = 'subscriptions';
        $perPage = 100;
        $data['subscriptions'] = Subscriptions::orderBy('id', 'desc')
            ->paginate($perPage);
        return view('admin.subscriptionsList', $data);
    }
}
