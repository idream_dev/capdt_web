<?php

namespace App\Http\Controllers\Admin;


use App\Models\Banners;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class BannersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = 100;
        $banners = Banners::paginate($perPage);
        $data = array();
        $data['title'] = 'Banners';
        $data['banners'] = $banners;
        return view('admin.banners.index', $data);
    }



    public function addBanners(Request $request)
    {




        if ($request->isMethod('post')) {



            $request->validate([
                'b_title' => 'required',
                'b_status' => 'required'
            ],
                [
                    'b_title.required' => 'Title is required',
                    'b_status.required' => 'Status is required',
                ]);

                $requestData = $request->all();

                if ($request->hasFile('b_banner')) {
                    $file = $request['b_banner'];
                    $uploadPath = public_path('/uploads/banners');
                    $extension = $file->getClientOriginalExtension();
                    $fileName = time() . '.' . $extension;
                    $file->move($uploadPath, $fileName);
                    $requestData['b_banner'] = $fileName;
                }


                Banners::create($requestData);


                return redirect('admin/banners')->with('flash_message', 'Banner added!');

            }

    }

    public function BannerEdit(Request $request)
    {

        $request->validate([
            'b_title' => 'required',
            'b_status' => 'required'
        ],
            [
                'b_title.required' => 'Title is required',
                'b_status.required' => 'Status is required',
            ]);

        $id = $request->b_id;
        $requestData = $request->all();


        $banners = Banners::findOrFail($id);

        if ($request->hasFile('b_banner')) {
            $file = $request['b_banner'];
            $uploadPath = public_path('/uploads/banners');
            $extension = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            $file->move($uploadPath, $fileName);
            $requestData['b_banner'] = $fileName;
        }

        $banners->update($requestData);
        return redirect('admin/banners')->with('flash_message', 'Banner Updated!');

    }


    public function BannerDelete(Request $request)
    {
            $requestData = $request->all();
            Banners::destroy($requestData['b_id']);
            return redirect()->route('bannersindex')->with('flash_message', 'Banner deleted successfully!');

    }


}
