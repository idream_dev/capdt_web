<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Banners;
use App\Models\Channels;
use App\Models\ContactUs;
use App\Models\Memes;
use App\Models\settings;
use App\Models\Stories;
use App\Models\Subscriptions;
use App\Models\Videos;
use App\Models\Webseries;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        //banners
        $banners = Banners::where('b_status', 1)->get();
        $data['banners'] = $banners;

        //webseries
        $webseries = Webseries::where('w_status', 1)->get();
        $data['webseries'] = $webseries;


        //latestvideos
        $latestvideos = Videos::where('yt_status', 1)->orderBy('yt_time_uploaded', 'desc')->take(10)->get();
        $data['latestvideos'] = $latestvideos;

        //popularvideos
        $popularvideos = Videos::where('yt_status', 1)->orderBy('yt_viewCount', 'desc')->take(10)->get();
        $data['popularvideos'] = $popularvideos;


        //stories
        $stories = Stories::where('s_status', 1)->orderBy('s_id', 'desc')->take(10)->get();
        $data['stories'] = $stories;

        //memes
        $memes = Memes::where('m_status', 1)->orderBy('m_id', 'desc')->take(10)->get();
        $data['memes'] = $memes;


        $data['title'] = 'Home | capdt.media';
        $data['active'] = 'home';
        $data['sub_active'] = 'home';
        return view('frontend.home', $data);
    }


    public function channels($alias)
    {
        $channelInfo = Channels::where('ch_customUrl', $alias)->first();

        $data = array();
        $data['title'] = $channelInfo->ch_title.' | capdt.media';
        $data['channelInfo'] = $channelInfo;
        $data['videos'] = Videos::where('yt_channel_id', $channelInfo->ch_channel_id)->where('yt_status', 1)->orderBy('yt_time_uploaded', 'desc')->paginate(32);
        $data['active'] = 'channels';
        $data['sub_active'] = 'channels';

        return view('frontend.channels', $data);
    }

    public function ChannelsvideoInfo(Request $request)
    {
        $data = array();
        $channelInfo=Channels::where('ch_customUrl',$request->alias)->first();
        $data['channelInfo'] = $channelInfo;
        $videos = Videos::where('yt_video_id',$request->id)->where('yt_channel_id',$channelInfo->ch_channel_id)->first();
        $data['videos'] = $videos;
        $channelvideos = Videos::where('yt_status', 1)->where('yt_video_id','!=',$request->id)->where('yt_channel_id',$channelInfo->ch_channel_id)->orderBy('yt_time_uploaded','desc')->take(10)->get();
        $data['channelvideos'] = $channelvideos;

        $data['title'] = $videos->yt_title.' | capdt.media';
        $data['active'] = 'ChannelsVideo';
        $data['sub_active'] = 'ChannelVideo';
        return view('frontend.channel-videoInfo', $data);
    }

    public function LatestVideos()
    {
        $data = array();
        $videos = Videos::where('yt_status', 1)->orderBy('yt_time_uploaded','desc')->paginate(32);
        $data['videos'] = $videos;

        $data['title'] = 'Latest Videos | capdt.media';
        $data['active'] = 'LatestVideos';
        $data['sub_active'] = 'LatestVideos';
        return view('frontend.latest-videos', $data);
    }

    public function PopularVideos()
    {
        $data = array();
        $videos = Videos::where('yt_status', 1)->orderBy('yt_viewCount', 'desc')->paginate(32);
        $data['videos'] = $videos;

        $data['title'] = 'Popular Videos | capdt.media';
        $data['active'] = 'PopularVideos';
        $data['sub_active'] = 'PopularVideos';
        return view('frontend.popular-videos', $data);
    }


    public function WebseriesList()
    {
        $data = array();
        $webseries = Webseries::with('WebseriesWiseVideos')->where('w_status', 1)->orderBy('w_id','desc')->paginate(32);
        $data['webseries'] = $webseries;

        $data['title'] = 'Webseries List | capdt.media';
        $data['active'] = 'Webseries';
        $data['sub_active'] = 'webserieslist';
        return view('frontend.webseries-list', $data);
    }




    public function WebseriesView($alias)
    {
        $data = array();
        $webseries = Webseries::where('w_alias',$alias)->first();
        $videos = Videos::where('yt_webseries_id',$webseries->w_id)->orderBy('yt_vid','desc')->paginate(32);
        $data['videos'] = $videos;
        $data['webseries'] = $webseries;

        $data['title'] = $webseries->w_title.' | capdt.media';
        $data['active'] = 'Webseries';
        $data['sub_active'] = 'webseriewvideos';
        return view('frontend.webseries-view', $data);
    }

    public function WebseriesVideoView(Request $request)
    {

        $id = $request->id;
        $alias=$request->alias;

        $data = array();
        $videos = Videos::where('yt_video_id',$id)->first();
        $data['videos'] = $videos;
        $webseries = Webseries::with('WebseriesWiseVideos')->where('w_id',$videos->yt_webseries_id)->first();
        $data['webseries'] = $webseries;
        $othervideos = Videos::where('yt_video_id','!=',$id)->where('yt_webseries_id',$videos->yt_webseries_id)->orderBy('yt_time_uploaded','desc')->get();
        $data['othervideos'] = $othervideos;


        $data['title'] = $videos->yt_title.' | capdt.media';
        $data['active'] = 'WebseriesVideo';
        $data['sub_active'] = 'webseriesvideoview';
        return view('frontend.video-view', $data);
    }

    public function LatestvideoInfo($id)
    {
        $data = array();
        $videos = Videos::where('yt_video_id',$id)->first();
        $data['videos'] = $videos;
        $latestvideos = Videos::where('yt_status', 1)->where('yt_video_id','!=',$id)->orderBy('yt_time_uploaded','desc')->take(10)->get();
        $data['latestvideos'] = $latestvideos;

        $data['title'] = $videos->yt_title.' | capdt.media';
        $data['active'] = 'WebseriesVideo';
        $data['sub_active'] = 'VideoInfo';
        return view('frontend.latest-videoInfo', $data);
    }

    public function PopularvideoInfo($id)
    {
        $data = array();
        $videos = Videos::where('yt_video_id',$id)->first();
        $data['videos'] = $videos;
        $popularvideos = Videos::where('yt_status', 1)->where('yt_video_id','!=',$id)->orderBy('yt_viewCount', 'desc')->take(10)->get();
        $data['popularvideos'] = $popularvideos;

        $data['title'] = $videos->yt_title.' | capdt.media';
        $data['active'] = 'WebseriesVideo';
        $data['sub_active'] = 'VideoInfo';
        return view('frontend.popular-videoInfo', $data);
    }

    public function Stories(){
        $data = array();
        $stories = Stories::where('s_status', 1)->orderBy('s_id', 'desc')->paginate(32);
        $data['stories'] = $stories;

        $data['title'] = 'Stories | capdt.media';
        $data['active'] = 'Stories';
        $data['sub_active'] = 'Stories';
        return view('frontend.stories', $data);
    }

    public function StoryInfo($alias)
    {

//        dd($alias);

        $data = array();
        $storyinfo = Stories::where('s_alias',$alias)->first();
        $relatedstories = Stories::where('s_alias','!=',$alias)->orderBy('s_id','desc')->take(5)->get();
        $data['storyinfo'] = $storyinfo;
        $data['relatedstories'] = $relatedstories;

        $data['title'] =  $storyinfo->s_title.' | capdt.media';
        $data['active'] = 'storyinfo';
        $data['sub_active'] = 'storyinfo';
        return view('frontend.story-info', $data);
    }

    public function authorProfile(Request $request, $id)
    {
        $data = array();
        $data['authorprofile'] = User::where('role', 2)->where('id',$id)->first();
        $data['active'] = 'authorProfile';
        $data['title'] = 'Authors Profile';
        $data['main_active'] = 'authors';
        return view('frontend.author-profile', $data);
    }

    public function Memes(){
        $data = array();
        $memes = Memes::where('m_status', 1)->orderBy('m_id', 'desc')->paginate(32);
        $data['memes'] = $memes;

        $data['title'] = 'Memes | capdt.media';
        $data['active'] = 'memes';
        $data['sub_active'] = 'Memes';
        return view('frontend.memes-list', $data);
    }

    public function MemesInfo($id)
    {
        $data = array();
        $memeinfo = Memes::where('m_id',$id)->first();
        $latestmemes = Memes::where('m_id','!=',$id)->orderBy('m_id','desc')->take(5)->get();
        $data['memeinfo'] = $memeinfo;
        $data['latestmemes'] = $latestmemes;

        $data['title'] =  'Memes | capdt.media';
        $data['active'] = 'memeinfo';
        $data['sub_active'] = 'memeinfo';
        return view('frontend.meme-info', $data);
    }

    public function aboutUs(){
        $data = array();

        $data['title'] = 'AboutUs | capdt.media';
        $data['active'] = 'aboutus';
        $data['sub_active'] = 'aboutus';
        return view('frontend.aboutus', $data);
    }

    public function terms(){
        $data = array();

        $data['title'] = 'Terms&Condtions | capdt.media';
        $data['active'] = 'terms';
        $data['sub_active'] = 'terms';
        return view('frontend.terms', $data);
    }

    public function privacyPolicy(){
        $data = array();

        $data['title'] = 'Privacy Policy | capdt.media';
        $data['active'] = 'privacypolicy';
        $data['sub_active'] = 'privacypolicy';
        return view('frontend.privacy-policy', $data);
    }

    public function contact(){
        $data = array();

        $data['title'] = 'ContactUs | capdt.media';
        $data['active'] = 'contactus';
        $data['sub_active'] = 'contactus';
        return view('frontend.contact', $data);
    }

    public function subscriptionSave(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $request->validate([
                'email' => 'required | email'
            ], [
                'email.required' => 'Enter email',
                'email.email' => 'Enter a valid email'
            ]);

            $email=Subscriptions::where('email',$request->email)->count();

            if($email==0){
                $id = Subscriptions::create($requestData)->id;
            }

            if (empty($id)) {
                $id = 0;
            }
            echo $id;
            exit();
        }

    }

    public function contactus(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $request->validate([
                'email' => 'required | email',
                'contact_number' => 'required | numeric'
            ], [
                'email.required' => 'Enter email',
                'email.email' => 'Enter a valid email',
                'contact_number.required' => 'Enter contact Number',
                'contact_number.numeric' => 'Enter a valid number'
            ]);

            ContactUs::create($requestData);
            $mes = 'Thank you ! will call you back';
            return redirect()->route('contactus')->with('flash_message', $mes);
        }
        $data = array();

        $data['title'] = 'Contact us | capdt.media';
        $data['meta_description'] = '';
        $data['meta_keywords'] = '';
        $data['active'] = 'contactus';
        $data['sub_active'] = 'contactus';
        return view('frontend.contact', $data);
    }

}
