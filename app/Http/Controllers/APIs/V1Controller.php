<?php

namespace App\Http\Controllers\Apis;


use App\Author;
use App\Http\Controllers\Controller;
use App\Models\Banners;
use App\Models\Channels;
use App\Models\Memes;
use App\Models\Stories;
use App\Models\Videos;

class V1Controller extends Controller
{

    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'welcome capdt API ';

    }

    public function v1()
    {
        return 'welcome capdt API V1';

    }


    public function getBanners()
    {
        $perPage = 10;
        $resaponceDump = Banners::where('b_status', 1)
            ->orderBy('b_id', 'DESC')
//            ->limit(2)
            ->paginate($perPage);

        if (count($resaponceDump) > 0) {
            foreach ($resaponceDump AS $item) {
                $item->b_banner = Banners::getImage($item->b_banner);

                $ssddd = explode('=', $item->b_link);

                $item->b_link_id = $ssddd[1];
            }
        }
        return $resaponceDump;

    }


    public function getAuthors()
    {
        $perPage = 2;

        $authorsDump = Author::with('getStories')
            ->where('role', 2)
            ->where('status', 1)
            ->paginate($perPage);

        if (count($authorsDump) > 0) {
            foreach ($authorsDump AS $itemKey => $item) {
                $item->profile_img = Author::getProfileImage($item->profile_image);

                if (count($item->getStories) > 0) {
//                    dd('daad');

                    foreach ($item->getStories AS $storiesKey => $stories) {
                        $stories->story_img = Stories::getImage($stories->s_poster);
                    }
                }

            }
        }

        return $authorsDump;

    }


    public function getStories($id = null)
    {

        if (!empty($id)) {


            $resaponceDump = Stories::with('getAuthor')
                ->where('s_id', $id)
                ->where('s_status', 1)
                ->orderBy('s_id', 'DESC')
//            ->limit(2)
                ->first();


            if (!empty($resaponceDump)) {
//                foreach ($resaponceDump AS $item) {

                $resaponceDump->s_poster = Stories::getImage($resaponceDump->s_poster);
//                $resaponceDump->push('s_poster', Stories::getImage($resaponceDump->s_poster));


//                $resaponceDump->s_poster_img = Stories::getImage($resaponceDump->s_poster);
//                }
            }
//            dd($resaponceDump);
            return $resaponceDump;


        } else {


            $perPage = 2;
            $resaponceDump = Stories::with('getAuthor')
                ->where('s_status', 1)
                ->orderBy('s_id', 'DESC')
//            ->limit(2)
                ->paginate($perPage);
            if (count($resaponceDump) > 0) {
                foreach ($resaponceDump AS $item) {
                    $item->s_poster_img = Stories::getImage($item->s_poster);
                }
            }
            return $resaponceDump;

        }


    }


    public function getLatestVideos()
    {
        $perPage = 5;
        $resaponceDump = Videos::where('yt_status', 1)
            ->orderBy('yt_vid', 'DESC')
//            ->limit(2)
            ->paginate($perPage);

        if (count($resaponceDump) > 0) {
            foreach ($resaponceDump AS $item) {
                $item->yt_thumbnails = json_decode($item->yt_thumbnails);
                $item->yt_video_duration = getVideoDuration($item->yt_video_duration);
            }
        }
        return $resaponceDump;

    }

    public function getChannels($id = null)
    {
        $results = array();
//        return $results;

        if (!empty($id)) {


            $resaponceChannelDump = Channels::where('ch_channel_id', $id)->where('ch_status', 1)->first();
            if (!empty($resaponceChannelDump)) {
                $resaponceChannelDump->ch_thumbnails = json_decode($resaponceChannelDump->ch_thumbnails);
                $resaponceChannelDump->ch_branding_image = json_decode($resaponceChannelDump->ch_branding_image);
            }
            $results['info'] = $resaponceChannelDump;


            $perPage = 5;
            $resaponceDump = Videos::where('yt_channel_id', $id)->where('yt_status', 1)->orderBy('yt_vid', 'DESC')->limit(2)->paginate($perPage);
            if (count($resaponceDump) > 0) {
                foreach ($resaponceDump AS $item) {
                    $item->yt_thumbnails = json_decode($item->yt_thumbnails);
                    $item->yt_video_duration = getVideoDuration($item->yt_video_duration);
                }
            }
            $results['videos'] = $resaponceDump;


        } else {


            $perPage = 5;
            $resaponceDump = Channels::where('ch_status', 1)
                ->orderBy('ch_id', 'DESC')
//                ->limit(2)
                ->paginate($perPage);
            if (count($resaponceDump) > 0) {
                foreach ($resaponceDump AS $item) {
                    $item->ch_thumbnails = json_decode($item->ch_thumbnails);
                    $item->ch_branding_image = json_decode($item->ch_branding_image);
                }
            }
            $results = $resaponceDump;
        }

        return $results;

    }

    public function getVideos($id)
    {
        $results = array();


        $resaponceVideoDump = Videos::where('yt_video_id', $id)->where('yt_status', 1)->first();
        if (!empty($resaponceVideoDump)) {
//            foreach ($resaponceVideoDump AS $item) {
            $resaponceVideoDump->yt_thumbnails = json_decode($resaponceVideoDump->yt_thumbnails);
            $resaponceVideoDump->yt_video_duration = getVideoDuration($resaponceVideoDump->yt_video_duration);
//            }
        }

//        dd($resaponceVideoDump);

        $results['video'] = $resaponceVideoDump;


        $perPage = 4;
        $resaponceDump = Videos::where('yt_video_id', '!=', $id)
            ->where('yt_status', 1)
            ->orderBy('yt_vid', 'DESC')
//            ->limit(2)
            ->paginate($perPage);

        if (count($resaponceDump) > 0) {
            foreach ($resaponceDump AS $item) {
                $item->yt_thumbnails = json_decode($item->yt_thumbnails);
                $item->yt_video_duration = getVideoDuration($item->yt_video_duration);
            }
        }

        $results['related'] = $resaponceDump;


        return $results;

    }


    public function getMems()
    {
        $perPage = 2;
        $resaponceDump = Memes::where('m_status', 1)
            ->orderBy('m_id', 'DESC')
//            ->limit(2)
            ->paginate($perPage);
        if (count($resaponceDump) > 0) {
            foreach ($resaponceDump AS $item) {
                $item->m_meme = Memes::getImage($item->m_meme);
            }
        }
        return $resaponceDump;

    }


}
