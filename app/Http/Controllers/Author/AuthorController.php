<?php

namespace App\Http\Controllers\Author;

use App\Models\ContactUs;
use App\Models\Stories;
use App\Models\Subscriptions;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('author');
    }

    public function index()
    {
        $data = array();
        $data['active'] = 'dashboard';
        $data['title'] = 'dashboard';
        $data['main_active'] = 'dashboard';
        return view('admin.dashboard', $data);
    }

    public function dashboard()
    {

//        dd('data');

        $data = array();
        $data['active'] = 'profile';
        $data['title'] = 'Author Profile';
        $data['main_active'] = 'author';
        return view('author.dashboard', $data);
    }

    public function authorStories()
    {

//        dd('data');

        $data = array();
        $data['stories'] = Stories::where('s_user_id', Auth::id())->get();
        $data['active'] = 'mystories';
        $data['title'] = 'My Stories';
        $data['main_active'] = 'author';
        return view('author.stories.list', $data);
    }

    public function profileEdit(Request $request, $id)
    {

//        dd("test");
        if ($request->isMethod('post')) {




            $userId = User::createAuthor($request);

            return redirect()->back()->with('flash_message', 'Auther Updated Successfully!');


        }

//        dd("test");

        $data = array();
        $data['profile'] = User::where('id',$id)->first();
        $data['active'] = 'myprofile';
        $data['title'] = 'My Profile';
        $data['main_active'] = 'author';
        return view('author.profile', $data);

    }

    public function authorStoriesManage(Request $request, $id = null)
    {

//        dd('data');


        if ($request->isMethod('post')) {


            $request->request->add(['s_user_id' => Auth::id()]);


            $result = Stories::createStory($request, $id);

            if ($result = 1) {
                return redirect()->route('author_stories')->with('flash_message', 'Story added!');
            } else if ($result = 2) {
                return redirect()->route('author_stories')->with('flash_message', 'Story Updated');
            }


        }

        $data = array();

        if (isset($id) && $id != '') {
            $data['item'] = Stories::where('s_id', $id)->first();
        } else {

        }


        $data['id'] = ($id) ? $id : '';
        $data['active'] = 'mystories';
        $data['title'] = 'My Stories';
        $data['main_active'] = 'author';
        return view('author.stories.manage', $data);
    }


}
