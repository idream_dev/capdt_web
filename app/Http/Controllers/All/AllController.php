<?php

namespace App\Http\Controllers\All;

use App\Models\Stories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AllController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }


    public function removeStories(Request $request)
    {


        $s_id = $request->post('s_id');
        if ($s_id != '') {
            Stories::removeRecord($s_id);
        }

        return redirect()->back()->with('flash_message', 'Story Removed!');


    }


}
