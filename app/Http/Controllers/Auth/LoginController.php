<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = 'admin/dashboard';
    public function redirectTo()
    {
        // Code here
        // User role
        $role = Auth::user()->role;


//        dd($role);

        // Check user role
        switch ($role) {
            case '1':
                return '/admin';
                break;
            case '2':
                return '/author';
                break;
            default:
                return '/';
                break;
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(Request $request)
    {
        return view('admin.auth.login');
    }


    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        return $credentials;
    }

//    protected $redirectAfterLogout = '/admin/login';

    public function redirectAfterLogout($userinfo)
    {

//        dd($userinfo);

        if ($userinfo->role == 2) {
            return 'author';
        } else {
            return 'admin';
        }

    }


    public function logout(Request $request)
    {

        $userinfo = Auth::user();

        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect($this->redirectAfterLogout($userinfo));
    }
}
