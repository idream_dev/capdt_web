<?php

namespace App;

use App\Models\Banners;
use App\Models\Stories;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class Author extends Authenticatable
{
    use Notifiable;

    protected $guard = 'author';

    protected $table = 'users';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'role', 'mobile', 'profile_image', 'password', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getStories()
    {
        return $this->hasMany(Stories::class, 's_user_id', 'id');
    }


    public static function getProfileImage($name = null)
    {
        $uploadPath = url(User::upDir());

        if ($name != '') {

            return $uploadPath . '/' . $name;
        } else {

            return null;
        }

    }


    public static function upDir()
    {
        $uploadPath = '/uploads/users/';

        return $uploadPath;
    }

    public static function removeImage($oldImage = null)
    {
        $uploadPath = public_path(User::upDir());

        if ($oldImage != '') {
            if ($oldImage != '') {
                File::delete($uploadPath . $oldImage);
            }

        }
    }

    public static function uploadImage($request, $oldImage = null)
    {

        $uploadPath = public_path(User::upDir());


        User::removeImage($oldImage);


        if ($request->hasFile('profile_image')) {
            $file = $request['profile_image'];
            $extension = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            $file->move($uploadPath, $fileName);
            return $fileName;
        }
    }

    public static function removeAuthor($request)
    {
        $requestData = $request->all();

        $Author = User::where('id', $requestData['id'])->first();


        User::removeImage($Author->profile_image);

        User::destroy($requestData['id']);


    }

    public static function createAuthor($request)
    {


        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'mobile' => 'required'
        ],
            [
                'name.required' => 'Name is required',
                'email.required' => 'Email is required',
                'mobile.required' => 'Mobile is required',
            ]);


        $requestData = $request->all();


        if ($requestData['action'] == 'new') {

            $defultpassword = 123456;

            if ($request->hasFile('profile_image')) {
                $requestData['profile_image'] = User::uploadImage($request);
            }
            $requestData['password'] = Hash::make($defultpassword);

            return User::create($requestData)->id;
        } else {

            $Author = User::where('id', $requestData['id'])->first();


            if ($request->hasFile('profile_image')) {
                $requestData['profile_image'] = User::uploadImage($request, $Author->profile_image);
            }


            return $Author->update($requestData);
        }


    }

}
