<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Memes extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'memes';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'm_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['m_title','m_meme','m_status'];



    public static function upDir()
    {
        return $uploadPath = '/uploads/memes';
    }


    public static function removeImage($oldImage)
    {
        $uploadPath = public_path(self::upDir());


        if ($oldImage != '') {
            File::delete($uploadPath . '/' . $oldImage);
        }

    }


    public static function uploadImage($request, $oldImage = null)
    {
        $uploadPath = public_path(self::upDir());

        if ($oldImage != '') {
            self::removeImage($oldImage);
        }
        if ($request->hasFile('m_meme')) {
            $file = $request['m_meme'];
//            dd($oldImage);
            $extension = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            $file->move($uploadPath, $fileName);
            return $fileName;
        }

    }

    public static function getImage($name)
    {
        if ($name != '') {
            return url(self::upDir() . '/' . $name);
        } else {
            return null;
        }
    }


}
