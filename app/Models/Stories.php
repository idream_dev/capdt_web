<?php

namespace App\Models;

use App\Author;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Stories extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stories';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 's_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['s_user_id', 's_title', 's_alias', 's_poster', 's_description', 's_meta_keys', 's_status'];


    public function getAuthor()
    {
        return $this->belongsTo(Author::class, 's_user_id', 'id');
    }


    public static function upDir()
    {
        return $uploadPath = '/uploads/stories';
    }


    public static function removeImage($oldImage)
    {
        $uploadPath = public_path(Stories::upDir());


        if ($oldImage != '') {
            File::delete($uploadPath . '/' . $oldImage);
        }

    }


    public static function uploadImage($request, $oldImage = null)
    {
        $uploadPath = public_path(Stories::upDir());

        if ($oldImage != '') {
            Stories::removeImage($oldImage);
        }
        if ($request->hasFile('s_poster')) {
            $file = $request['s_poster'];
//            dd($oldImage);
            $extension = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            $file->move($uploadPath, $fileName);
            return $fileName;
        }

    }

    public static function getImage($name)
    {
        if ($name != '') {
            return url(Stories::upDir() . '/' . $name);
        } else {
            return null;
        }
    }


    public static function removeRecord($id)
    {
        $itemFile = Stories::findOrFail($id);

        if ($itemFile->s_poster != '') {
            if ($itemFile->s_poster != '') {
                Stories::removeImage($itemFile->s_poster);
            }
        }
        Stories::destroy($id);

        return true;
    }


    public static function createStory($request, $id = null)
    {

        $requestData = $request->all();


        if (isset($requestData['s_id']) && $requestData['s_id'] != '') {


            $s_id = $requestData['s_id'];

            $request->validate([
//                's_title' => 'required|unique:stories',
                's_title' => 'required|unique:stories,s_title,' . $s_id . ',s_id',
                's_description' => 'required',
                's_status' => 'required'
            ],
                [
                    's_title.required' => 'Title is required',
                    's_title.unique' => 'Title Already Exist',
                    's_description.required' => 'Please Enter Description',
                    's_status.required' => 'Status is required',
                ]);


            $requestData['s_alias'] = str_slug($request['s_title']);


            $itemStory = Stories::where('s_id', $s_id)->first();

            if ($request->hasFile('s_poster')) {
                $requestData['s_poster'] = Stories::uploadImage($request, $itemStory->s_poster);
            }


//            dump($s_id);
//            dd($itemStory);

            $itemStory->update($requestData);

            return '2';


        } else {
            $request->validate([
                's_title' => 'required|unique:stories',
                's_poster' => 'required',
                's_description' => 'required',
                's_status' => 'required'
            ],
                [
                    's_title.required' => 'Title is required',
                    's_title.unique' => 'Title Already Exist',
                    's_poster.required' => 'Please Select Photo',
                    's_description.required' => 'Please Enter Description',
                    's_status.required' => 'Status is required',
                ]);


//            $insertRequest = array();
            if ($request->hasFile('s_poster')) {
                $requestData['s_poster'] = self::uploadImage($request);
            }

            $requestData['s_alias'] = str_slug($request['s_title']);
            unset($requestData['s_id']);
            unset($requestData['_token']);

//            dd($requestData);

            $data = self::create($requestData);

//            dd($data->s_id);

            return '1';


//            dd($request);


        }


    }

}
