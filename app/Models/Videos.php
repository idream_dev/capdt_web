<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{
    protected $table = 'videos';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'yt_vid';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'yt_video_id', 'yt_channel_id', 'yt_title', 'yt_time_uploaded', 'yt_status', 'yt_trending', 'yt_live_update_status', 'yt_description', 'yt_thumbnails', 'yt_tags', 'yt_viewCount', 'yt_likeCount', 'yt_dislikeCount', 'yt_favoriteCount', 'yt_commentCount','yt_webseries_id','yt_video_duration'
    ];

//    public function getChannelInfo()
//    {
//        return $this->belongsTo(ApiYoutubeChannels::class, 'yt_channel_id', 'ch_channel_id');
//    }
//
//    public function getvideos()
//    {
//        return $this->belongsTo(MoviesAppVideos::class, 'yt_video_id','mv_video_id');
//    }
}
