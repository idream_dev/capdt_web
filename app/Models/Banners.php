<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Banners extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banners';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'b_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['b_channel_name','b_title','b_banner','b_description','b_link','b_status'];


    public static function upDir()
    {
        return $uploadPath = '/uploads/banners';
    }


    public static function removeImage($oldImage)
    {
        $uploadPath = public_path(self::upDir());


        if ($oldImage != '') {
            File::delete($uploadPath . '/' . $oldImage);
        }

    }


    public static function uploadImage($request, $oldImage = null)
    {
        $uploadPath = public_path(self::upDir());

        if ($oldImage != '') {
            self::removeImage($oldImage);
        }
        if ($request->hasFile('m_meme')) {
            $file = $request['m_meme'];
//            dd($oldImage);
            $extension = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            $file->move($uploadPath, $fileName);
            return $fileName;
        }

    }

    public static function getImage($name)
    {
        if ($name != '') {
            return url(self::upDir() . '/' . $name);
        } else {
            return null;
        }
    }



}
