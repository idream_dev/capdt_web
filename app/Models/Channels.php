<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channels extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'channels';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'ch_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ch_channel_id', 'ch_title', 'ch_description', 'ch_customUrl', 'ch_publishedAt','ch_banner_images',
        'ch_thumbnails', 'ch_viewCount', 'ch_commentCount','ch_subscriberCount','ch_videoCount','ch_status','ch_branding_image'
    ];


    public function channelWiseVideos()
    {
        return $this->hasMany(Videos::class,'yt_channel_id','ch_channel_id');
    }
}
