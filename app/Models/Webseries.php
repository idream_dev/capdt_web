<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Webseries extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'webseries';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'w_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['w_title', 'w_alias','w_poster', 'w_description', 'w_meta_keys', 'w_status'];

    public function WebseriesWiseVideos()
    {
        return $this->hasMany(Videos::class,'yt_webseries_id','w_id');
    }
}
