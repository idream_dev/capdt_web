<?php

namespace App\Console\Commands;
use Youtube;
use App\Models\Channels;
use App\Models\Videos;
use Illuminate\Console\Command;


class YoutubeDataSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'youtubeapi:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'start';
        ini_set("memory_limit", "7G");
        ini_set('max_execution_time', '0');
        ini_set('max_input_time', '0');
        set_time_limit(0);
        ignore_user_abort(true);
        $channelid =Channels::where('ch_status',1)->get();
//        dd($channelid);
        if ($channelid) {
            foreach ($channelid as $newchannel){
                $id=$newchannel->ch_channel_id;
                $channel = Youtube::getChannelById($id);
                if($channel->statistics->videoCount < 50){
                    $videoList = Youtube::listChannelVideos($id, 50, 'date');
                    $requestData = array();
                    foreach($videoList as $videos){
                        $videoind = Youtube::getVideoInfo($videos->id->videoId);
                        $requestData['yt_title']=$videos->snippet->title;
                        $requestData['yt_video_id']=$videos->id->videoId;
                        $requestData['yt_channel_id']=$videos->snippet->channelId;
                        $requestData['yt_thumbnails']=json_encode($videos->snippet->thumbnails);
                        $requestData['yt_description']=$videos->snippet->description;
                        $requestData['yt_viewCount']=$videoind->statistics->viewCount;
                        if (isset($videoind->snippet->tags)) {
                            $requestData['yt_tags'] = implode(',', $videoind->snippet->tags);
                        }else{
                            $requestData['yt_tags'] = '';
                        }
                        $requestData['yt_status']='1';
                        $requestData['yt_time_uploaded']=date('Y-m-d H:i:s', strtotime($videos->snippet->publishedAt));
                        $requestData['yt_video_duration'] = $videoind->contentDetails->duration;
                        $videosnew=Videos::where('yt_video_id',$videos->id->videoId)->get();
                        $countpost=count($videosnew);
                        if($countpost == 0){
                            $videoid=Videos::create($requestData)->yt_vid;
                        }
                    }
                }else {
                    $totalVideos = array();
                    // Set default parameters
                    $perpage = 50;
                    $params = [
                        'type' => 'video',
                        'channelId' => $id,
                        'part' => 'id, snippet',
                        'order' => 'date',
                        'maxResults' => $perpage
                    ];
                    $search = Youtube::searchAdvanced($params, true);
                    $totall = ceil($search['info']['totalResults'] / $perpage);
                    for ($i = 0; $i < ($totall - 1); $i++) {
                        if ($i == 0) {
                            $totalVideos = array_merge($totalVideos, $search['results']);
                            if (isset($search['info']['nextPageToken'])) {
                                $params['pageToken'] = $search['info']['nextPageToken'];
                                //echo ' loop no: ' . $i . ' prepage: ' . $perpage . ' nextpage : ' . $search['info']['nextPageToken'] . ' prev: ' . $search['info']['nextPageToken'] . '<br />';
                            }
                        }
                        $search = Youtube::searchAdvanced($params, true);
                        if (isset($search['info']['nextPageToken'])) {
                            $params['pageToken'] = $search['info']['nextPageToken'];
                        }
                        if (is_array($search['results'])) {
                            $totalVideos = array_merge($totalVideos, $search['results']);
                        } else {
                        }
                    }
                    $requestData = array();
                    foreach ($totalVideos as $videos) {
                        $videoind = Youtube::getVideoInfo($videos->id->videoId);
                        $requestData['yt_title']=$videos->snippet->title;
                        $requestData['yt_video_id']=$videos->id->videoId;
                        $requestData['yt_channel_id']=$videos->snippet->channelId;
                        $requestData['yt_thumbnails']=json_encode($videos->snippet->thumbnails);
                        $requestData['yt_description']=$videos->snippet->description;
                        $requestData['yt_viewCount']=$videoind->statistics->viewCount;
                        if (isset($videoind->snippet->tags)) {
                            $requestData['yt_tags'] = implode(',', $videoind->snippet->tags);
                        }else{
                            $requestData['yt_tags'] = '';
                        }
                        $requestData['yt_status']='1';
                        $requestData['yt_time_uploaded']=date('Y-m-d H:i:s', strtotime($videos->snippet->publishedAt));
                        $requestData['yt_video_duration'] = $videoind->contentDetails->duration;
                        $videosnew = Videos::where('yt_video_id', $videos->id->videoId)->get();
                        $countpost = count($videosnew);
                        if ($countpost == 0) {
                            Videos::create($requestData);
                        }
                    }
                }
            }
        }
        echo "successfully updated";
        echo 'End';
    }
}
