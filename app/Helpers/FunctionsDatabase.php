<?php

use App\Models\Channels;
use App\Models\settings;

function get_yt_subs($channel_id)
{


//    $api_key = 'AIzaSyCjdRRrQ0bhXG068tuR3sR0dH0hKgu6wXg';
    $api_key = 'AIzaSyB8Joq6cPoRsQqjjh-_x5FXnrhih6xxgVI';
//    $channel_id = '123456789';
    if (!$api_key || !$channel_id)
        return false;
    $youtube_url = 'https://www.googleapis.com/youtube/v3/channels?part=statistics&id=' . $channel_id . '&key=' . $api_key;
//    $data = file_get_contents($youtube_url);


    $url = $youtube_url;

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //<----- I will add this line
    $resultData = curl_exec($ch);

    $json_data = json_decode($resultData, true);

//    dd($json_data);


//    $json_data = json_decode($data);


    if (!$json_data)
        return false;
    $subs = $json_data['items'][0]['statistics']['subscriberCount'];


//    dd($subs);
    return ($subs);
}


function getVideoDuration($pattern)
{

    if (isset($pattern) && !empty($pattern)) {
        preg_match_all('/(\d+)/', $pattern, $parts);

        // Put in zeros if we have less than 3 numbers.
        if (count($parts[0]) == 1) {
            array_unshift($parts[0], "0", "0");
        } elseif (count($parts[0]) == 2) {
            array_unshift($parts[0], "0");
        }

        $sec_init = $parts[0][2];
        $seconds = $sec_init % 60;
        $seconds_overflow = floor($sec_init / 60);

        $min_init = $parts[0][1] + $seconds_overflow;
        $minutes = ($min_init) % 60;
        $minutes_overflow = floor(($min_init) / 60);

        $hours = $parts[0][0] + $minutes_overflow;

        $seconds = $seconds < 10 ? '0' . $seconds : $seconds;

        if ($hours != 0) {
            $minutes = $minutes < 10 ? '0' . $minutes : $minutes;
            return $hours . ':' . $minutes . ':' . $seconds;
        } else {
            return $minutes . ':' . $seconds;
        }
    }


}


function getChannels()
{
    return $channels = Channels::where('ch_status', 1)->get();
}

function getsettings()
{
    return $settings = settings::first();;
}

function getAuthors(){
    return \App\Author::where('role', 2)->get();
}


function storiesStatus($id = null)
{

    $status = array(
        1 => 'active',
        2 => 'inactive',
    );

    if ($id != '') {

        if (array_key_exists($id, $status)) {
            return $status[$id];
        } else {
            return null;
        }


    } else {
        return $status;
    }


}








