const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');


mix.copyDirectory('resources/assets/site/images', 'public/site/images');

// Site
mix.sass('resources/assets/site/scss/siteStyle.scss', 'public/site/css');
mix.js('resources/assets/site/js/siteScripts.js', 'public/site/js');

// Admin
mix.sass('resources/assets/sadmin/scss/style.scss', 'public/sadmin/css')
    .js('resources/assets/sadmin/js/scripts.js', 'public/sadmin/js');


mix.copyDirectory('resources/assets/sadmin/images', 'public/sadmin/images');
mix.copyDirectory('resources/assets/sadmin/pages', 'public/sadmin/pages');
mix.copyDirectory('resources/assets/plugins', 'public/plugins');
