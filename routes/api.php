<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', 'APIs\V1Controller@index')->name('api_home');

Route::prefix('v1')->group(function () {
    Route::get('/', 'APIs\V1Controller@v1')->name('api_v1');
    Route::get('get-banners', 'APIs\V1Controller@getBanners')->name('api_getbanners');
    Route::get('get-authors', 'APIs\V1Controller@getAuthors')->name('api_getAuthors');
    Route::get('get-stories/{id?}', 'APIs\V1Controller@getStories')->name('api_getStories');
    Route::get('get-latest-videos', 'APIs\V1Controller@getLatestVideos')->name('api_getLatestVideos');
    Route::get('get-videos/{id}', 'APIs\V1Controller@getVideos')->name('api_getVideos');
    Route::get('get-mems', 'APIs\V1Controller@getMems')->name('api_getMems');
    Route::get('get-channels/{id?}', 'APIs\V1Controller@getChannels')->name('api_getChannels');
});


//Route::get('/', 'APIs\V1Controller@index')->name('api_home');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
