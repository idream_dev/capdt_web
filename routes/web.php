<?php

Route::get('/', 'Frontend\FrontendController@index')->name('home');


Route::get('/channels/{alias?}', 'Frontend\FrontendController@channels')->name('Channels');
Route::get('channels/{alias}/{id}', 'Frontend\FrontendController@ChannelsvideoInfo')->name('ChannelsvideoInfo');

Route::get('/webseries', 'Frontend\FrontendController@WebseriesList')->name('WebseriesList');
Route::get('/webseries/{alias}', 'Frontend\FrontendController@WebseriesView')->name('WebseriesView');
Route::get('/webseries/{alias}/{id}', 'Frontend\FrontendController@WebseriesVideoView')->name('WebseriesVideoView');


Route::get('/latest-videos', 'Frontend\FrontendController@LatestVideos')->name('LatestVideos');
Route::get('/popular-videos', 'Frontend\FrontendController@PopularVideos')->name('PopularVideos');

Route::get('video/{id}', 'Frontend\FrontendController@LatestvideoInfo')->name('LatestvideoInfo');
Route::get('popular/{id}', 'Frontend\FrontendController@PopularvideoInfo')->name('PopularvideoInfo');


Route::get('/stories', 'Frontend\FrontendController@Stories')->name('Stories');
Route::get('/stories/{alias}', 'Frontend\FrontendController@StoryInfo')->name('StoryInfo');
Route::get('/author-profile/{id}', 'Frontend\FrontendController@authorProfile')->name('authorProfile');

Route::get('/memes', 'Frontend\FrontendController@Memes')->name('Memes');
Route::get('/memes/{id}', 'Frontend\FrontendController@MemesInfo')->name('MemesInfo');

Route::get('/aboutus', 'Frontend\FrontendController@aboutUs')->name('aboutUs');
Route::get('/terms-condtions', 'Frontend\FrontendController@terms')->name('terms');
Route::get('/privacy-policy', 'Frontend\FrontendController@privacyPolicy')->name('privacyPolicy');
Route::get('/contact', 'Frontend\FrontendController@contact')->name('contact');

Route::post('/subscription/save', 'Frontend\FrontendController@subscriptionSave')->name('subscriptionSave');
Route::match(array('GET', 'POST'), '/contactus', 'Frontend\FrontendController@contactus')->name('contactus');

Auth::routes();

//Route::get('/home', 'Admin\AdminController@index')->name('home');

Route::prefix('admin')->group(function () {
    Route::get('/', 'Admin\AdminController@index')->name('dashboard');

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
//    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/changePassword', 'Admin\AdminController@showChangePasswordForm')->name('changePassword');
    Route::post('/changePassword', 'Admin\AdminController@showChangePasswordForm')->name('changePassword');

    Route::resource('/settings', 'Admin\SettingsController');


    //Web Series Module
    Route::match(array('GET', 'POST'), 'webseries', 'Admin\WebseriesController@index')->name('webseriesindex');
    Route::match(array('GET', 'POST'), 'webseries/create', 'Admin\WebseriesController@addWebseries')->name('createWebseries');
    Route::post('/webseries/editsave', 'Admin\WebseriesController@webseriesEdit')->name('editWebseries');
    Route::post('/webseries/delete', 'Admin\WebseriesController@webseriesDelete')->name('webseriesDelete');
    Route::get('/all-videos', 'Admin\WebseriesController@AllVideos')->name('AllVideos');
    Route::post('/all-videos/datatablesServersideAllVideos', 'Admin\WebseriesController@DatatablesServersideAllVideos')->name('DatatablesServersideAllVideos');
    Route::get('/uncategorized-videos', 'Admin\WebseriesController@UncategorizedVideos')->name('UncategorizedVideos');
    Route::post('/uncategorized-videos/uncategorizedAllVideos', 'Admin\WebseriesController@UncategorizedAllVideos')->name('UncategorizedAllVideos');
    Route::post('/assign-videos/assign', 'Admin\WebseriesController@AssignedToWebseries')->name('AssignedToWebseries');
    Route::get('/video/show/{id}', 'Admin\WebseriesController@VideosShow')->name('VideosShow');
    Route::post('/videos/remove', 'Admin\WebseriesController@VideoRemove')->name('VideoRemove');
    Route::post('/checkwebseriesAlias', 'Admin\\WebseriesController@checkNewWebseriesAlias');
    Route::get('/webseries/show/{id}', 'Admin\WebseriesController@WebseriesShow')->name('WebseriesShow');
    Route::post('/webseries/serversideVideos', 'Admin\WebseriesController@WebseriesVideos')->name('WebseriesVideos');


    Route::post('/assinged-videos/remove', 'Admin\WebseriesController@WebseriesVideoRemove')->name('WebseriesVideoRemove');


    //Web Series Module
//    Route::match(array('GET', 'POST'), 'stories', 'Admin\StoriesController@index')->name('storiesindex');
//    Route::match(array('GET', 'POST'), 'stories/create', 'Admin\StoriesController@addStories')->name('createStory');
//    Route::post('/stories/editsave', 'Admin\StoriesController@storyEdit')->name('editStory');
    Route::post('/stories/delete', 'Admin\StoriesController@storyDelete')->name('storyDelete');
    Route::post('/checkNewStoriesAlias', 'Admin\\StoriesController@checkNewStoriesAlias');


    Route::match(array('GET', 'POST'), 'channels', 'Admin\ChannelsController@index')->name('channelindex');
    Route::match(array('GET', 'POST'), 'channels/create', 'Admin\ChannelsController@addChannel')->name('createChannel');
    Route::post('/channel/editsave', 'Admin\ChannelsController@channelEdit')->name('editChannel');
    Route::post('/channel/delete', 'Admin\ChannelsController@channelDelete')->name('channelDelete');
    Route::get('/channel/show/{id}', 'Admin\ChannelsController@channelShow')->name('channelShow');
    Route::post('/channel/DatatablesServersideVideos', 'Admin\ChannelsController@DatatablesServersideVideos')->name('DatatablesServersideVideos');
    Route::get('/channel-videos-import/{id}', 'Admin\ChannelsController@channelVideosimport')->name('channelVideosimport');
    Route::post('/checkChannelID', 'Admin\\ChannelsController@checkChannelID');

//  Route::post('/categories/editsave', 'Admin\CategoriesController@categoryEdit')->name('editCategory');


    /*MEMES*/
    Route::match(array('GET', 'POST'), 'memes', 'Admin\MemesController@index')->name('memesindex');
    Route::match(array('GET', 'POST'), 'memes/create', 'Admin\MemesController@addMemes')->name('createMeme');
    Route::post('/memes/editsave', 'Admin\MemesController@MemesEdit')->name('editMeme');
    Route::post('/memes/delete', 'Admin\MemesController@memeDelete')->name('memeDelete');


    /*Banners*/
    Route::match(array('GET', 'POST'), 'banners', 'Admin\BannersController@index')->name('bannersindex');
    Route::match(array('GET', 'POST'), 'banners/create', 'Admin\BannersController@addBanners')->name('createBanner');
    Route::post('/banner/editsave', 'Admin\BannersController@BannerEdit')->name('editBanner');
    Route::post('/banner/delete', 'Admin\BannersController@BannerDelete')->name('BannerDelete');

    Route::get('contactus', 'Admin\AdminController@contactList')->name('contactList');
    Route::get('subscriptions', 'Admin\AdminController@subscriptions')->name('subscriptions');


    Route::match(array('GET', 'POST'), 'authors', 'Admin\AdminController@author')->name('admin_authors');

    //Web Series Module
//    Route::match(array('GET', 'POST'), 'stories', 'Admin\StoriesController@index')->name('storiesindex');
//    Route::match(array('GET', 'POST'), 'stories/create', 'Admin\StoriesController@addStories')->name('createStory');
//    Route::post('/stories/editsave', 'Admin\StoriesController@storyEdit')->name('editStory');
    Route::match(array('GET', 'POST'), 'manage/stories', 'Admin\StoriesController@index')->name('storiesindex');
    Route::match(array('GET', 'POST'), 'manage/stories/manage/{id?}', 'Admin\StoriesController@addStories')->name('createStory');
    Route::post('/stories/delete', 'Admin\StoriesController@storyDelete')->name('storyDelete');
    Route::post('/checkNewStoriesAlias', 'Admin\\StoriesController@checkNewStoriesAlias');


});

Route::prefix('author')->group(function () {

    Route::get('/', 'Author\AuthorController@dashboard')->name('author_dashboard');
    Route::get('/stories', 'Author\AuthorController@authorStories')->name('author_stories');
    Route::match(array('GET', 'POST'), '/stories/manage/{id?}', 'Author\AuthorController@authorStoriesManage')->name('authorStoriesManage');

    Route::match(array('GET', 'POST'), '/profile/{id?}', 'Author\AuthorController@profileEdit')->name('authorprofileEdit');

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('author_login');
    Route::post('/login', 'Auth\LoginController@login');
//    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/changePassword', 'Admin\AdminController@showChangePasswordForm')->name('changePassword');
    Route::post('/changePassword', 'Admin\AdminController@showChangePasswordForm')->name('changePassword');


});


Route::prefix('global')->group(function () {
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::post('/stories_remove', 'All\AllController@removeStories')->name('global_remove_stories');
});
Route::prefix('ajax')->group(function () {

});


//
//Route::prefix('api')->group(function () {
//

//
//
//});
