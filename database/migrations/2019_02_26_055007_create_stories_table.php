<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stories', function (Blueprint $table) {
            $table->increments('s_id');
            $table->integer('s_user_id');
            $table->string('s_title');
            $table->string('s_alias');
            $table->string('s_poster')->nullable();
            $table->longText('s_description')->nullable();
            $table->text('s_meta_keys')->nullable();
            $table->string('s_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stories');
    }
}
