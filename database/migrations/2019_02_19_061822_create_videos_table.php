<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('yt_vid');
            $table->string('yt_video_id')->unique();
            $table->integer('yt_webseries_id')->nullable();
            $table->string('yt_channel_id')->default(0)->nullable();
            $table->text('yt_title')->nullable();
            $table->text('yt_description')->nullable();
            $table->text('yt_thumbnails')->nullable();
            $table->text('yt_tags')->nullable();
            $table->integer('yt_viewCount')->nullable();
            $table->integer('yt_likeCount')->nullable();
            $table->integer('yt_dislikeCount')->nullable();
            $table->integer('yt_favoriteCount')->nullable();
            $table->integer('yt_commentCount')->nullable();
            $table->integer('yt_status')->default(0);
            $table->integer('yt_live_update_status')->default(0)->nullable();
            $table->integer('yt_trending')->default(0)->comment('0=No,1=Yes')->nullable();
            $table->string('yt_time_uploaded')->nullable();
            $table->string('yt_video_duration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
