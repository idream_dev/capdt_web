<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebseriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webseries', function (Blueprint $table) {
            $table->increments('w_id');
            $table->string('w_title');
            $table->string('w_alias');
            $table->string('w_poster')->nullable();
            $table->text('w_description')->nullable();
            $table->text('w_meta_keys')->nullable();
            $table->string('w_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webseries');
    }
}
