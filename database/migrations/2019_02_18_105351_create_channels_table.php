<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channels', function (Blueprint $table) {
            $table->increments('ch_id');
            $table->string('ch_channel_id')->unique();
            $table->string('ch_title')->nullable();
            $table->text('ch_description')->nullable();
            $table->string('ch_customUrl')->nullable();
            $table->string('ch_publishedAt')->nullable();
            $table->text('ch_thumbnails')->nullable();
            $table->text('ch_banner_images')->nullable();
            $table->text('ch_branding_image')->nullable();
            $table->integer('ch_viewCount')->nullable();
            $table->integer('ch_commentCount')->nullable();
            $table->integer('ch_subscriberCount')->nullable();
            $table->integer('ch_videoCount')->nullable();
            $table->integer('ch_status')->default(1)->comment('0=InActive,1=Active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channels');
    }
}
